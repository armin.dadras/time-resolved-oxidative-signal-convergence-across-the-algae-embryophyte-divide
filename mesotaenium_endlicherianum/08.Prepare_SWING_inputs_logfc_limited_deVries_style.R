library(tidyverse)
### Keywords
apocarotenoid <- read_tsv("Analyses/SWING/inputs/unique_ATH/apocarotenoid.txt", col_names = F)
carotenoid <- read_tsv("Analyses/SWING/inputs/unique_ATH/carotenoid.txt", col_names = F)
cold_keywords <- read_tsv("Analyses/SWING/inputs/unique_ATH/cold.txt", col_names = F)
heat_keywords <- read_tsv("Analyses/SWING/inputs/unique_ATH/heat.txt", col_names = F)
high_light_keywords <- read_tsv("Analyses/SWING/inputs/unique_ATH/high_light.txt", col_names = F)
oxidative <- read_tsv("Analyses/SWING/inputs/unique_ATH/oxidative.txt", col_names = F)
ath <- c(unlist(apocarotenoid), unlist(carotenoid), unlist(cold_keywords), unlist(heat_keywords), unlist(high_light_keywords), unlist(oxidative))
ath <- unique(ath)
### Orthofinder
ath_meso_og <- read_tsv("../orthofinder/Phylogenetic_Hierarchical_Orthogroups/N0.tsv") %>%
    select(c(A_thaliana, M_endlicherianum)) %>%
    separate_rows(A_thaliana, sep = ", ") %>%
    separate_rows(M_endlicherianum, sep = ", ") %>%
    drop_na() %>%
    distinct() %>%
    filter(A_thaliana %in% ath) %>%
    select(M_endlicherianum) %>%
    unlist() %>%
    unique() %>%
    as_vector()
### TFs
meso_TFs <- c(unlist(read_tsv("Analyses/SWING/inputs/unique_ATH/TFs.txt", col_names = F)))


### high_light_s
high_light_s025 <- read_tsv("Analyses/DGEA/tables/high_light_s_0.25_vs_standard_growth_0_logFC_ebFit.tsv")
colnames(high_light_s025) <- c("geneID", paste0(colnames(high_light_s025)[-1], "_high_light_s025"))
high_light_s05 <- read_tsv("Analyses/DGEA/tables/high_light_s_0.5_vs_standard_growth_0.5_logFC_ebFit.tsv")
colnames(high_light_s05) <- c("geneID", paste0(colnames(high_light_s05)[-1], "_high_light_s05"))
high_light_s2 <- read_tsv("Analyses/DGEA/tables/high_light_s_2_vs_standard_growth_2_logFC_ebFit.tsv")
colnames(high_light_s2) <- c("geneID", paste0(colnames(high_light_s2)[-1], "_high_light_s2"))
high_light_s4 <- read_tsv("Analyses/DGEA/tables/high_light_s_4_vs_standard_growth_4_logFC_ebFit.tsv")
colnames(high_light_s4) <- c("geneID", paste0(colnames(high_light_s4)[-1], "_high_light_s4"))
high_light_s6 <- read_tsv("Analyses/DGEA/tables/high_light_s_6_vs_standard_growth_6_logFC_ebFit.tsv")
colnames(high_light_s6) <- c("geneID", paste0(colnames(high_light_s6)[-1], "_high_light_s6"))

high_light_s <- high_light_s025 %>%
    full_join(high_light_s05, by = "geneID") %>%
    full_join(high_light_s2, by = "geneID") %>%
    full_join(high_light_s4, by = "geneID") %>%
    full_join(high_light_s6, by = "geneID") %>%
    select("geneID", "logFC_high_light_s025", "logFC_high_light_s05", "logFC_high_light_s2", "logFC_high_light_s4", "logFC_high_light_s6")
high_light_s_filtered <- high_light_s %>%
    filter((geneID %in% ath_meso_og) | (geneID %in% meso_TFs))
colnames(high_light_s_filtered) <- c("gene", "0.25", "0.5", "2", "4", "6")
high_light_s_filtered_t <- t(high_light_s_filtered)
colnames(high_light_s_filtered_t) <- high_light_s_filtered_t[1,]
high_light_s_filtered_t <- high_light_s_filtered_t[-1,]
high_light_s_filtered_t <- rownames_to_column(as.data.frame(high_light_s_filtered_t), var = "time")

### Write the inputs for SWING
write_tsv(x = high_light_s_filtered_t, file = "Analyses/SWING/inputs/swing_input/high_light_only_transcripts.tsv")
