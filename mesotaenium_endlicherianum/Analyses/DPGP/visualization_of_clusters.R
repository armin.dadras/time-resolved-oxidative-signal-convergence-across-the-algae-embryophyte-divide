library(tidyverse)
library(clusterProfiler)

clustering <- read_tsv("_optimal_clustering.txt")
background <- read_tsv("combined_logFC_filtered_only_cold_samples.tsv") %>%
    select(gene) %>%
    unlist() %>%
    as.vector()

term2gene <- read_tsv("term2gene_GO.tsv")
term2name <- read_tsv("term2name_GO.tsv") %>%
    drop_na()
term2gene <- term2gene %>%
    filter(GOs %in% dplyr::intersect(term2gene$GOs, term2name$GOs))
common <- dplyr::intersect(term2gene$gene, background)

my_universe <- term2gene[term2gene$gene %in% background,]

## enrichment happens here
for (my_cluster in unique(clustering$cluster)){
    cluster_genes <- clustering %>%
        filter(cluster == my_cluster) %>%
        select(gene) %>%
        unlist() %>%
        as.vector()
    
    enrichment <- enricher(cluster_genes,
                           TERM2GENE = term2gene,
                           TERM2NAME = term2name,
                           pvalueCutoff = 0.05,
                           universe = my_universe$gene,
                           qvalueCutoff = 0.05)
    #save the enrichment result
    write.csv(file = paste0("enrichment/tables/", my_cluster, "_enrichment.csv"),                 # EDIT THIS
              x = enrichment@result)
    
    if (any(enrichment@result$p.adjust <= 0.05)){
        p <- dotplot(enrichment,
                     x= "geneRatio", # Options: GeneRatio, BgRatio, pvalue, p.adjust, qvalue
                     color="p.adjust",
                     orderBy = "x", # Options: GeneRatio, BgRatio, pvalue, p.adjust, qvalue
                     showCategory=100,
                     font.size=8) +
            ggtitle("dotplot for ORA")
        
        ggsave(filename = paste0("enrichment/plots/", my_cluster, "_dotplot.pdf"),                # EDIT THIS
               plot =  p,  dpi = 300, width = 21, height = 42, units = "cm")
        
        p <- cnetplot(enrichment,
                      node_label="all",
                      showCategory = 100,
                      cex.params = list(category_label = 0.5,
                                        gene_label = 0.5),
                      layout = "kk")
        ggsave(filename = paste0("enrichment/plots/", my_cluster, "_cnetplot.pdf"),               # EDIT THIS
               plot =  p,  dpi = 300,
               width =  21, height = 42, units = "cm")
    }
}
