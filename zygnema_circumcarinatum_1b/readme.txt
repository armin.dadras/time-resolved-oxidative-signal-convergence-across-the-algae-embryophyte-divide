####################
RNA-Seq quantification
####################

The transcriptome file was used from the below paper (SAG698_1b_CDS.fna).

Cite: https://www.biorxiv.org/content/10.1101/2023.01.31.526407v1

Then, the strandness of reads were checked using the snakemake pipeline and this is a "reverse" library.


For some samples, we had more than two files per sample since the sequencing facility sequenced them on more than one lane. Afterwards, the reads got quantified using the snakemake pipeline.