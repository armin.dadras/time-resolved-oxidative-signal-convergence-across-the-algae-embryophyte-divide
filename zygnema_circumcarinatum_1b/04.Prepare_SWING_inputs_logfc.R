library(tidyverse)
### Keywords
apocarotenoid <- read_tsv("Analyses/SWING/inputs/unique_ATH/apocarotenoid.txt", col_names = F)
carotenoid <- read_tsv("Analyses/SWING/inputs/unique_ATH/carotenoid.txt", col_names = F)
cold_keywords <- read_tsv("Analyses/SWING/inputs/unique_ATH/cold.txt", col_names = F)
heat_keywords <- read_tsv("Analyses/SWING/inputs/unique_ATH/heat.txt", col_names = F)
high_light_keywords <- read_tsv("Analyses/SWING/inputs/unique_ATH/high_light.txt", col_names = F)
oxidative <- read_tsv("Analyses/SWING/inputs/unique_ATH/oxidative.txt", col_names = F)
ath <- c(unlist(apocarotenoid), unlist(carotenoid), unlist(cold_keywords), unlist(heat_keywords), unlist(high_light_keywords), unlist(oxidative))
ath <- unique(ath)
### Orthofinder
ath_zygnema_og <- read_tsv("../orthofinder/Phylogenetic_Hierarchical_Orthogroups/N0.tsv") %>%
    select(c(A_thaliana, Z_circumcarinatum_SAG698_1b)) %>%
    separate_rows(A_thaliana, sep = ", ") %>%
    separate_rows(Z_circumcarinatum_SAG698_1b, sep = ", ") %>%
    drop_na() %>%
    distinct() %>%
    filter(A_thaliana %in% ath) %>%
    select(Z_circumcarinatum_SAG698_1b) %>%
    unlist() %>%
    unique() %>%
    as_vector()
### TFs
zygnema_TFs <- c(unlist(read_tsv("Analyses/SWING/inputs/unique_ATH/TFs.txt", col_names = F)))

### cold
cold2 <- read_tsv("Analyses/DGEA/tables/cold_2_vs_standard_growth_2_logFC_ebFit.tsv")
colnames(cold2) <- c("geneID", paste0(colnames(cold2)[-1], "_cold2"))
cold6 <- read_tsv("Analyses/DGEA/tables/cold_6_vs_standard_growth_6_logFC_ebFit.tsv")
colnames(cold6) <- c("geneID", paste0(colnames(cold6)[-1], "_cold6"))
cold24 <- read_tsv("Analyses/DGEA/tables/cold_24_vs_standard_growth_24_logFC_ebFit.tsv")
colnames(cold24) <- c("geneID", paste0(colnames(cold24)[-1], "_cold24"))
cold <- cold2 %>%
    full_join(cold6, by = "geneID") %>%
    full_join(cold24, by = "geneID") %>%
    select("geneID", "logFC_cold2", "logFC_cold6", "logFC_cold24")
cold_filtered <- cold %>%
    filter((geneID %in% ath_zygnema_og) | (geneID %in% zygnema_TFs))
colnames(cold_filtered) <- c("gene", "2", "6", "24")
cold_filtered_t <- t(cold_filtered)
colnames(cold_filtered_t) <- cold_filtered_t[1,]
cold_filtered_t <- cold_filtered_t[-1,]
cold_filtered_t <- rownames_to_column(as.data.frame(cold_filtered_t), var = "time")
### read the metabolites
metabolites <- read_tsv("Analyses/SWING/inputs/metabolites/cold_metabolite.txt", col_names = T)
metabolites$time <- as.character(metabolites$time)
cold_combined <- cold_filtered_t %>%
    full_join(metabolites, by = "time") %>%
    drop_na()
### Write the inputs for SWING
write_tsv(x = cold_combined, file = "Analyses/SWING/inputs/swing_input/Zc_cold.tsv")
### heat
heat2 <- read_tsv("Analyses/DGEA/tables/heat_2_vs_standard_growth_2_logFC_ebFit.tsv")
colnames(heat2) <- c("geneID", paste0(colnames(heat2)[-1], "_heat2"))
heat6 <- read_tsv("Analyses/DGEA/tables/heat_6_vs_standard_growth_6_logFC_ebFit.tsv")
colnames(heat6) <- c("geneID", paste0(colnames(heat6)[-1], "_heat6"))
heat24 <- read_tsv("Analyses/DGEA/tables/heat_24_vs_standard_growth_24_logFC_ebFit.tsv")
colnames(heat24) <- c("geneID", paste0(colnames(heat24)[-1], "_heat24"))
heat <- heat2 %>%
    full_join(heat6, by = "geneID") %>%
    full_join(heat24, by = "geneID") %>%
    select("geneID", "logFC_heat2", "logFC_heat6", "logFC_heat24")
heat_filtered <- heat %>%
    filter((geneID %in% ath_zygnema_og) | (geneID %in% zygnema_TFs))
colnames(heat_filtered) <- c("gene", "2", "6", "24")
heat_filtered_t <- t(heat_filtered)
colnames(heat_filtered_t) <- heat_filtered_t[1,]
heat_filtered_t <- heat_filtered_t[-1,]
heat_filtered_t <- rownames_to_column(as.data.frame(heat_filtered_t), var = "time")
### read the metabolites
metabolites <- read_tsv("Analyses/SWING/inputs/metabolites/heat_metabolite.txt", col_names = T)
metabolites$time <- as.character(metabolites$time)
heat_combined <- heat_filtered_t %>%
    full_join(metabolites, by = "time") %>%
    drop_na()
### Write the inputs for SWING
write_tsv(x = heat_combined, file = "Analyses/SWING/inputs/swing_input/Zc_heat.tsv")
### high_light_s
high_light_s025 <- read_tsv("Analyses/DGEA/tables/high_light_s_0.25_vs_standard_growth_0_logFC_ebFit.tsv")
colnames(high_light_s025) <- c("geneID", paste0(colnames(high_light_s025)[-1], "_high_light_s025"))
high_light_s2 <- read_tsv("Analyses/DGEA/tables/high_light_s_2_vs_standard_growth_2_logFC_ebFit.tsv")
colnames(high_light_s2) <- c("geneID", paste0(colnames(high_light_s2)[-1], "_high_light_s2"))
high_light_s6 <- read_tsv("Analyses/DGEA/tables/high_light_s_6_vs_standard_growth_6_logFC_ebFit.tsv")
colnames(high_light_s6) <- c("geneID", paste0(colnames(high_light_s6)[-1], "_high_light_s6"))
high_light_r025 <- read_tsv("Analyses/DGEA/tables/high_light_r_0.25_vs_high_light_s_6_logFC_ebFit.tsv")
colnames(high_light_r025) <- c("geneID", paste0(colnames(high_light_r025)[-1], "_high_light_r025"))
high_light_r4 <- read_tsv("Analyses/DGEA/tables/high_light_r_4_vs_high_light_s_6_logFC_ebFit.tsv")
colnames(high_light_r4) <- c("geneID", paste0(colnames(high_light_r4)[-1], "_high_light_r4"))

high_light_s <- high_light_s025 %>%
    full_join(high_light_s2, by = "geneID") %>%
    full_join(high_light_s6, by = "geneID") %>%
    full_join(high_light_r025, by = "geneID") %>%
    full_join(high_light_r4, by = "geneID") %>%
    select("geneID", "logFC_high_light_s025", "logFC_high_light_s2", "logFC_high_light_s6",
           "logFC_high_light_r025", "logFC_high_light_r4")
high_light_s_filtered <- high_light_s %>%
    filter((geneID %in% ath_zygnema_og) | (geneID %in% zygnema_TFs))
colnames(high_light_s_filtered) <- c("gene", "0.25", "2", "6", "6.25", "10")
high_light_s_filtered_t <- t(high_light_s_filtered)
colnames(high_light_s_filtered_t) <- high_light_s_filtered_t[1,]
high_light_s_filtered_t <- high_light_s_filtered_t[-1,]
high_light_s_filtered_t <- rownames_to_column(as.data.frame(high_light_s_filtered_t), var = "time")
### read the metabolites
metabolites <- read_tsv("Analyses/SWING/inputs/metabolites/high_light_metabolite.txt", col_names = T)
metabolites$time <- as.character(metabolites$time)
high_light_s_combined <- high_light_s_filtered_t %>%
    full_join(metabolites, by = "time") %>%
    drop_na()
### Write the inputs for SWING
write_tsv(x = high_light_s_combined, file = "Analyses/SWING/inputs/swing_input/Zc_high_light.tsv")
