)))))) %>%
select(color) %>%
unlist() %>%
as.vector()
V(network)$labelColor <- node_names %>%
filter(gene %in% V(network)$name) %>%
mutate(color = if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][1]]), "#8f6900",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][2]]), "#a13502",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][3]]), "#66002c",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][4]]), "#460e95",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][5]]), "#003fa3",
"#000000"
)))))) %>%
select(color) %>%
unlist() %>%
as.vector()
edge_colors <- colorRampPalette(c("#e5e5e5", "#000000"))(100)[as.numeric(cut(zygnema_filtered$weight*1e10, breaks = 100))]
V(network)$size <- node_names %>%
filter(gene %in% V(network)$name) %>%
select(vertex_size) %>%
unlist() %>%
as.vector()
V(network)$name <- node_names %>%
filter(gene %in% V(network)$name) %>%
select(use_me) %>%
unlist() %>%
as.vector()
#-------------------------------------------------------------------------------
# plot the network
#-------------------------------------------------------------------------------
pdf(file = paste0("~/Desktop/black_magic/merged_top.001_Vodo_list_transcript_only.pdf"),   # The directory you want to save the file in
width = 15, # The width of the plot in inches
height = 15) # The height of the plot in inches
plot(network,
# vertex
vertex.shape = "circle",
vertex.size = V(network)$size,
vertex.size = 1,
vertex.color = V(network)$color,
vertex.label = V(network)$name,
vertex.label.cex = 0.4,
vertex.frame.color = NA,
vertex.frame.width = 1,
vertex.label.color=V(network)$labelColor,
#edge
#edge.color = "#e5e5e5",
edge.color = edge_colors,
edge.width = 0.5,
dge.arrow.width = 0.1,
edge.arrow.size= 0.1,
edge.lty = "solid",
edge.label = NA,
# other
main = paste0("Zygnema merged SWING top 0.1% pairs - Vodo"),
layout=network_layout*0.7,
)
dev.off()
#-------------------------------------------------------------------------------
# Load the data
#-------------------------------------------------------------------------------
cold <- read_tsv("Analyses/SWING/outputs/Zc_cold.tsv") %>%
filter(mean_importance != 0) # filter out those that have no meaning
colnames(cold) <- c("regulator", "target", "weight")
heat <- read_tsv("Analyses/SWING/outputs/Zc_heat.tsv") %>%
filter(mean_importance != 0) # filter out those that have no meaning
colnames(heat) <- c("regulator", "target", "weight")
highlight <- read_tsv("Analyses/SWING/outputs/Zc_HL.tsv") %>%
filter(mean_importance != 0) # filter out those that have no meaning
colnames(highlight) <- c("regulator", "target", "weight")
blast <- read_tsv("../best_blast_hit/zygnema_swing_labels.tsv", col_names = T) %>%
distinct(zygnema, .keep_all = TRUE)
metabolites <- c("viol",	"9_cis_neo",	"anthera",	"Chlb",	"Lut",	"Zeax",	"Chla",	"alpha_car",	"11_cis",	"beta_car",	"9_cis",	"6MHO",	"beta_CC",	"beta_Io",	"DHA",	"DHA_per_beta_Io",	"Chla_per_b",	"beta_car_per_9_cis_neox",	"V_A_Z_per_Chla_b",	"A_Z_per_V_A_Z",	"beta_Car_per_beta_CC",	"beta_Car_per_beta_Io",	"beta_Car_per_DHA",	"beta_Car_per_beta_CC_beta_Io_DHA")
metabolite_tibble <- tibble(
zygnema = metabolites,
ath = metabolites,
symbol = metabolites,
use_me = metabolites
)
blast <- bind_rows(blast, metabolite_tibble)
ath_carotenoid <- read_tsv("Analyses/SWING/inputs/unique_ATH/carotenoid.txt", col_names = F)
ath_apocarotenoid <- read_tsv("Analyses/SWING/inputs/unique_ATH/apocarotenoid.txt", col_names = F)
ath_cold <- read_tsv("Analyses/SWING/inputs/unique_ATH/cold.txt", col_names = F)
ath_heat <- read_tsv("Analyses/SWING/inputs/unique_ATH/heat.txt", col_names = F)
ath_highlight <- read_tsv("Analyses/SWING/inputs/unique_ATH/high_light.txt", col_names = F)
ath_oxidative <- read_tsv("Analyses/SWING/inputs/unique_ATH/oxidative.txt", col_names = F)
TFs <- read_tsv("Analyses/SWING/inputs/unique_ATH/TFs.txt", col_names = F)
#-------------------------------------------------------------------------------
# Filter first for specific keywords and then visualize the top 0.1%
#-------------------------------------------------------------------------------
zygnema_ID_list <- read_tsv("~/Desktop/black_magic/ZciIDsForBlackMagic.txt", col_names = F) %>%
unlist() %>% as.vector()
cold_filtered <- cold %>%
filter((regulator %in% zygnema_ID_list) | (target %in% zygnema_ID_list) |
(regulator %in% metabolites) | (target %in% metabolites) |
(regulator %in% unlist(TFs)) | (regulator %in% unlist(TFs)) )
cold_filtered <- cold_filtered[1:round(dim(cold_filtered)[1]*0.001), ] # pick top 1 %
node_list_unique <- cold_filtered[, 1:2] %>%
gather(key, value, regulator, target) %>%
select(value) %>%
unlist() %>%
as.vector() %>%
unique()
blast_filtered <- blast %>%
filter(zygnema %in% node_list_unique)
top_predictions <- cold_filtered %>%
slice_head(n=100) %>%
gather(key, value, regulator, target) %>%
select(value) %>%
left_join(blast, by = join_by("value" == "zygnema")) %>%
select(value, use_me) %>%
mutate(use_me = ifelse(is.na(use_me), value, use_me))
node_names <- cold_filtered %>%
gather(key, gene, regulator, target) %>%
select(gene) %>%
left_join(top_predictions, by = join_by("gene" == "value")) %>%
mutate(vertex_size = if_else(gene %in% top_predictions$value, 3, 1)) %>%
distinct()
network <- graph_from_data_frame(d=cold_filtered[,1:2], directed=T)
network_layout <- layout_nicely(network)
# Find connected components
components <- clusters(network)
top_components <- components$csize[order(components$csize, decreasing = TRUE)][1:5]
top_indices <- which(components$csize %in% top_components)
V(network)$color <- node_names %>%
filter(gene %in% V(network)$name) %>%
mutate(color = if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][1]]), "#FFBE0B",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][2]]), "#FB5607",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][3]]), "#FF006E",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][4]]), "#8338EC",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][5]]), "#3A86FF",
"#ece4db"
)))))) %>%
select(color) %>%
unlist() %>%
as.vector()
V(network)$labelColor <- node_names %>%
filter(gene %in% V(network)$name) %>%
mutate(color = if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][1]]), "#8f6900",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][2]]), "#a13502",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][3]]), "#66002c",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][4]]), "#460e95",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][5]]), "#003fa3",
"#000000"
)))))) %>%
select(color) %>%
unlist() %>%
as.vector()
edge_colors <- colorRampPalette(c("#e5e5e5", "#000000"))(100)[as.numeric(cut(cold_filtered$weight*1e10, breaks = 100))]
V(network)$size <- node_names %>%
filter(gene %in% V(network)$name) %>%
select(vertex_size) %>%
unlist() %>%
as.vector()
V(network)$name <- node_names %>%
filter(gene %in% V(network)$name) %>%
select(use_me) %>%
unlist() %>%
as.vector()
#-------------------------------------------------------------------------------
# plot the network
#-------------------------------------------------------------------------------
pdf(file = paste0("~/Desktop/black_magic/cold_top.001_Vodo_list.pdf"),   # The directory you want to save the file in
width = 15, # The width of the plot in inches
height = 15) # The height of the plot in inches
plot(network,
# vertex
vertex.shape = "circle",
vertex.size = V(network)$size,
vertex.size = 1,
vertex.color = V(network)$color,
vertex.label = V(network)$name,
vertex.label.cex = 0.4,
vertex.frame.color = NA,
vertex.frame.width = 1,
vertex.label.color=V(network)$labelColor,
#edge
#edge.color = "#e5e5e5",
edge.color = edge_colors,
edge.width = 0.5,
dge.arrow.width = 0.1,
edge.arrow.size= 0.1,
edge.lty = "solid",
edge.label = NA,
# other
main = paste0("Zygnema Cold SWING top 0.1% pairs - Vodo list"),
layout=network_layout*0.7,
)
dev.off()
heat_filtered <- heat %>%
filter((regulator %in% zygnema_ID_list) | (target %in% zygnema_ID_list) |
(regulator %in% metabolites) | (target %in% metabolites) |
(regulator %in% unlist(TFs)) | (regulator %in% unlist(TFs)) )
heat_filtered <- heat_filtered[1:round(dim(heat_filtered)[1]*0.001), ] # pick top 1 %
node_list_unique <- heat_filtered[, 1:2] %>%
gather(key, value, regulator, target) %>%
select(value) %>%
unlist() %>%
as.vector() %>%
unique()
blast_filtered <- blast %>%
filter(zygnema %in% node_list_unique)
top_predictions <- heat_filtered %>%
slice_head(n=100) %>%
gather(key, value, regulator, target) %>%
select(value) %>%
left_join(blast, by = join_by("value" == "zygnema")) %>%
select(value, use_me) %>%
mutate(use_me = ifelse(is.na(use_me), value, use_me))
node_names <- heat_filtered %>%
gather(key, gene, regulator, target) %>%
select(gene) %>%
left_join(top_predictions, by = join_by("gene" == "value")) %>%
mutate(vertex_size = if_else(gene %in% top_predictions$value, 3, 1)) %>%
distinct()
network <- graph_from_data_frame(d=heat_filtered[,1:2], directed=T)
network_layout <- layout_nicely(network)
# Find connected components
components <- clusters(network)
top_components <- components$csize[order(components$csize, decreasing = TRUE)][1:5]
top_indices <- which(components$csize %in% top_components)
V(network)$color <- node_names %>%
filter(gene %in% V(network)$name) %>%
mutate(color = if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][1]]), "#FFBE0B",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][2]]), "#FB5607",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][3]]), "#FF006E",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][4]]), "#8338EC",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][5]]), "#3A86FF",
"#ece4db"
)))))) %>%
select(color) %>%
unlist() %>%
as.vector()
V(network)$labelColor <- node_names %>%
filter(gene %in% V(network)$name) %>%
mutate(color = if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][1]]), "#8f6900",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][2]]), "#a13502",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][3]]), "#66002c",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][4]]), "#460e95",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][5]]), "#003fa3",
"#000000"
)))))) %>%
select(color) %>%
unlist() %>%
as.vector()
edge_colors <- colorRampPalette(c("#e5e5e5", "#000000"))(100)[as.numeric(cut(heat_filtered$weight*1e10, breaks = 100))]
V(network)$size <- node_names %>%
filter(gene %in% V(network)$name) %>%
select(vertex_size) %>%
unlist() %>%
as.vector()
V(network)$name <- node_names %>%
filter(gene %in% V(network)$name) %>%
select(use_me) %>%
unlist() %>%
as.vector()
#-------------------------------------------------------------------------------
# plot the network
#-------------------------------------------------------------------------------
pdf(file = paste0("~/Desktop/black_magic/heat_top.001_Vodo_list.pdf"),   # The directory you want to save the file in
width = 15, # The width of the plot in inches
height = 15) # The height of the plot in inches
plot(network,
# vertex
vertex.shape = "circle",
vertex.size = V(network)$size,
vertex.size = 1,
vertex.color = V(network)$color,
vertex.label = V(network)$name,
vertex.label.cex = 0.4,
vertex.frame.color = NA,
vertex.frame.width = 1,
vertex.label.color=V(network)$labelColor,
#edge
#edge.color = "#e5e5e5",
edge.color = edge_colors,
edge.width = 0.5,
dge.arrow.width = 0.1,
edge.arrow.size= 0.1,
edge.lty = "solid",
edge.label = NA,
# other
main = paste0("Zygnema heat SWING top 0.1% pairs - Vodo"),
layout=network_layout*0.7,
)
dev.off()
highlight_filtered <- highlight %>%
filter((regulator %in% zygnema_ID_list) | (target %in% zygnema_ID_list) |
(regulator %in% metabolites) | (target %in% metabolites) |
(regulator %in% unlist(TFs)) | (regulator %in% unlist(TFs)) )
highlight_filtered <- highlight_filtered[1:round(dim(highlight_filtered)[1]*0.001), ] # pick top 1 %
node_list_unique <- highlight_filtered[, 1:2] %>%
gather(key, value, regulator, target) %>%
select(value) %>%
unlist() %>%
as.vector() %>%
unique()
blast_filtered <- blast %>%
filter(zygnema %in% node_list_unique)
top_predictions <- highlight_filtered %>%
slice_head(n=100) %>%
gather(key, value, regulator, target) %>%
select(value) %>%
left_join(blast, by = join_by("value" == "zygnema")) %>%
select(value, use_me) %>%
mutate(use_me = ifelse(is.na(use_me), value, use_me))
node_names <- highlight_filtered %>%
gather(key, gene, regulator, target) %>%
select(gene) %>%
left_join(top_predictions, by = join_by("gene" == "value")) %>%
mutate(vertex_size = if_else(gene %in% top_predictions$value, 3, 1)) %>%
distinct()
network <- graph_from_data_frame(d=highlight_filtered[,1:2], directed=T)
network_layout <- layout_nicely(network)
# Find connected components
components <- clusters(network)
top_components <- components$csize[order(components$csize, decreasing = TRUE)][1:5]
top_indices <- which(components$csize %in% top_components)
V(network)$color <- node_names %>%
filter(gene %in% V(network)$name) %>%
mutate(color = if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][1]]), "#FFBE0B",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][2]]), "#FB5607",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][3]]), "#FF006E",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][4]]), "#8338EC",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][5]]), "#3A86FF",
"#ece4db"
)))))) %>%
select(color) %>%
unlist() %>%
as.vector()
V(network)$labelColor <- node_names %>%
filter(gene %in% V(network)$name) %>%
mutate(color = if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][1]]), "#8f6900",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][2]]), "#a13502",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][3]]), "#66002c",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][4]]), "#460e95",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][5]]), "#003fa3",
"#000000"
)))))) %>%
select(color) %>%
unlist() %>%
as.vector()
edge_colors <- colorRampPalette(c("#e5e5e5", "#000000"))(100)[as.numeric(cut(highlight_filtered$weight*1e10, breaks = 100))]
V(network)$size <- node_names %>%
filter(gene %in% V(network)$name) %>%
select(vertex_size) %>%
unlist() %>%
as.vector()
V(network)$name <- node_names %>%
filter(gene %in% V(network)$name) %>%
select(use_me) %>%
unlist() %>%
as.vector()
#-------------------------------------------------------------------------------
# plot the network
#-------------------------------------------------------------------------------
pdf(file = paste0("~/Desktop/black_magic/highlight_top.001_Vodo_list.pdf"),   # The directory you want to save the file in
width = 15, # The width of the plot in inches
height = 15) # The height of the plot in inches
plot(network,
# vertex
vertex.shape = "circle",
vertex.size = V(network)$size,
vertex.size = 1,
vertex.color = V(network)$color,
vertex.label = V(network)$name,
vertex.label.cex = 0.4,
vertex.frame.color = NA,
vertex.frame.width = 1,
vertex.label.color=V(network)$labelColor,
#edge
#edge.color = "#e5e5e5",
edge.color = edge_colors,
edge.width = 0.5,
dge.arrow.width = 0.1,
edge.arrow.size= 0.1,
edge.lty = "solid",
edge.label = NA,
# other
main = paste0("Zygnema highlight SWING top 0.1% pairs - Vodo"),
layout=network_layout*0.7,
)
dev.off()
#####
cold <- read_tsv("Analyses/SWING/outputs/Zc_cold.tsv") %>%
filter(mean_importance != 0) # filter out those that have no meaning
colnames(cold) <- c("regulator", "target", "weight")
cold <- cold %>%
mutate(pair = paste0(regulator, "-", target))
heat <- read_tsv("Analyses/SWING/outputs/Zc_heat.tsv") %>%
filter(mean_importance != 0) # filter out those that have no meaning
colnames(heat) <- c("regulator", "target", "weight")
heat <- heat %>%
mutate(pair = paste0(regulator, "-", target))
highlight <- read_tsv("Analyses/SWING/outputs/Zc_HL.tsv") %>%
filter(mean_importance != 0) # filter out those that have no meaning
colnames(highlight) <- c("regulator", "target", "weight")
highlight <- highlight %>%
mutate(pair = paste0(regulator, "-", target))
# zygnema
zygnema <- cold %>%
full_join(heat, by = "pair") %>%
full_join(highlight, by = "pair") %>%
mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
mutate(regulator.x = ifelse(is.na(regulator.x), ifelse(is.na(regulator.y), regulator, regulator.y), regulator.x)) %>%
mutate(target.x = ifelse(is.na(target.x), ifelse(is.na(target.y), target, target.y), target.x)) %>%
select(c(regulator.x, target.x, mean_weight)) %>%
distinct() %>%
arrange(desc(mean_weight))
colnames(zygnema) <- c("regulator", "target", "weight")
zygnema <- zygnema %>%
mutate(pair = paste0(regulator, "-",target)) %>%
drop_na() %>%
filter(regulator != target) %>%
distinct(pair, .keep_all = T) %>%
select(c(regulator, target, weight))
zygnema_filtered <- zygnema %>%
filter((regulator %in% zygnema_ID_list) | (target %in% zygnema_ID_list) |
(regulator %in% metabolites) | (target %in% metabolites) |
(regulator %in% unlist(TFs)) | (regulator %in% unlist(TFs)) )
zygnema_filtered <- zygnema_filtered[1:round(dim(zygnema_filtered)[1]*0.001), ] # pick top 1 %
node_list_unique <- zygnema_filtered[, 1:2] %>%
gather(key, value, regulator, target) %>%
select(value) %>%
unlist() %>%
as.vector() %>%
unique()
blast_filtered <- blast %>%
filter(zygnema %in% node_list_unique)
top_predictions <- zygnema_filtered %>%
slice_head(n=100) %>%
gather(key, value, regulator, target) %>%
select(value) %>%
left_join(blast, by = join_by("value" == "zygnema")) %>%
select(value, use_me) %>%
mutate(use_me = ifelse(is.na(use_me), value, use_me))
node_names <- zygnema_filtered %>%
gather(key, gene, regulator, target) %>%
select(gene) %>%
left_join(top_predictions, by = join_by("gene" == "value")) %>%
mutate(vertex_size = if_else(gene %in% top_predictions$value, 3, 1)) %>%
distinct()
network <- graph_from_data_frame(d=zygnema_filtered[,1:2], directed=T)
network_layout <- layout_nicely(network)
# Find connected components
components <- clusters(network)
top_components <- components$csize[order(components$csize, decreasing = TRUE)][1:5]
top_indices <- which(components$csize %in% top_components)
V(network)$color <- node_names %>%
filter(gene %in% V(network)$name) %>%
mutate(color = if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][1]]), "#FFBE0B",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][2]]), "#FB5607",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][3]]), "#FF006E",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][4]]), "#8338EC",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][5]]), "#3A86FF",
"#ece4db"
)))))) %>%
select(color) %>%
unlist() %>%
as.vector()
V(network)$labelColor <- node_names %>%
filter(gene %in% V(network)$name) %>%
mutate(color = if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][1]]), "#8f6900",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][2]]), "#a13502",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][3]]), "#66002c",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][4]]), "#460e95",
if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][5]]), "#003fa3",
"#000000"
)))))) %>%
select(color) %>%
unlist() %>%
as.vector()
edge_colors <- colorRampPalette(c("#e5e5e5", "#000000"))(100)[as.numeric(cut(zygnema_filtered$weight*1e10, breaks = 100))]
V(network)$size <- node_names %>%
filter(gene %in% V(network)$name) %>%
select(vertex_size) %>%
unlist() %>%
as.vector()
V(network)$name <- node_names %>%
filter(gene %in% V(network)$name) %>%
select(use_me) %>%
unlist() %>%
as.vector()
#-------------------------------------------------------------------------------
# plot the network
#-------------------------------------------------------------------------------
pdf(file = paste0("~/Desktop/black_magic/merged_top.001_Vodo_list.pdf"),   # The directory you want to save the file in
width = 15, # The width of the plot in inches
height = 15) # The height of the plot in inches
plot(network,
# vertex
vertex.shape = "circle",
vertex.size = V(network)$size,
vertex.size = 1,
vertex.color = V(network)$color,
vertex.label = V(network)$name,
vertex.label.cex = 0.4,
vertex.frame.color = NA,
vertex.frame.width = 1,
vertex.label.color=V(network)$labelColor,
#edge
#edge.color = "#e5e5e5",
edge.color = edge_colors,
edge.width = 0.5,
dge.arrow.width = 0.1,
edge.arrow.size= 0.1,
edge.lty = "solid",
edge.label = NA,
# other
main = paste0("Zygnema merged SWING top 0.1% pairs - Vodo"),
layout=network_layout*0.7,
)
dev.off()
