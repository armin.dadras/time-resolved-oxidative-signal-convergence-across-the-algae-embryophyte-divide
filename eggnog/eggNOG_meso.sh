#!/bin/bash
#SBATCH -p medium
#SBATCH -C scratch
#SBATCH -N 1
#SBATCH -c 40
#SBATCH --mem=64G
#SBATCH -t 48:00:00
#SBATCH -o job-%J.out
#SBATCH --mail-type=BEGIN,END
#SBATCH --mail-user=armin.dadras@uni-goettingen.de

# add --translate if needed
emapper.py -m diamond --itype proteins --data_dir $HOME/eggnog-mapper-data/ --dmnd_iterate yes -i /scratch/users/dadras/stress_profiling/eggnog/Me1_v2.release.gff3.pep_no_annotation.fasta  --output mesotaenium_endlicherianum --output_dir /scratch/users/dadras/stress_profiling/eggnog/mesotaenium --dbmem --cpu 0 --evalue 1e-10 --sensmode ultra-sensitive --tax_scope 33090 --dmnd_db ~/eggnog-mapper-data/eggnog_proteins_default_viridiplantae.dmnd
