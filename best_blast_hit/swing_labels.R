library(tidyverse)
ath <- read_tsv("Araport11_pep_representative.fa", col_names = F)
head(ath)
meso <- read_tsv("mesotaenium_IDs.tsv", col_names = F)
head(meso)
zyg <- read_tsv("zygnema_IDs.tsv", col_names = F)
head(zyg)
physco <- read_tsv("physco_IDs.tsv", col_names = F)
head(physco)

meso_annotate <- meso %>%
    left_join(ath, by = join_by("X2" == "X1")) %>%
    mutate(use_me = if_else(X2.y == "-", X2, X2.y))
colnames(meso_annotate) <- c("meso", "ath", "symbol", "use_me")
write_tsv(x = meso_annotate, file = "meso_swing_labels_tsv")

physco_annotate <- physco %>%
    left_join(ath, by = join_by("X2" == "X1")) %>%
    mutate(use_me = if_else(X2.y == "-", X2, X2.y))
colnames(physco_annotate) <- c("physco", "ath", "symbol", "use_me")
write_tsv(x = physco_annotate, file = "physco_swing_labels_tsv")

zygnema_annotate <- zyg %>%
    left_join(ath, by = join_by("X2" == "X1")) %>%
    mutate(use_me = if_else(X2.y == "-", X2, X2.y))
colnames(zygnema_annotate) <- c("zygnema", "ath", "symbol", "use_me")
write_tsv(x = zygnema_annotate, file = "zygnema_swing_labels_tsv")
