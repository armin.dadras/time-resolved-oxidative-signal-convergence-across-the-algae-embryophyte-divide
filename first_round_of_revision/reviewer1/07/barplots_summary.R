library(tidyverse)
meso_cold <- read_tsv("mesotaenium_n0_cold.tsv")
meso_heat <- read_tsv("mesotaenium_n0_heat.tsv")
meso_highlight <- read_tsv("mesotaenium_n0_hl.tsv")
meso_hlr <- read_tsv("mesotaenium_n0_hlr.tsv")

zygnema_cold <- read_tsv("zygnema_n0_cold.tsv")
zygnema_heat <- read_tsv("zygnema_n0_heat.tsv")
zygnema_highlight <- read_tsv("zygnema_n0_hl.tsv")
zygnema_hlr <- read_tsv("zygnema_n0_hlr.tsv")

physco_cold <- read_tsv("physcomitrium_n0_cold.tsv")
physco_heat <- read_tsv("physcomitrium_n0_heat.tsv")
physco_highlight <- read_tsv("physcomitrium_n0_hl.tsv")
physco_hlr <- read_tsv("physcomitrium_n0_hlr.tsv")

n0 <- read_tsv("../../../orthofinder/Phylogenetic_Hierarchical_Orthogroups/N0.tsv") %>%
    select(HOG, M_endlicherianum, P_patens, Z_circumcarinatum_SAG698_1b)
hog_GO <- read_tsv("../02/HOG_GO_assignment.tsv")

enriched_cold_HOGs <- n0 %>%
    filter(HOG %in% unique(c(meso_cold$HOG, zygnema_cold$HOG, physco_cold$HOG)))
enriched_heat_HOGs <- n0 %>%
    filter(HOG %in% unique(c(meso_heat$HOG, zygnema_heat$HOG, physco_heat$HOG)))
enriched_highlight_HOGs <- n0 %>%
    filter(HOG %in% unique(c(meso_highlight$HOG, zygnema_highlight$HOG, physco_highlight$HOG)))
enriched_hlr_HOGs <- n0 %>%
    filter(HOG %in% unique(c(meso_hlr$HOG, zygnema_hlr$HOG, physco_hlr$HOG)))

enriched_cold_HOGs <- enriched_cold_HOGs %>%
    mutate(
        presence_pattern = case_when(
            !is.na(M_endlicherianum) & !is.na(P_patens) & !is.na(Z_circumcarinatum_SAG698_1b) ~ "All three",
            !is.na(M_endlicherianum) & !is.na(P_patens) & is.na(Z_circumcarinatum_SAG698_1b) ~ "M and P",
            !is.na(M_endlicherianum) & is.na(P_patens) & !is.na(Z_circumcarinatum_SAG698_1b) ~ "M and Z",
            is.na(M_endlicherianum) & !is.na(P_patens) & !is.na(Z_circumcarinatum_SAG698_1b) ~ "P and Z",
            !is.na(M_endlicherianum) & is.na(P_patens) & is.na(Z_circumcarinatum_SAG698_1b) ~ "Only M",
            is.na(M_endlicherianum) & !is.na(P_patens) & is.na(Z_circumcarinatum_SAG698_1b) ~ "Only P",
            is.na(M_endlicherianum) & is.na(P_patens) & !is.na(Z_circumcarinatum_SAG698_1b) ~ "Only Z",
            TRUE ~ "None"
        )
    ) %>%
    mutate(category = "Cold")
enriched_heat_HOGs <- enriched_heat_HOGs %>%
    mutate(
        presence_pattern = case_when(
            !is.na(M_endlicherianum) & !is.na(P_patens) & !is.na(Z_circumcarinatum_SAG698_1b) ~ "All three",
            !is.na(M_endlicherianum) & !is.na(P_patens) & is.na(Z_circumcarinatum_SAG698_1b) ~ "M and P",
            !is.na(M_endlicherianum) & is.na(P_patens) & !is.na(Z_circumcarinatum_SAG698_1b) ~ "M and Z",
            is.na(M_endlicherianum) & !is.na(P_patens) & !is.na(Z_circumcarinatum_SAG698_1b) ~ "P and Z",
            !is.na(M_endlicherianum) & is.na(P_patens) & is.na(Z_circumcarinatum_SAG698_1b) ~ "Only M",
            is.na(M_endlicherianum) & !is.na(P_patens) & is.na(Z_circumcarinatum_SAG698_1b) ~ "Only P",
            is.na(M_endlicherianum) & is.na(P_patens) & !is.na(Z_circumcarinatum_SAG698_1b) ~ "Only Z",
            TRUE ~ "None"
        )
    ) %>%
    mutate(category = "Heat")
enriched_highlight_HOGs <- enriched_highlight_HOGs %>%
    mutate(
        presence_pattern = case_when(
            !is.na(M_endlicherianum) & !is.na(P_patens) & !is.na(Z_circumcarinatum_SAG698_1b) ~ "All three",
            !is.na(M_endlicherianum) & !is.na(P_patens) & is.na(Z_circumcarinatum_SAG698_1b) ~ "M and P",
            !is.na(M_endlicherianum) & is.na(P_patens) & !is.na(Z_circumcarinatum_SAG698_1b) ~ "M and Z",
            is.na(M_endlicherianum) & !is.na(P_patens) & !is.na(Z_circumcarinatum_SAG698_1b) ~ "P and Z",
            !is.na(M_endlicherianum) & is.na(P_patens) & is.na(Z_circumcarinatum_SAG698_1b) ~ "Only M",
            is.na(M_endlicherianum) & !is.na(P_patens) & is.na(Z_circumcarinatum_SAG698_1b) ~ "Only P",
            is.na(M_endlicherianum) & is.na(P_patens) & !is.na(Z_circumcarinatum_SAG698_1b) ~ "Only Z",
            TRUE ~ "None"
        )
    ) %>%
    mutate(category = "High Light")
enriched_hlr_HOGs <- enriched_hlr_HOGs %>%
    mutate(
        presence_pattern = case_when(
            !is.na(M_endlicherianum) & !is.na(P_patens) & !is.na(Z_circumcarinatum_SAG698_1b) ~ "All three",
            !is.na(M_endlicherianum) & !is.na(P_patens) & is.na(Z_circumcarinatum_SAG698_1b) ~ "M and P",
            !is.na(M_endlicherianum) & is.na(P_patens) & !is.na(Z_circumcarinatum_SAG698_1b) ~ "M and Z",
            is.na(M_endlicherianum) & !is.na(P_patens) & !is.na(Z_circumcarinatum_SAG698_1b) ~ "P and Z",
            !is.na(M_endlicherianum) & is.na(P_patens) & is.na(Z_circumcarinatum_SAG698_1b) ~ "Only M",
            is.na(M_endlicherianum) & !is.na(P_patens) & is.na(Z_circumcarinatum_SAG698_1b) ~ "Only P",
            is.na(M_endlicherianum) & is.na(P_patens) & !is.na(Z_circumcarinatum_SAG698_1b) ~ "Only Z",
            TRUE ~ "None"
        )
    ) %>%
    mutate(category = "High Light recovery")

combined_HOGs <- bind_rows(
    enriched_cold_HOGs,
    enriched_heat_HOGs,
    enriched_highlight_HOGs,
    enriched_hlr_HOGs
)

presence_summary <- combined_HOGs %>%
    group_by(category, presence_pattern) %>%
    summarise(count = n(), .groups = "drop") %>%
    group_by(category) %>%
    mutate(percentage = (count / sum(count)) * 100)

custom_colors <- c(
    "All three" = "#3b5249",  # Blue
    "M and P"   = "#519872",  # Green
    "M and Z"   = "#f2bac9",  # Red
    "P and Z"   = "#f76f8e",  # Orange
    "Only M"    = "#ee6c4d",  # Purple
    "Only P"    = "#7ea3f1",  # Brown
    "Only Z"    = "#ffb703",  # Light Blue
    "None"      = "#0d0e14"   # Light Green
)

p <- ggplot(presence_summary, aes(x = category, y = percentage, fill = presence_pattern)) +
    geom_bar(stat = "identity", position = "stack") +
    scale_y_continuous(labels = scales::percent_format(scale = 1)) +
    scale_fill_manual(values = custom_colors) +
    labs(
        x = "Category",
        y = "Percentage",
        fill = "Presence pattern",
        title = "Distribution of differentially expressged genes across Species using HOGs"
    ) +
    theme_minimal() +
    theme(axis.text.x = element_text(angle = 90))

ggsave(filename = "distribution_of_differentially_expressed_orthologous_across_species_via_HOG.pdf",
       plot = p, width = 20, height = 20, units = "cm", dpi = 600)
