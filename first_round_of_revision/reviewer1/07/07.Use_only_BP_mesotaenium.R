## load the libraries and read the GO data
library(alluvial)
library(tidyverse)
library(clusterProfiler)
GO <- read_tsv("../../../Functional_annotation/mesotaenium_GO.tsv") %>%
    filter(domain == "biological_process")
term2gene <- GO %>% select(term, gene)
term2name <- GO %>% select(term, name)
universe <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/EDA/tables/count_voom_transformed_qsmooth.tsv")
colors <- c("#f8bc97", "#e97032", "#b90736", "#e62026", "#037f34",
            "#00a33a", "#57af31", "#95c351", "#ffd632",
            "#c4d200", "#d29197", "#2e6f82","#797979",
            "#699cd5", "#6ac2cc", "#e9467f", "#00a3e2", "#9d8158",
            "#aa6629", "#784689")
## read the tables
### cold
cold05 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/cold_0.5_vs_standard_growth_0.5_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)
cold1 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/cold_1_vs_standard_growth_1_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)
cold2 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/cold_2_vs_standard_growth_2_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)
cold4 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/cold_4_vs_standard_growth_4_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)
cold6 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/cold_6_vs_standard_growth_6_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)
cold24 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/cold_24_vs_standard_growth_24_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)
## enrichment analysis
enrichment_cold05 <- enricher(cold05$geneID,
                              pAdjustMethod = "BH",
                              TERM2GENE = term2gene,
                              TERM2NAME = term2name,
                              minGSSize = 10,
                              maxGSSize = 500,
                              pvalueCutoff = 0.05,
                              universe = universe$geneID,
                              qvalueCutoff = 0.05)
enriched_terms05 <- enrichment_cold05@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 0.5)
enrichment_cold1 <- enricher(cold1$geneID,
                             pAdjustMethod = "BH",
                             TERM2GENE = term2gene,
                             TERM2NAME = term2name,
                             minGSSize = 10,
                             maxGSSize = 500,
                             pvalueCutoff = 0.05,
                             universe = universe$geneID,
                             qvalueCutoff = 0.05)
enriched_terms1 <- enrichment_cold1@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 1)
enrichment_cold2 <- enricher(cold2$geneID,
                             pAdjustMethod = "BH",
                             TERM2GENE = term2gene,
                             TERM2NAME = term2name,
                             minGSSize = 10,
                             maxGSSize = 500,
                             pvalueCutoff = 0.05,
                             universe = universe$geneID,
                             qvalueCutoff = 0.05)
enriched_terms2 <- enrichment_cold2@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 2)
enrichment_cold4 <- enricher(cold4$geneID,
                             pAdjustMethod = "BH",
                             TERM2GENE = term2gene,
                             TERM2NAME = term2name,
                             minGSSize = 10,
                             maxGSSize = 500,
                             pvalueCutoff = 0.05,
                             universe = universe$geneID,
                             qvalueCutoff = 0.05)
enriched_terms4 <- enrichment_cold4@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 4)
enrichment_cold6 <- enricher(cold6$geneID,
                             pAdjustMethod = "BH",
                             TERM2GENE = term2gene,
                             TERM2NAME = term2name,
                             minGSSize = 10,
                             maxGSSize = 500,
                             pvalueCutoff = 0.05,
                             universe = universe$geneID,
                             qvalueCutoff = 0.05)
enriched_terms6 <- enrichment_cold6@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 6)
enrichment_cold24 <- enricher(cold24$geneID,
                              pAdjustMethod = "BH",
                              TERM2GENE = term2gene,
                              TERM2NAME = term2name,
                              minGSSize = 10,
                              maxGSSize = 500,
                              pvalueCutoff = 0.05,
                              universe = universe$geneID,
                              qvalueCutoff = 0.05)
enriched_terms24 <- enrichment_cold24@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 24)

enriched_terms <- bind_rows(list(enriched_terms05,
                                 enriched_terms1,
                                 enriched_terms2,
                                 enriched_terms4,
                                 enriched_terms6,
                                 enriched_terms24)) %>%
    select(Description, time, Count)

enriched_terms_ndups <- enriched_terms %>%
    group_by(Description) %>%
    mutate(DuplicationCount = n(), Sum_Value = sum(Count)) %>%
    arrange(desc(DuplicationCount), desc(Sum_Value)) %>%
    select(Description) %>%
    unlist() %>%
    as.vector() %>%
    unique()
enriched_terms_ndups <- enriched_terms_ndups[1:20]


enriched_terms <- enriched_terms %>%
    filter(Description %in% enriched_terms_ndups)
enriched_terms$time <- factor(enriched_terms$time, levels = c("0.5", "1", "2", "4", "6", "24"))

# Specify the desired time points
desired_time_points <- c("0.5", "1", "2", "4", "6", "24")
# Create a data frame with all combinations of ID and Time
enriched_terms_all_combinations <- expand.grid(Description = unique(enriched_terms$Description), time = desired_time_points)
# Left join the original data frame with all_combinations
result_df <- enriched_terms_all_combinations %>%
    left_join(enriched_terms, by = c("Description", "time")) %>%
    mutate(Count = ifelse(is.na(Count), 1, Count))  # Replace missing values with a very small value
write_tsv(file = "mesoteanium_GO_cold.tsv", x = result_df)
# Input is a dataframe with 3 columns: (1) categories (2) time-variable (3) value
pdf(file = paste0("mesotaenium_DGEA_alluvial_cold.pdf"),   # The directory you want to save the file in
    width = 20, # The width of the plot in inches
    height = 10)
alluvial_ts(result_df,
            wave = 0.2, 
            ygap = 20,
            col = colors,
            plotdir = 'down',
            alpha= 0.7,
            grid = T,
            grid.lwd = 1,
            xmargin = 2,
            lab.cex = 1,
            rankup = T,
            axis.col = "black",
            xlab = 'Hours after start of the experiment',
            ylab = 'GO Terms (counts as width)',
            border = F,
            axis.cex = 1,
            leg.mode = F,
            leg.cex = 0.7,
            #leg.col='white', 
            title = "M. endlicherianum top 20 largest enriched BP GO terms under cold treatment")
dev.off()
### heat
heat05 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/heat_0.5_vs_standard_growth_0.5_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)
heat1 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/heat_1_vs_standard_growth_1_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)
heat2 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/heat_2_vs_standard_growth_2_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)
heat4 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/heat_4_vs_standard_growth_4_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)
heat6 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/heat_6_vs_standard_growth_6_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)
heat24 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/heat_24_vs_standard_growth_24_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)
## enrichment analysis
enrichment_heat05 <- enricher(heat05$geneID,
                              pAdjustMethod = "BH",
                              TERM2GENE = term2gene,
                              TERM2NAME = term2name,
                              minGSSize = 10,
                              maxGSSize = 500,
                              pvalueCutoff = 0.05,
                              universe = universe$geneID,
                              qvalueCutoff = 0.05)
enriched_terms05 <- enrichment_heat05@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 0.5)
enrichment_heat1 <- enricher(heat1$geneID,
                             pAdjustMethod = "BH",
                             TERM2GENE = term2gene,
                             TERM2NAME = term2name,
                             minGSSize = 10,
                             maxGSSize = 500,
                             pvalueCutoff = 0.05,
                             universe = universe$geneID,
                             qvalueCutoff = 0.05)
enriched_terms1 <- enrichment_heat1@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 1)
enrichment_heat2 <- enricher(heat2$geneID,
                             pAdjustMethod = "BH",
                             TERM2GENE = term2gene,
                             TERM2NAME = term2name,
                             minGSSize = 10,
                             maxGSSize = 500,
                             pvalueCutoff = 0.05,
                             universe = universe$geneID,
                             qvalueCutoff = 0.05)
enriched_terms2 <- enrichment_heat2@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 2)
enrichment_heat4 <- enricher(heat4$geneID,
                             pAdjustMethod = "BH",
                             TERM2GENE = term2gene,
                             TERM2NAME = term2name,
                             minGSSize = 10,
                             maxGSSize = 500,
                             pvalueCutoff = 0.05,
                             universe = universe$geneID,
                             qvalueCutoff = 0.05)
enriched_terms4 <- enrichment_heat4@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 4)
enrichment_heat6 <- enricher(heat6$geneID,
                             pAdjustMethod = "BH",
                             TERM2GENE = term2gene,
                             TERM2NAME = term2name,
                             minGSSize = 10,
                             maxGSSize = 500,
                             pvalueCutoff = 0.05,
                             universe = universe$geneID,
                             qvalueCutoff = 0.05)
enriched_terms6 <- enrichment_heat6@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 6)
enrichment_heat24 <- enricher(heat24$geneID,
                              pAdjustMethod = "BH",
                              TERM2GENE = term2gene,
                              TERM2NAME = term2name,
                              minGSSize = 10,
                              maxGSSize = 500,
                              pvalueCutoff = 0.05,
                              universe = universe$geneID,
                              qvalueCutoff = 0.05)
enriched_terms24 <- enrichment_heat24@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 24)

enriched_terms <- bind_rows(list(enriched_terms05,
                                 enriched_terms1,
                                 enriched_terms2,
                                 enriched_terms4,
                                 enriched_terms6,
                                 enriched_terms24)) %>%
    select(Description, time, Count)

enriched_terms_ndups <- enriched_terms %>%
    group_by(Description) %>%
    mutate(DuplicationCount = n(), Sum_Value = sum(Count)) %>%
    arrange(desc(DuplicationCount), desc(Sum_Value)) %>%
    select(Description) %>%
    unlist() %>%
    as.vector() %>%
    unique()
enriched_terms_ndups <- enriched_terms_ndups[1:20]


enriched_terms <- enriched_terms %>%
    filter(Description %in% enriched_terms_ndups)
enriched_terms$time <- factor(enriched_terms$time, levels = c("0.5", "1", "2", "4", "6", "24"))

# Specify the desired time points
desired_time_points <- c("0.5", "1", "2", "4", "6", "24")
# Create a data frame with all combinations of ID and Time
enriched_terms_all_combinations <- expand.grid(Description = unique(enriched_terms$Description), time = desired_time_points)
# Left join the original data frame with all_combinations
result_df <- enriched_terms_all_combinations %>%
    left_join(enriched_terms, by = c("Description", "time")) %>%
    mutate(Count = ifelse(is.na(Count), 1, Count))  # Replace missing values with a very small value

enriched_terms$time <- factor(enriched_terms$time, levels = c("0.5", "1", "2", "4", "6", "24"))
write_tsv(file = "mesoteanium_GO_heat.tsv", x = result_df)

# Input is a dataframe with 3 columns: (1) categories (2) time-variable (3) value
pdf(file = paste0("mesotaenium_DGEA_alluvial_heat.pdf"),   # The directory you want to save the file in
    width = 20, # The width of the plot in inches
    height = 10)
alluvial_ts(result_df,
            wave = 0.2, 
            ygap = 20,
            col = colors,
            plotdir = 'down',
            alpha= 0.7,
            grid = T,
            grid.lwd = 1,
            xmargin = 2,
            lab.cex = 1,
            rankup = T,
            axis.col = "black",
            xlab = 'Hours after start of the experiment',
            ylab = 'GO Terms (counts as width)',
            border = F,
            axis.cex = 1,
            leg.mode = F,
            leg.cex = 0.7,
            #leg.col='white', 
            title = "M. endlicherianum top 20 largest enriched BP GO terms under heat treatment")
dev.off()
### high light recovery
hl_r025 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/high_light_r_0.25_vs_high_light_s_6_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)
hl_r05 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/high_light_r_0.5_vs_high_light_s_6_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)
hl_r1 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/high_light_r_1_vs_high_light_s_6_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)
hl_r2 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/high_light_r_2_vs_high_light_s_6_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)
hl_r4 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/high_light_r_4_vs_high_light_s_6_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)

## enrichment analysis
enrichment_hl_r025 <- enricher(hl_r025$geneID,
                               pAdjustMethod = "BH",
                               TERM2GENE = term2gene,
                               TERM2NAME = term2name,
                               minGSSize = 10,
                               maxGSSize = 500,
                               pvalueCutoff = 0.05,
                               universe = universe$geneID,
                               qvalueCutoff = 0.05)
enriched_terms025 <- enrichment_hl_r025@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 0.25)
enrichment_hl_r05 <- enricher(hl_r05$geneID,
                              pAdjustMethod = "BH",
                              TERM2GENE = term2gene,
                              TERM2NAME = term2name,
                              minGSSize = 10,
                              maxGSSize = 500,
                              pvalueCutoff = 0.05,
                              universe = universe$geneID,
                              qvalueCutoff = 0.05)
enriched_terms05 <- enrichment_hl_r05@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 0.5)
enrichment_hl_r1 <- enricher(hl_r1$geneID,
                             pAdjustMethod = "BH",
                             TERM2GENE = term2gene,
                             TERM2NAME = term2name,
                             minGSSize = 10,
                             maxGSSize = 500,
                             pvalueCutoff = 0.05,
                             universe = universe$geneID,
                             qvalueCutoff = 0.05)
enriched_terms1 <- enrichment_hl_r1@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 1)
enrichment_hl_r2 <- enricher(hl_r2$geneID,
                             pAdjustMethod = "BH",
                             TERM2GENE = term2gene,
                             TERM2NAME = term2name,
                             minGSSize = 10,
                             maxGSSize = 500,
                             pvalueCutoff = 0.05,
                             universe = universe$geneID,
                             qvalueCutoff = 0.05)
enriched_terms2 <- enrichment_hl_r2@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 2)
enrichment_hl_r4 <- enricher(hl_r4$geneID,
                             pAdjustMethod = "BH",
                             TERM2GENE = term2gene,
                             TERM2NAME = term2name,
                             minGSSize = 10,
                             maxGSSize = 500,
                             pvalueCutoff = 0.05,
                             universe = universe$geneID,
                             qvalueCutoff = 0.05)
enriched_terms4 <- enrichment_hl_r4@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 4)

enriched_terms <- bind_rows(list(enriched_terms025,
                                 enriched_terms05,
                                 enriched_terms1,
                                 enriched_terms2,
                                 enriched_terms4)) %>%
    select(Description, time, Count)

enriched_terms_ndups <- enriched_terms %>%
    group_by(Description) %>%
    mutate(DuplicationCount = n(), Sum_Value = sum(Count)) %>%
    arrange(desc(DuplicationCount), desc(Sum_Value)) %>%
    select(Description) %>%
    unlist() %>%
    as.vector() %>%
    unique()
enriched_terms_ndups <- enriched_terms_ndups[1:20]


enriched_terms <- enriched_terms %>%
    filter(Description %in% enriched_terms_ndups)
enriched_terms$time <- factor(enriched_terms$time, levels = c("0.25", "0.5", "1", "2", "4"))

# Specify the desired time points
desired_time_points <- c("0.25", "0.5", "1", "2", "4")
# Create a data frame with all combinations of ID and Time
enriched_terms_all_combinations <- expand.grid(Description = unique(enriched_terms$Description), time = desired_time_points)
# Left join the original data frame with all_combinations
result_df <- enriched_terms_all_combinations %>%
    left_join(enriched_terms, by = c("Description", "time")) %>%
    mutate(Count = ifelse(is.na(Count), 1, Count))  # Replace missing values with a very small value
enriched_terms$time <- factor(enriched_terms$time, levels = c("0.25", "0.5", "1", "2", "4"))
write_tsv(file = "mesoteanium_GO_hlr.tsv", x = result_df)

# Input is a dataframe with 3 columns: (1) categories (2) time-variable (3) value
pdf(file = paste0("mesotaenium_DGEA_alluvial_highlight_recovery.pdf"),   # The directory you want to save the file in
    width = 20, # The width of the plot in inches
    height = 10)
alluvial_ts(result_df,
            wave = 0.2, 
            ygap = 20,
            col = colors,
            plotdir = 'down',
            alpha= 0.7,
            grid = T,
            grid.lwd = 1,
            xmargin = 2,
            lab.cex = 1,
            rankup = T,
            axis.col = "black",
            xlab = 'Hours after start of the experiment',
            ylab = 'GO Terms (counts as width)',
            border = F,
            axis.cex = 1,
            leg.mode = F,
            leg.cex = 0.7,
            #leg.col='white', 
            title = "M. endlicherianum top 20 largest enriched BP GO terms under recovery after 6 hours of high light treatment")
dev.off()
### high light
hl025 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/high_light_s_0.25_vs_standard_growth_0_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)
hl05 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/high_light_s_0.5_vs_standard_growth_0.5_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)
hl2 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/high_light_s_2_vs_standard_growth_2_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)
hl4 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/high_light_s_4_vs_standard_growth_4_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)
hl6 <- read_tsv("../../../mesotaenium_endlicherianum/Analyses/DGEA/tables/high_light_s_6_vs_standard_growth_6_logFC_ebFit.tsv") %>%
    filter(adj.P.Val <= 0.05) %>%
    filter(abs(logFC) >= 1)

## enrichment analysis
enrichment_hl025 <- enricher(hl025$geneID,
                             pAdjustMethod = "BH",
                             TERM2GENE = term2gene,
                             TERM2NAME = term2name,
                             minGSSize = 10,
                             maxGSSize = 500,
                             pvalueCutoff = 0.05,
                             universe = universe$geneID,
                             qvalueCutoff = 0.05)
enriched_terms025 <- enrichment_hl025@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 0.25)
enrichment_hl05 <- enricher(hl05$geneID,
                            pAdjustMethod = "BH",
                            TERM2GENE = term2gene,
                            TERM2NAME = term2name,
                            minGSSize = 10,
                            maxGSSize = 500,
                            pvalueCutoff = 0.05,
                            universe = universe$geneID,
                            qvalueCutoff = 0.05)
enriched_terms05 <- enrichment_hl05@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 0.5)
enrichment_hl2 <- enricher(hl2$geneID,
                           pAdjustMethod = "BH",
                           TERM2GENE = term2gene,
                           TERM2NAME = term2name,
                           minGSSize = 10,
                           maxGSSize = 500,
                           pvalueCutoff = 0.05,
                           universe = universe$geneID,
                           qvalueCutoff = 0.05)
enriched_terms2 <- enrichment_hl2@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 2)
enrichment_hl4 <- enricher(hl4$geneID,
                           pAdjustMethod = "BH",
                           TERM2GENE = term2gene,
                           TERM2NAME = term2name,
                           minGSSize = 10,
                           maxGSSize = 500,
                           pvalueCutoff = 0.05,
                           universe = universe$geneID,
                           qvalueCutoff = 0.05)
enriched_terms4 <- enrichment_hl4@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 4)
enrichment_hl6 <- enricher(hl6$geneID,
                           pAdjustMethod = "BH",
                           TERM2GENE = term2gene,
                           TERM2NAME = term2name,
                           minGSSize = 10,
                           maxGSSize = 500,
                           pvalueCutoff = 0.05,
                           universe = universe$geneID,
                           qvalueCutoff = 0.05)
enriched_terms6 <- enrichment_hl6@result %>%
    filter(p.adjust <= 0.05) %>%
    mutate(time = 6)

enriched_terms <- bind_rows(list(enriched_terms025,
                                 enriched_terms05,
                                 enriched_terms2,
                                 enriched_terms4,
                                 enriched_terms6)) %>%
    select(Description, time, Count)


enriched_terms_ndups <- enriched_terms %>%
    group_by(Description) %>%
    mutate(DuplicationCount = n(), Sum_Value = sum(Count)) %>%
    arrange(desc(DuplicationCount), desc(Sum_Value)) %>%
    select(Description) %>%
    unlist() %>%
    as.vector() %>%
    unique()
enriched_terms_ndups <- enriched_terms_ndups[1:20]

enriched_terms <- enriched_terms %>%
    filter(Description %in% enriched_terms_ndups)
enriched_terms$time <- factor(enriched_terms$time, levels = c("0.25", "0.5", "2", "4", "6"))

# Specify the desired time points
desired_time_points <- c("0.25", "0.5", "2", "4", "6")
# Create a data frame with all combinations of ID and Time
enriched_terms_all_combinations <- expand.grid(Description = unique(enriched_terms$Description), time = desired_time_points)
# Left join the original data frame with all_combinations
result_df <- enriched_terms_all_combinations %>%
    left_join(enriched_terms, by = c("Description", "time")) %>%
    mutate(Count = ifelse(is.na(Count), 1, Count))  # Replace missing values with a very small value

enriched_terms$time <- factor(enriched_terms$time, levels = c("0.25", "0.5", "2", "4", "6"))
write_tsv(file = "mesoteanium_GO_highlight.tsv", x = result_df)

# Input is a dataframe with 3 columns: (1) categories (2) time-variable (3) value
pdf(file = paste0("mesotaenium_DGEA_alluvial_highlight.pdf"),   # The directory you want to save the file in
    width = 20, # The width of the plot in inches
    height = 10)
alluvial_ts(result_df,
            wave = 0.2, 
            ygap = 20,
            col = colors,
            plotdir = 'down',
            alpha= 0.7,
            grid = T,
            grid.lwd = 1,
            xmargin = 2,
            lab.cex = 1,
            rankup = T,
            axis.col = "black",
            xlab = 'Hours after start of the experiment',
            ylab = 'GO Terms (counts as width)',
            border = F,
            axis.cex = 1,
            leg.mode = F,
            leg.cex = 0.7,
            #leg.col='white', 
            title = "M. endlicherianum top 20 largest enriched BP GO terms under high light treatment")
dev.off()

diff_meso_genes_cold <- unique(c(cold05$geneID, cold1$geneID, cold2$geneID, cold4$geneID, cold6$geneID, cold24$geneID))
diff_meso_genes_heat <- unique(c(heat05$geneID, heat1$geneID, heat2$geneID, heat4$geneID, heat6$geneID, heat24$geneID))
diff_meso_genes_hl <- unique(c(hl025$geneID, hl05$geneID, hl2$geneID, hl4$geneID, hl6$geneID))
diff_meso_genes_hlr <- unique(c(hl_r025$geneID, hl_r05$geneID, hl_r1$geneID, hl_r2$geneID, hl_r4$geneID))
n0_cold <- read_tsv("../../../orthofinder/Phylogenetic_Hierarchical_Orthogroups/N0.tsv") %>%
    select(HOG, M_endlicherianum) %>%
    separate_rows(M_endlicherianum, sep = ", ") %>%
    filter(M_endlicherianum %in% diff_meso_genes_cold) %>%
    select(HOG) %>%
    distinct()

n0_heat <- read_tsv("../../../orthofinder/Phylogenetic_Hierarchical_Orthogroups/N0.tsv") %>%
    select(HOG, M_endlicherianum) %>%
    separate_rows(M_endlicherianum, sep = ", ") %>%
    filter(M_endlicherianum %in% diff_meso_genes_heat) %>%
    select(HOG) %>%
    distinct()

n0_hl <- read_tsv("../../../orthofinder/Phylogenetic_Hierarchical_Orthogroups/N0.tsv") %>%
    select(HOG, M_endlicherianum) %>%
    separate_rows(M_endlicherianum, sep = ", ") %>%
    filter(M_endlicherianum %in% diff_meso_genes_hl) %>%
    select(HOG) %>%
    distinct()

n0_hlr <- read_tsv("../../../orthofinder/Phylogenetic_Hierarchical_Orthogroups/N0.tsv") %>%
    select(HOG, M_endlicherianum) %>%
    separate_rows(M_endlicherianum, sep = ", ") %>%
    filter(M_endlicherianum %in% diff_meso_genes_hlr) %>%
    select(HOG) %>%
    distinct()

write_tsv(x = n0_cold, file = "mesotaenium_n0_cold.tsv")
write_tsv(x = n0_heat, file = "mesotaenium_n0_heat.tsv")
write_tsv(x = n0_hl, file = "mesotaenium_n0_hl.tsv")
write_tsv(x = n0_hlr, file = "mesotaenium_n0_hlr.tsv")
