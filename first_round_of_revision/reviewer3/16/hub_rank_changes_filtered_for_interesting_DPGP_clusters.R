library(tidyverse)

connectivity <- read_tsv("connectivity_ranked_sorted_by_conserved_network_HOGs_filtered_for_DPGP_interesting_clusters.tsv") %>%
    mutate(rank_all = row_number())
connectivity_only_transcripts <- read_tsv("ONLY_TRANSCRIPTS_connectivity_ranked_sorted_by_conserved_network_HOGs_filtered_for_DPGP_interesting_clusters.tsv") %>%
    mutate(rank_all = row_number())
merged_data <- connectivity %>%
    select(names, rank_all) %>%
    rename(rank_all_connectivity = rank_all) %>%
    inner_join(connectivity_only_transcripts %>% select(names, rank_all),
               by = "names") %>%
    rename(rank_all_transcripts = rank_all) %>%
    mutate(category = case_when(
        rank_all_connectivity <= 100 | rank_all_transcripts <= 100 ~ "Top 100",
        TRUE ~ "Rest"
    )) %>%
    mutate(rank_change = rank_all_connectivity - rank_all_transcripts)

p <- ggplot(merged_data, aes(x = rank_all_connectivity, y = rank_all_transcripts, color = category)) +
    geom_point(alpha = 0.7) +
    geom_abline(slope = 1, intercept = 0, linetype = "dashed", color = "grey") +
    labs(
        x = "Rank in Genes and Metabolites GRN network",
        y = "Rank in Genes GRN network",
        color = "Category",
        title = "Rank Changes Between Reconstructed GRNs (Genes&Metabolites vs. Only Genes) - Filtered for intersting DPGP clusters"
    ) +
    theme_minimal()

ggsave(filename = "outputs/comparison_of_HOGs_ranks_GRN_between_two_reconstructions_with_and_without_metabolites_filtered_for_interesting_DPGP_clusters.pdf",
       plot = p, width = 20, height = 20, dpi = 600, units = "cm")

summary <- merged_data %>%
    group_by(category) %>%
    summarize(
        mean_change = mean(rank_change, na.rm = TRUE),
        median_change = median(rank_change, na.rm = TRUE),
        max_change = max(rank_change, na.rm = TRUE),
        min_change = min(rank_change, na.rm = TRUE)
    )

write_tsv(file = "outputs/ranke_change_summary_filtered_for_interesting_DPGP_clusters.tsv", x = summary)

merged_data <- merged_data %>%
    mutate(category = factor(category, levels = c("Top 100", "Rest"))) %>% # Ensure "Top 100" appears first
    arrange(category, desc(rank_all_connectivity)) %>% # Sort by category and rank
    mutate(x_axis = row_number()) # Create x-axis index

# Step 2: Create bar plot
p <- ggplot(merged_data, aes(x = x_axis, y = rank_change, fill = category)) +
    geom_col() + # Bar plot
    scale_fill_manual(values = c("Top 100" = "steelblue", "Rest" = "lightgrey")) + # Custom colors
    labs(
        x = "Entries (sorted by category and rank)",
        y = "Rank Change (Genes&Metabolite Network - Genes Network)",
        fill = "Category",
        title = "Rank Changes Between Reconstructed GRNs (Genes&Metabolites vs. Only Genes) - Filtered for intersting DPGP clusters"
    ) +
    theme_minimal() +
    theme(
        axis.text.x = element_blank(), # Hide x-axis labels
        axis.ticks.x = element_blank()
    ) +
    geom_hline(yintercept = 0, linetype = "dashed", color = "black") # Add horizontal line at 0

ggsave(filename = "outputs/bar_plot_comparison_of_HOGs_ranks_GRN_between_two_reconstructions_with_and_without_metabolites_filtered_for_interesting_DPGP_clusters.pdf",
       plot = p, width = 20, height = 20, dpi = 600, units = "cm")

