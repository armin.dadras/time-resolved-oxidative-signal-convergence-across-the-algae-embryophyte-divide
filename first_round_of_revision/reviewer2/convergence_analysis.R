####
# Setup
####
library(tidyverse)
library(igraph)
library(scales)
library(cowplot)
#-------------------------------------------------------------------------------
# load the data
#-------------------------------------------------------------------------------
meso_heat <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING_only_transcripts/tables/Me_heat_only_transcripts.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
meso_cold <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING_only_transcripts/tables/Me_cold_only_transcripts.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
meso_highlight <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING_only_transcripts/tables/Me_HL_only_transcripts.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
zygnema_cold <- read_tsv("../../zygnema_circumcarinatum_1b/Analyses/SWING_only_transcripts/tables/Zc_cold.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
zygnema_heat <- read_tsv("../../zygnema_circumcarinatum_1b/Analyses/SWING_only_transcripts/tables/Zc_heat.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
zygnema_highlight <- read_tsv("../../zygnema_circumcarinatum_1b/Analyses/SWING_only_transcripts/tables/Zc_highlight.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
physco_cold <- read_tsv("../../physcomitrium_patens/Analyses/SWING_only_transcripts/tables/Pp_cold.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
physco_heat <- read_tsv("../../physcomitrium_patens/Analyses/SWING_only_transcripts/tables/Pp_heat.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
physco_highlight <- read_tsv("../../physcomitrium_patens/Analyses/SWING_only_transcripts/tables/Pp_highlight.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
# Functional annotation
blast_meso <- read_tsv("../../best_blast_hit/meso_swing_labels.tsv", col_names = T) %>%
    distinct(meso, .keep_all = TRUE)
blast_physco <- read_tsv("../../best_blast_hit/physco_swing_labels.tsv", col_names = T) %>%
    distinct(physco, .keep_all = TRUE)
blast_zygnema <- read_tsv("../../best_blast_hit/zygnema_swing_labels.tsv", col_names = T) %>%
    distinct(zygnema, .keep_all = TRUE)

ath_carotenoid <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/inputs/unique_ATH/carotenoid.txt", col_names = F)
ath_apocarotenoid <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/inputs/unique_ATH/apocarotenoid.txt", col_names = F)
ath_cold <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/inputs/unique_ATH/cold.txt", col_names = F)
ath_heat <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/inputs/unique_ATH/heat.txt", col_names = F)
ath_highlight <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/inputs/unique_ATH/high_light.txt", col_names = F)
ath_oxidative <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/inputs/unique_ATH/oxidative.txt", col_names = F)
#-------------------------------------------------------------------------------
# Merging data using inner join
#-------------------------------------------------------------------------------
# mesotaenium
meso <- meso_heat %>%
    full_join(meso_cold, by = "pair") %>%
    full_join(meso_highlight, by = "pair") %>%
    mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
    mutate(regulator.x = ifelse(is.na(regulator.x), ifelse(is.na(regulator.y), regulator, regulator.y), regulator.x)) %>%
    mutate(target.x = ifelse(is.na(target.x), ifelse(is.na(target.y), target, target.y), target.x)) %>%
    select(c(regulator.x, target.x, mean_weight)) %>%
    distinct() %>%
    arrange(desc(mean_weight))
colnames(meso) <- c("regulator", "target", "weight")
meso <- meso %>%
    left_join(blast_meso, by = join_by("regulator" == "meso")) %>%
    select(c(use_me, target, weight)) %>%
    drop_na(use_me)
colnames(meso) <- c("regulator", "target", "weight")
meso <- meso %>%
    left_join(blast_meso, by = join_by("target" == "meso")) %>%
    select(c(regulator, use_me, weight))
colnames(meso) <- c("regulator", "target", "weight")
meso <- meso %>%
    mutate(pair = paste0(regulator, "-",target)) %>%
    drop_na()
# zygnema
zygnema <- zygnema_cold %>%
    full_join(zygnema_heat, by = "pair") %>%
    full_join(zygnema_highlight, by = "pair") %>%
    mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
    mutate(regulator.x = ifelse(is.na(regulator.x), ifelse(is.na(regulator.y), regulator, regulator.y), regulator.x)) %>%
    mutate(target.x = ifelse(is.na(target.x), ifelse(is.na(target.y), target, target.y), target.x)) %>%
    select(c(regulator.x, target.x, mean_weight)) %>%
    distinct() %>%
    arrange(desc(mean_weight))
colnames(zygnema) <- c("regulator", "target", "weight")
zygnema <- zygnema %>%
    left_join(blast_zygnema, by = join_by("regulator" == "zygnema")) %>%
    select(c(use_me, target, weight)) %>%
    drop_na(use_me)
colnames(zygnema) <- c("regulator", "target", "weight")
zygnema <- zygnema %>%
    left_join(blast_zygnema, by = join_by("target" == "zygnema")) %>%
    select(c(regulator, use_me, weight))
colnames(zygnema) <- c("regulator", "target", "weight")
zygnema <- zygnema %>%
    mutate(pair = paste0(regulator, "-",target)) %>%
    drop_na()
# physco
physco <- physco_cold %>%
    full_join(physco_heat, by = "pair") %>%
    full_join(physco_highlight, by = "pair") %>%
    mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
    mutate(regulator.x = ifelse(is.na(regulator.x), ifelse(is.na(regulator.y), regulator, regulator.y), regulator.x)) %>%
    mutate(target.x = ifelse(is.na(target.x), ifelse(is.na(target.y), target, target.y), target.x)) %>%
    select(c(regulator.x, target.x, mean_weight)) %>%
    distinct() %>%
    arrange(desc(mean_weight))
colnames(physco) <- c("regulator", "target", "weight")
physco <- physco %>%
    left_join(blast_physco, by = join_by("regulator" == "physco")) %>%
    select(c(use_me, target, weight)) %>%
    drop_na(use_me)
colnames(physco) <- c("regulator", "target", "weight")
physco <- physco %>%
    left_join(blast_physco, by = join_by("target" == "physco")) %>%
    select(c(regulator, use_me, weight))
colnames(physco) <- c("regulator", "target", "weight")
physco <- physco %>%
    mutate(pair = paste0(regulator, "-",target)) %>%
    drop_na()
# merge all
swing_all <- meso %>%
    inner_join(zygnema, by = "pair") %>%
    inner_join(physco, by = "pair")  %>%
    mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
    mutate(pair = paste0(regulator, "-", "target")) %>%
    distinct(pair, .keep_all = TRUE) %>%
    select(c(regulator, target, mean_weight))
colnames(swing_all) <- c("regulator", "target", "weight")
swing_all <- swing_all %>%
    filter(regulator != target) %>%
    arrange(desc(weight))
#-------------------------------------------------------------------------------
# Connectivity analysis
#-------------------------------------------------------------------------------
network_all <- graph_from_data_frame(d=swing_all, directed=T)
degree_all_all <- degree(network_all, normalized = TRUE, loops = FALSE, mode = "all")
degree_all_in <- degree(network_all, normalized = TRUE, loops = FALSE, mode = "in")
hits_scores_all <- authority_score(network_all, weights = swing_all$weight)$vector
alpha_centrality_scores_all <- alpha_centrality(network_all, weights = swing_all$weight)
eigen_centrality_scores_all <- eigen_centrality(network_all, directed = T, weights = swing_all$weight)$vector
betweenness_scores_all <- betweenness(network_all, directed = T, normalized = T, weights = swing_all$weight)
closeness_all_all <- closeness(network_all, mode = "all", normalized = T, weights = swing_all$weight)
closeness_all_in <- closeness(network_all, mode = "in", normalized = T, weights = swing_all$weight)
closeness_all_out <- closeness(network_all, mode = "out", normalized = T, weights = swing_all$weight)
# Combine the metrics into a data frame
metrics_all <- tibble(
    Node = V(network_all)$name,
    AllDegree = degree_all_all,
    InDegree = degree_all_in,
    Authority = hits_scores_all,
    AlphaCentrality = alpha_centrality_scores_all,
    EigenCentrality = eigen_centrality_scores_all,
    Betweenness = betweenness_scores_all,
    Closeness_all = closeness_all_all,
    Closeness_in = closeness_all_in,
    Closeness_out = closeness_all_out,
)
metrics_all <- metrics_all %>%
    arrange(desc(AllDegree)) %>%
    mutate(HubStatus = if_else(row_number() <= 50, "Hub", "NotHub"))
network_meso <- graph_from_data_frame(d=meso[,1:3], directed=T) 
degree_meso_all <- degree(network_meso, normalized = TRUE, loops = FALSE, mode = "all")
degree_meso_in <- degree(network_meso, normalized = TRUE, loops = FALSE, mode = "in")
hits_scores_meso <- authority_score(network_meso, weights = network_meso$weight)$vector
alpha_centrality_scores_meso <- alpha_centrality(network_meso, weights = network_meso$weight)
eigen_centrality_scores_meso <- eigen_centrality(network_meso, directed = T, weights = network_meso$weight)$vector
betweenness_scores_meso <- betweenness(network_meso, directed = T, normalized = T, weights = network_meso$weight)
closeness_meso_all <- closeness(network_meso, mode = "all", normalized = T, weights = network_meso$weight)
closeness_meso_in <- closeness(network_meso, mode = "in", normalized = T, weights = network_meso$weight)
closeness_meso_out <- closeness(network_meso, mode = "out", normalized = T, weights = network_meso$weight)
# Combine the metrics into a data frame
metrics_meso <- tibble(
    Node = V(network_meso)$name,
    AllDegree = degree_meso_all,
    InDegree = degree_meso_in,
    Authority = hits_scores_meso,
    AlphaCentrality = alpha_centrality_scores_meso,
    EigenCentrality = eigen_centrality_scores_meso,
    Betweenness = betweenness_scores_meso,
    Closeness_all = closeness_meso_all,
    Closeness_in = closeness_meso_in,
    Closeness_out = closeness_meso_out,
)
metrics_meso <- metrics_meso %>%
    arrange(desc(AllDegree)) %>%
    mutate(HubStatus = if_else(row_number() <= 50, "Hub", "NotHub"))
network_physco <- graph_from_data_frame(d=physco[,1:3], directed=T) 
degree_physco_all <- degree(network_physco, normalized = TRUE, loops = FALSE, mode = "all")
degree_physco_in <- degree(network_physco, normalized = TRUE, loops = FALSE, mode = "in")
hits_scores_physco <- authority_score(network_physco, weights = network_physco$weight)$vector
alpha_centrality_scores_physco <- alpha_centrality(network_physco, weights = network_physco$weight)
eigen_centrality_scores_physco <- eigen_centrality(network_physco, directed = T, weights = network_physco$weight)$vector
betweenness_scores_physco <- betweenness(network_physco, directed = T, normalized = T, weights = network_physco$weight)
closeness_physco_all <- closeness(network_physco, mode = "all", normalized = T, weights = network_physco$weight)
closeness_physco_in <- closeness(network_physco, mode = "in", normalized = T, weights = network_physco$weight)
closeness_physco_out <- closeness(network_physco, mode = "out", normalized = T, weights = network_physco$weight)
# Combine the metrics into a data frame
metrics_physco <- tibble(
    Node = V(network_physco)$name,
    AllDegree = degree_physco_all,
    InDegree = degree_physco_in,
    Authority = hits_scores_physco,
    AlphaCentrality = alpha_centrality_scores_physco,
    EigenCentrality = eigen_centrality_scores_physco,
    Betweenness = betweenness_scores_physco,
    Closeness_all = closeness_physco_all,
    Closeness_in = closeness_physco_in,
    Closeness_out = closeness_physco_out,
)
metrics_physco <- metrics_physco %>%
    arrange(desc(AllDegree)) %>%
    mutate(HubStatus = if_else(row_number() <= 50, "Hub", "NotHub"))
network_zygnema <- graph_from_data_frame(d=zygnema[,1:3], directed=T) 
degree_zygnema_all <- degree(network_zygnema, normalized = TRUE, loops = FALSE, mode = "all")
degree_zygnema_in <- degree(network_zygnema, normalized = TRUE, loops = FALSE, mode = "in")
hits_scores_zygnema <- authority_score(network_zygnema, weights = network_zygnema$weight)$vector
alpha_centrality_scores_zygnema <- alpha_centrality(network_zygnema, weights = network_zygnema$weight)
eigen_centrality_scores_zygnema <- eigen_centrality(network_zygnema, directed = T, weights = network_zygnema$weight)$vector
betweenness_scores_zygnema <- betweenness(network_zygnema, directed = T, normalized = T, weights = network_zygnema$weight)
closeness_zygnema_all <- closeness(network_zygnema, mode = "all", normalized = T, weights = network_zygnema$weight)
closeness_zygnema_in <- closeness(network_zygnema, mode = "in", normalized = T, weights = network_zygnema$weight)
closeness_zygnema_out <- closeness(network_zygnema, mode = "out", normalized = T, weights = network_zygnema$weight)
# Combine the metrics into a data frame
metrics_zygnema <- tibble(
    Node = V(network_zygnema)$name,
    AllDegree = degree_zygnema_all,
    InDegree = degree_zygnema_in,
    Authority = hits_scores_zygnema,
    AlphaCentrality = alpha_centrality_scores_zygnema,
    EigenCentrality = eigen_centrality_scores_zygnema,
    Betweenness = betweenness_scores_zygnema,
    Closeness_all = closeness_zygnema_all,
    Closeness_in = closeness_zygnema_in,
    Closeness_out = closeness_zygnema_out,
)
metrics_zygnema <- metrics_zygnema %>%
    arrange(desc(AllDegree)) %>%
    mutate(HubStatus = if_else(row_number() <= 50, "Hub", "NotHub"))
# Save files
write_tsv(x = metrics_all, file = "blast_all_network_metrics.tsv")
write_tsv(x = metrics_meso, file = "blast_meso_network_metrics.tsv")
write_tsv(x = metrics_physco, file = "blast_physco_network_metrics.tsv")
write_tsv(x = metrics_zygnema, file = "blast_zygnema_network_metrics.tsv")
#-------------------------------------------------------------------------------
# Visualization
#-------------------------------------------------------------------------------
p0 <- ggplot(metrics_all, aes(x = factor(HubStatus), y = AllDegree)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "AllDegree",
         x = "Hub?",
         y = "AllDegree") +
    theme_minimal()
p1 <- ggplot(metrics_all, aes(x = factor(HubStatus), y = InDegree)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "InDegree",
         x = "Hub?",
         y = "InDegree") +
    theme_minimal()
p2 <- ggplot(metrics_all, aes(x = factor(HubStatus), y = Authority)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "Authority",
         x = "Hub?",
         y = "Authority") +
    theme_minimal()
p3 <- ggplot(metrics_all, aes(x = factor(HubStatus), y = AlphaCentrality)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "AlphaCentrality",
         x = "Hub?",
         y = "AlphaCentrality") +
    theme_minimal()
p4 <- ggplot(metrics_all, aes(x = factor(HubStatus), y = EigenCentrality)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "EigenCentrality",
         x = "Hub?",
         y = "EigenCentrality") +
    theme_minimal()
p5 <- ggplot(metrics_all, aes(x = factor(HubStatus), y = Betweenness)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "Betweenness",
         x = "Hub?",
         y = "Betweenness") +
    theme_minimal()
p6 <- ggplot(metrics_all, aes(x = factor(HubStatus), y = Closeness_all)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "Closeness_all",
         x = "Hub?",
         y = "Closeness_all") +
    theme_minimal()
p7 <- ggplot(metrics_all, aes(x = factor(HubStatus), y = Closeness_in)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "Closeness_in",
         x = "Hub?",
         y = "Closeness_in") +
    theme_minimal()
p8 <- ggplot(metrics_all, aes(x = factor(HubStatus), y = Closeness_out)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "Closeness_out",
         x = "Hub?",
         y = "Closeness_out") +
    theme_minimal()
plots <- plot_grid(p0, p1, p2, p3, p4, p5, p6, p7, p8, nrow = 2, ncol = 4, align = "hv", labels = "AUTO")
ggsave(filename = "Network_metrics_all.pdf", plots, width = 40, height = 20, units = "cm", )
p0 <- ggplot(metrics_meso, aes(x = factor(HubStatus), y = AllDegree)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "AllDegree",
         x = "Hub?",
         y = "AllDegree") +
    theme_minimal()
p1 <- ggplot(metrics_meso, aes(x = factor(HubStatus), y = InDegree)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "InDegree",
         x = "Hub?",
         y = "InDegree") +
    theme_minimal()
p2 <- ggplot(metrics_meso, aes(x = factor(HubStatus), y = Authority)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "Authority",
         x = "Hub?",
         y = "Authority") +
    theme_minimal()
p3 <- ggplot(metrics_meso, aes(x = factor(HubStatus), y = AlphaCentrality)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "AlphaCentrality",
         x = "Hub?",
         y = "AlphaCentrality") +
    theme_minimal()
p4 <- ggplot(metrics_meso, aes(x = factor(HubStatus), y = EigenCentrality)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "EigenCentrality",
         x = "Hub?",
         y = "EigenCentrality") +
    theme_minimal()
p5 <- ggplot(metrics_meso, aes(x = factor(HubStatus), y = Betweenness)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "Betweenness",
         x = "Hub?",
         y = "Betweenness") +
    theme_minimal()
p6 <- ggplot(metrics_meso, aes(x = factor(HubStatus), y = Closeness_all)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "Closeness_all",
         x = "Hub?",
         y = "Closeness_all") +
    theme_minimal()
p7 <- ggplot(metrics_meso, aes(x = factor(HubStatus), y = Closeness_in)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "Closeness_in",
         x = "Hub?",
         y = "Closeness_in") +
    theme_minimal()
p8 <- ggplot(metrics_meso, aes(x = factor(HubStatus), y = Closeness_out)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "Closeness_out",
         x = "Hub?",
         y = "Closeness_out") +
    theme_minimal()
plots <- plot_grid(p0, p1, p2, p3, p4, p5, p6, p7, p8, nrow = 2, ncol = 4, align = "hv", labels = "AUTO")
ggsave(filename = "Network_metrics_meso.pdf", plots, width = 40, height = 20, units = "cm", )
p0 <- ggplot(metrics_physco, aes(x = factor(HubStatus), y = AllDegree)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "AllDegree",
         x = "Hub?",
         y = "AllDegree") +
    theme_minimal()
p1 <- ggplot(metrics_physco, aes(x = factor(HubStatus), y = InDegree)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "InDegree",
         x = "Hub?",
         y = "InDegree") +
    theme_minimal()
p2 <- ggplot(metrics_physco, aes(x = factor(HubStatus), y = Authority)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "Authority",
         x = "Hub?",
         y = "Authority") +
    theme_minimal()
p3 <- ggplot(metrics_physco, aes(x = factor(HubStatus), y = AlphaCentrality)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "AlphaCentrality",
         x = "Hub?",
         y = "AlphaCentrality") +
    theme_minimal()
p4 <- ggplot(metrics_physco, aes(x = factor(HubStatus), y = EigenCentrality)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "EigenCentrality",
         x = "Hub?",
         y = "EigenCentrality") +
    theme_minimal()
p5 <- ggplot(metrics_physco, aes(x = factor(HubStatus), y = Betweenness)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "Betweenness",
         x = "Hub?",
         y = "Betweenness") +
    theme_minimal()
p6 <- ggplot(metrics_physco, aes(x = factor(HubStatus), y = Closeness_all)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "Closeness_all",
         x = "Hub?",
         y = "Closeness_all") +
    theme_minimal()
p7 <- ggplot(metrics_physco, aes(x = factor(HubStatus), y = Closeness_in)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "Closeness_in",
         x = "Hub?",
         y = "Closeness_in") +
    theme_minimal()
p8 <- ggplot(metrics_physco, aes(x = factor(HubStatus), y = Closeness_out)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "Closeness_out",
         x = "Hub?",
         y = "Closeness_out") +
    theme_minimal()
plots <- plot_grid(p0, p1, p2, p3, p4, p5, p6, p7, p8, nrow = 2, ncol = 4, align = "hv", labels = "AUTO")
ggsave(filename = "Network_metrics_physco.pdf", plots, width = 40, height = 20, units = "cm", )
p0 <- ggplot(metrics_zygnema, aes(x = factor(HubStatus), y = AllDegree)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "AllDegree",
         x = "Hub?",
         y = "AllDegree") +
    theme_minimal()
p1 <- ggplot(metrics_zygnema, aes(x = factor(HubStatus), y = InDegree)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "InDegree",
         x = "Hub?",
         y = "InDegree") +
    theme_minimal()
p2 <- ggplot(metrics_zygnema, aes(x = factor(HubStatus), y = Authority)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "Authority",
         x = "Hub?",
         y = "Authority") +
    theme_minimal()
p3 <- ggplot(metrics_zygnema, aes(x = factor(HubStatus), y = AlphaCentrality)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "AlphaCentrality",
         x = "Hub?",
         y = "AlphaCentrality") +
    theme_minimal()
p4 <- ggplot(metrics_zygnema, aes(x = factor(HubStatus), y = EigenCentrality)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "EigenCentrality",
         x = "Hub?",
         y = "EigenCentrality") +
    theme_minimal()
p5 <- ggplot(metrics_zygnema, aes(x = factor(HubStatus), y = Betweenness)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "Betweenness",
         x = "Hub?",
         y = "Betweenness") +
    theme_minimal()
p6 <- ggplot(metrics_zygnema, aes(x = factor(HubStatus), y = Closeness_all)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "Closeness_all",
         x = "Hub?",
         y = "Closeness_all") +
    theme_minimal()
p7 <- ggplot(metrics_zygnema, aes(x = factor(HubStatus), y = Closeness_in)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "Closeness_in",
         x = "Hub?",
         y = "Closeness_in") +
    theme_minimal()
p8 <- ggplot(metrics_zygnema, aes(x = factor(HubStatus), y = Closeness_out)) +
    geom_violin(trim = FALSE) +
    geom_boxplot(width = 0.1, fill = "white", outlier.color = "red") +
    labs(title = "Closeness_out",
         x = "Hub?",
         y = "Closeness_out") +
    theme_minimal()
plots <- plot_grid(p0, p1, p2, p3, p4, p5, p6, p7, p8, nrow = 2, ncol = 4, align = "hv", labels = "AUTO")
ggsave(filename = "Network_metrics_zygnema.pdf", plots, width = 40, height = 20, units = "cm", )

