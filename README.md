This is a repository to collect codes and results of our research available here: https://doi.org/10.1101/2024.03.11.584470

File Hierarhcy:

1. `Functional_annotation`: Includes the tables and codes used to perform functional annotation for *M. endlicherianum*, *Z. circumcariantum*, and *P. patens*. This includes the results of InterProScan and eggNOG-mapper as well as an aggregated annotation via both tools.

2. `NCBI_ppatens_zcircumcarinatum`: Includes files used to deposit *Z. circumcariantum* and *P. patens* to the SRA NCBI.

3. `best_blast_hit`: Includes the results of best blast hit of proteins of *M. endlicherianum*, *Z. circumcariantum*, and *P. patens* against Araport11 database.

4. `carotenoid_analyse`: Includes tables as well as visualization of metabolites and respective genes related to Carotenoid analyses.

5. `comparative_analyses`: Includes any analyses, visualization, scripts, tables, and plots that we performed to compare genes, metabolites, or networks across species using comparative OMIC methods.

6. `eggnog`: Includes inputs, scripts, and outputs used to annotate the protein sequences of *M. endlicherianum*, *Z. circumcariantum*, and *P. patens* using eggNOG-mapper.

7. `first_round_of_revision`: Includes files, results, and scripts that were used to prepare the answers for the first round of the review.

8. `InterProScan`: Includes inputs, scripts, and outputs used to annotate the protein sequences of *M. endlicherianum*, *Z. circumcariantum*, and *P. patens* using InterProScan.

9. `mesotaenium_endlicherianum`: Includes R scripts, inputs, datasets, results, tables, plots, and various analysis that we did for *M. endlicherianum* which includes inputs and outputs of the following analyses/tools: `kallisto`, `Quality control using FastQC and MultiQC`, `Differential Gene expression analysis (DGEA)`, `Dirichlet Process Gaussian Process (DPGP) for Co-expression network analysis`, `Exploratory data analyses (EDA)`, `Sliding Window Inference for Network Generation (SWING) for Gene regulatory network inference (GRN)`, `SWING_only_transcripts which is the same as before but without the metabolite featuers`, `Weighted Gene Co-expression Network Analysis (WGCNA)`.

10. `metabolite_measurement`: The results of metabolite measurements as tables.

11. `orthofinder`: All results of the Orthogroup analysis using Orthofinder.

12. `physcomitrium_patens`: Includes R scripts, inputs, datasets, results, tables, plots, and various analysis that we did for *P. patens* which includes inputs and outputs of the following analyses/tools: `kallisto`, `Quality control using FastQC and MultiQC`, `Differential Gene expression analysis (DGEA)`, `Dirichlet Process Gaussian Process (DPGP) for Co-expression network analysis`, `Exploratory data analyses (EDA)`, `Sliding Window Inference for Network Generation (SWING) for Gene regulatory network inference (GRN)`, `SWING_only_transcripts which is the same as before but without the metabolite featuers`, `Weighted Gene Co-expression Network Analysis (WGCNA)`.

13. `stress_profiling_phylogeny`: The inputs, results, and scripts for phylogeny analysis that was done using blast, mafft, and iqtree for selected Hierarchical Orthogroups.

14. `tapscan`: The results of transcription factor analysis performed with a tool called TapScan.

12. `zygnema_circumcarinatum_1b`: Includes R scripts, inputs, datasets, results, tables, plots, and various analysis that we did for *Z. circumcarinatum* which includes inputs and outputs of the following analyses/tools: `kallisto`, `Quality control using FastQC and MultiQC`, `Differential Gene expression analysis (DGEA)`, `Dirichlet Process Gaussian Process (DPGP) for Co-expression network analysis`, `Exploratory data analyses (EDA)`, `Sliding Window Inference for Network Generation (SWING) for Gene regulatory network inference (GRN)`, `SWING_only_transcripts which is the same as before but without the metabolite featuers`, `Weighted Gene Co-expression Network Analysis (WGCNA)`.

13. `study_design_*`: Comma-separeted files that captures the metadata of our experiment design, information about sequenced reads, and measured metabolites.