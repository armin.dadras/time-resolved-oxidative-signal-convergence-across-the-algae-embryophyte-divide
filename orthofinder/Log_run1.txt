2023-11-20 11:22:11 : Started OrthoFinder version 2.5.5
Command Line: /home/armin/test_tools/OrthoFinder/orthofinder.py -f /data/armin/orthofinder_stress_profiling/raw_prot_seqs/primary_transcripts -S diamond -M msa -A mafft -T fasttree -t 200 -a 6 -y -n run_1

WorkingDirectory_Base: /data/armin/orthofinder_stress_profiling/raw_prot_seqs/primary_transcripts/OrthoFinder/Results_run_1/WorkingDirectory/

Species used: 
0: A_agrestis_oxf.fa
1: A_filiculoides.fa
2: A_thaliana.fa
3: B_distachyon.fa
4: C_braunii.fa
5: C_melkonianii.fa
6: C_reinhardtii.fa
7: Closterium_sp_NIES_67.fa
8: K_nitens.fa
9: M_endlicherianum.fa
10: M_polymorpha.fa
11: M_viride.fa
12: O_lucimarinus.fa
13: O_sativa.fa
14: P_coloniale.fa
15: P_margaritaceum.fa
16: P_patens.fa
17: S_lycopersicum.fa
18: S_moellendorffii.fa
19: S_muscicola.fa
20: Z_circumcarinatum_SAG698_1a.fa
21: Z_circumcarinatum_SAG698_1b.fa
22: Z_circumcarinatum_UTEX1559.fa
23: Z_circumcarinatum_UTEX1560.fa
24: Z_mays.fa

FN_Orthogroups: /data/armin/orthofinder_stress_profiling/raw_prot_seqs/primary_transcripts/OrthoFinder/Results_run_1/WorkingDirectory/clusters_OrthoFinder_I1.5.txt_id_pairs.txt
WorkingDirectory_Trees: /data/armin/orthofinder_stress_profiling/raw_prot_seqs/primary_transcripts/OrthoFinder/Results_run_1/WorkingDirectory/
2023-11-20 17:16:26 : OrthoFinder run completed
