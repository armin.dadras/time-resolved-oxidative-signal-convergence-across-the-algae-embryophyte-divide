stat_smooth(data = filtered_cold, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = ets, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
stat_smooth(data = filtered_heat, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = ets, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
stat_smooth(data = filtered_high_light_s, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = ets, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
ggtitle(paste0(organism, " - ", "FvFm values")) +
scale_fill_manual(values = my_colors_treatments) +
scale_color_manual(values = my_colors_treatments) +
scale_y_continuous(limits = c(0, 1), breaks = seq(0, 1, 0.2)) +
scale_x_continuous(breaks = unique(all_data[["time"]])) +
geom_text(x = Inf, y = Inf, hjust = 1, vjust = 1, label = poly_formula_standard_growth, size = 4, color = "black", aes(label = paste("Formula: ", poly_formula_standard_growth))) +
geom_text(x = Inf, y = Inf, hjust = 1, vjust = 4, label = poly_formula_cold, size = 4, color = "black", aes(label = paste("Formula: ", poly_formula_cold))) +
geom_text(x = Inf, y = Inf, hjust = 1, vjust = 7, label = poly_formula_heat, size = 4, color = "black", aes(label = paste("Formula: ", poly_formula_heat))) +
geom_text(x = Inf, y = Inf, hjust = 1, vjust = 10, label = poly_formula_high_light_s, size = 4, color = "black", aes(label = paste("Formula: ", poly_formula_high_light_s))) +
labs(y = "FvFm", x = "Time") +
theme_bw() +
theme(axis.text.x = element_text(angle = 90), legend.position = "none")
plots <- list()
i <- 1
for (organism in species) {
all_data <- get(organism) %>%
filter(!(time > 6))
reg_standard_growth <- 1
reg_cold <- 4
reg_heat <- 7
reg_high_light <- 10
filtered_standard_growth <- all_data %>%
filter(treatment == "standard_growth" | (time == 0 )) %>%
filter(!(time > 6))
filtered_cold <- all_data %>%
filter(treatment == "cold" | (time == 0 )) %>%
filter(!(time > 6))
filtered_heat <- all_data %>%
filter(treatment == "heat" | (time == 0)) %>%
filter(!(time > 6))
filtered_high_light_s <- all_data %>%
filter(treatment == "high_light_s" | (time == 0 )) %>%
filter(!(time > 6))
filtered_standard_growth$condition <- factor(filtered_standard_growth$condition, levels = unique(filtered_standard_growth$condition))
filtered_standard_growth$treatment <- factor(rep("standard_growth", dim(filtered_standard_growth)[1]))
filtered_cold$condition <- factor(filtered_cold$condition, levels = unique(filtered_cold$condition))
filtered_cold$treatment <- factor(rep("cold", dim(filtered_cold)[1]))
filtered_heat$condition <- factor(filtered_heat$condition, levels = unique(filtered_heat$condition))
filtered_heat$treatment <- factor(rep("heat", dim(filtered_heat)[1]))
filtered_high_light_s$condition <- factor(filtered_high_light_s$condition, levels = unique(filtered_high_light_s$condition))
filtered_high_light_s$treatment <- factor(rep("high_light_s", dim(filtered_high_light_s)[1]))
# Extract polynomial regression coefficients
poly_coefs <- coef(lm((get("FvFm")) ~ poly(time, 2), data = filtered_standard_growth))
# Create a string representing the formula
poly_formula_standard_growth <- sprintf("Standard growth: y = %.2f + %.2fx + %.2fx^2", poly_coefs[1], poly_coefs[2], poly_coefs[3])
# Extract polynomial regression coefficients
poly_coefs <- coef(lm((get("FvFm")) ~ poly(time, 2), data = filtered_cold))
# Create a string representing the formula
poly_formula_cold <- sprintf("Cold: y = %.2f + %.2fx + %.2fx^2", poly_coefs[1], poly_coefs[2], poly_coefs[3])
# Extract polynomial regression coefficients
poly_coefs <- coef(lm((get("FvFm")) ~ poly(time, 2), data = filtered_heat))
# Create a string representing the formula
poly_formula_heat <- sprintf("Heat: y = %.2f + %.2fx + %.2fx^2", poly_coefs[1], poly_coefs[2], poly_coefs[3])
# Extract polynomial regression coefficients
poly_coefs <- coef(lm((get("FvFm")) ~ poly(time, 2), data = filtered_high_light_s))
# Create a string representing the formula
poly_formula_high_light_s <- sprintf("High light: y = %.2f + %.2fx + %.2fx^2", poly_coefs[1], poly_coefs[2], poly_coefs[3])
p <- ggplot(all_data, aes(time, get("FvFm"))) +
geom_point(aes(fill=treatment), shape=21, size=2, alpha = .5) +
stat_smooth(data = filtered_standard_growth, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = lm, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
stat_smooth(data = filtered_cold, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = lm, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
stat_smooth(data = filtered_heat, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = lm, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
stat_smooth(data = filtered_high_light_s, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = lm, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
ggtitle(paste0(organism, " - ", "FvFm values")) +
scale_fill_manual(values = my_colors_treatments) +
scale_color_manual(values = my_colors_treatments) +
scale_y_continuous(limits = c(0, 1), breaks = seq(0, 1, 0.2)) +
scale_x_continuous(breaks = unique(all_data[["time"]])) +
geom_text(x = Inf, y = Inf, hjust = 1, vjust = 1, label = poly_formula_standard_growth, size = 4, color = "black", aes(label = paste("Formula: ", poly_formula_standard_growth))) +
geom_text(x = Inf, y = Inf, hjust = 1, vjust = 4, label = poly_formula_cold, size = 4, color = "black", aes(label = paste("Formula: ", poly_formula_cold))) +
geom_text(x = Inf, y = Inf, hjust = 1, vjust = 7, label = poly_formula_heat, size = 4, color = "black", aes(label = paste("Formula: ", poly_formula_heat))) +
geom_text(x = Inf, y = Inf, hjust = 1, vjust = 10, label = poly_formula_high_light_s, size = 4, color = "black", aes(label = paste("Formula: ", poly_formula_high_light_s))) +
labs(y = "FvFm", x = "Time") +
theme_bw() +
theme(axis.text.x = element_text(angle = 90), legend.position = "none")
plots[[i]] <- p
i <- i + 1
}
plots[[3]]
# Step 1: Call the pdf command to start the plot
pdf(file = "plots/FvFm_scatter_up_to_six_hours.pdf",   # The directory you want to save the file in
width = 15, # The width of the plot in inches
height = 9) # The height of the plot in inches
# Step 2: Create the plot with R code
p <- plot_grid(plots[[1]], plots[[2]], plots[[3]],plots[[4]], plots[[5]], plots[[6]], plots[[7]],
plots[[8]], plots[[9]], plots[[10]], plots[[11]], plots[[12]], labels = "AUTO")
# Step 2: Create the plot with R code
p <- plot_grid(plots, labels = "AUTO")
# Step 2: Create the plot with R code
p <- plot_grid(unlist(plots), labels = "AUTO")
print(p)
# Step 2: Create the plot with R code
p <- plot_grid(plots[[1]], plots[[2]], plots[[3]], labels = "AUTO")
print(p)
# Step 3: Run dev.off() to create the file!
dev.off()
# Step 3: Run dev.off() to create the file!
dev.off()
# Step 3: Run dev.off() to create the file!
dev.off()
# Step 1: Call the pdf command to start the plot
pdf(file = "plots/FvFm_scatter_up_to_six_hours.pdf",   # The directory you want to save the file in
width = 15, # The width of the plot in inches
height = 9) # The height of the plot in inches
# Step 2: Create the plot with R code
p <- plot_grid(plots[[1]], plots[[2]], plots[[3]], labels = "AUTO")
print(p)
# Step 3: Run dev.off() to create the file!
dev.off()
# Step 1: Call the pdf command to start the plot
pdf(file = "plots/FvFm_scatter_up_to_six_hours.pdf",   # The directory you want to save the file in
width = 15, # The width of the plot in inches
height = 9) # The height of the plot in inches
# Step 2: Create the plot with R code
p <- plot_grid(plots[[1]], plots[[2]], plots[[3]], labels = "AUTO", ncol = 1)
print(p)
# Step 3: Run dev.off() to create the file!
dev.off()
# Step 1: Call the pdf command to start the plot
pdf(file = "plots/FvFm_scatter_up_to_six_hours.pdf",   # The directory you want to save the file in
width = 3.5, # The width of the plot in inches
height = 9) # The height of the plot in inches
# Step 2: Create the plot with R code
p <- plot_grid(plots[[1]], plots[[2]], plots[[3]], labels = "AUTO", ncol = 1)
print(p)
# Step 3: Run dev.off() to create the file!
dev.off()
################################################################################
########## Plot
################################################################################
# Step 2: Create the plot with R code
#par(mfcol = c(3,4)) # rows, columns
#par(mar = c(2.7,1.2,2.7,1.2))
species <- c("mesotaenium", "physcomitrium", "zygnema")
treatments <- c("standard_growth", "cold", "heat", "high_light_s")
plots <- list()
i <- 1
for (organism in species) {
all_data <- get(organism) %>%
filter(!(time > 6))
reg_standard_growth <- 1
reg_cold <- 4
reg_heat <- 7
reg_high_light <- 10
filtered_standard_growth <- all_data %>%
filter(treatment == "standard_growth" | (time == 0 )) %>%
filter(!(time > 6))
filtered_cold <- all_data %>%
filter(treatment == "cold" | (time == 0 )) %>%
filter(!(time > 6))
filtered_heat <- all_data %>%
filter(treatment == "heat" | (time == 0)) %>%
filter(!(time > 6))
filtered_high_light_s <- all_data %>%
filter(treatment == "high_light_s" | (time == 0 )) %>%
filter(!(time > 6))
filtered_standard_growth$condition <- factor(filtered_standard_growth$condition, levels = unique(filtered_standard_growth$condition))
filtered_standard_growth$treatment <- factor(rep("standard_growth", dim(filtered_standard_growth)[1]))
filtered_cold$condition <- factor(filtered_cold$condition, levels = unique(filtered_cold$condition))
filtered_cold$treatment <- factor(rep("cold", dim(filtered_cold)[1]))
filtered_heat$condition <- factor(filtered_heat$condition, levels = unique(filtered_heat$condition))
filtered_heat$treatment <- factor(rep("heat", dim(filtered_heat)[1]))
filtered_high_light_s$condition <- factor(filtered_high_light_s$condition, levels = unique(filtered_high_light_s$condition))
filtered_high_light_s$treatment <- factor(rep("high_light_s", dim(filtered_high_light_s)[1]))
# Extract polynomial regression coefficients
poly_coefs <- coef(lm((get("FvFm")) ~ poly(time, 2), data = filtered_standard_growth))
# Create a string representing the formula
poly_formula_standard_growth <- sprintf("Standard growth: y = %.2f + %.2fx + %.2fx^2", poly_coefs[1], poly_coefs[2], poly_coefs[3])
# Extract polynomial regression coefficients
poly_coefs <- coef(lm((get("FvFm")) ~ poly(time, 2), data = filtered_cold))
# Create a string representing the formula
poly_formula_cold <- sprintf("Cold: y = %.2f + %.2fx + %.2fx^2", poly_coefs[1], poly_coefs[2], poly_coefs[3])
# Extract polynomial regression coefficients
poly_coefs <- coef(lm((get("FvFm")) ~ poly(time, 2), data = filtered_heat))
# Create a string representing the formula
poly_formula_heat <- sprintf("Heat: y = %.2f + %.2fx + %.2fx^2", poly_coefs[1], poly_coefs[2], poly_coefs[3])
# Extract polynomial regression coefficients
poly_coefs <- coef(lm((get("FvFm")) ~ poly(time, 2), data = filtered_high_light_s))
# Create a string representing the formula
poly_formula_high_light_s <- sprintf("High light: y = %.2f + %.2fx + %.2fx^2", poly_coefs[1], poly_coefs[2], poly_coefs[3])
p <- ggplot(all_data, aes(time, get("FvFm"))) +
geom_point(aes(fill=treatment), shape=21, size=2, alpha = .5) +
stat_smooth(data = filtered_standard_growth, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = lm, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
stat_smooth(data = filtered_cold, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = lm, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
stat_smooth(data = filtered_heat, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = lm, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
stat_smooth(data = filtered_high_light_s, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = lm, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
ggtitle(paste0(organism, " - ", "FvFm values")) +
scale_fill_manual(values = my_colors_treatments) +
scale_color_manual(values = my_colors_treatments) +
scale_y_continuous(limits = c(0, 1), breaks = seq(0, 1, 0.2)) +
scale_x_continuous(breaks = unique(all_data[["time"]])) +
geom_text(x = Inf, y = Inf, hjust = 1, vjust = 1, label = poly_formula_standard_growth, size = 2, color = "black", aes(label = paste("Formula: ", poly_formula_standard_growth))) +
geom_text(x = Inf, y = Inf, hjust = 1, vjust = 3, label = poly_formula_cold, size = 2, color = "black", aes(label = paste("Formula: ", poly_formula_cold))) +
geom_text(x = Inf, y = Inf, hjust = 1, vjust = 5, label = poly_formula_heat, size = 2, color = "black", aes(label = paste("Formula: ", poly_formula_heat))) +
geom_text(x = Inf, y = Inf, hjust = 1, vjust = 7, label = poly_formula_high_light_s, size = 2, color = "black", aes(label = paste("Formula: ", poly_formula_high_light_s))) +
labs(y = "FvFm", x = "Time") +
theme_bw() +
theme(axis.text.x = element_text(angle = 90), legend.position = "none")
plots[[i]] <- p
i <- i + 1
}
# Step 1: Call the pdf command to start the plot
pdf(file = "plots/FvFm_scatter_up_to_six_hours.pdf",   # The directory you want to save the file in
width = 3.5, # The width of the plot in inches
height = 9) # The height of the plot in inches
# Step 2: Create the plot with R code
p <- plot_grid(plots[[1]], plots[[2]], plots[[3]], labels = "AUTO", ncol = 1)
print(p)
# Step 3: Run dev.off() to create the file!
dev.off()
ggplot(filtered, aes(treatment, get("FvFm"))) +
ggdist::stat_halfeye(adjust = .5, width = .5, .width = 0, justification = -.4) +
geom_boxplot(width = .3, outlier.shape = NA, fill= my_colors_treatments,  alpha = 0.3) +
geom_jitter(aes(fill=treatment), shape=21, size=2, alpha = .3, width = 0.1) +
ggtitle(paste0("Aggregated FvFm", " - ", organism)) +
labs(y = "FvFm") +
scale_fill_manual(values = my_colors_treatments) +
scale_color_manual(values = my_colors_treatments) +
scale_y_continuous(limits = c(0, 1), breaks = seq(0, 1, 0.2)) +
theme_bw() +
theme(axis.text.x = "none",
legend.position = "none")
plots <- list()
i <- 1
for (organism in species) {
filtered <- get(organism)
filtered$condition <- factor(filtered$condition, levels = unique(filtered$condition))
filtered$treatment <- factor(filtered$treatment, levels = c("standard_growth",
"cold",
"heat",
"high_light_s",
"high_light_r"))
p <- ggplot(filtered, aes(treatment, get("FvFm"))) +
ggdist::stat_halfeye(adjust = .5, width = .5, .width = 0, justification = -.4) +
geom_boxplot(width = .3, outlier.shape = NA, fill= my_colors_treatments,  alpha = 0.3) +
geom_jitter(aes(fill=treatment), shape=21, size=2, alpha = .3, width = 0.1) +
ggtitle(paste0("Aggregated FvFm", " - ", organism)) +
labs(y = "FvFm") +
scale_fill_manual(values = my_colors_treatments) +
scale_color_manual(values = my_colors_treatments) +
scale_y_continuous(limits = c(0, 1), breaks = seq(0, 1, 0.2)) +
theme_bw() +
theme(axis.text.x = "none",
legend.position = "none")
plots[[i]] <- p
i <- i + 1
}
plots[[1]]
plots <- list()
i <- 1
for (organism in species) {
all_data <- get(organism) %>%
filter(!(time > 6))
reg_standard_growth <- 1
reg_cold <- 4
reg_heat <- 7
reg_high_light <- 10
filtered_standard_growth <- all_data %>%
filter(treatment == "standard_growth" | (time == 0 )) %>%
filter(!(time > 6))
filtered_cold <- all_data %>%
filter(treatment == "cold" | (time == 0 )) %>%
filter(!(time > 6))
filtered_heat <- all_data %>%
filter(treatment == "heat" | (time == 0)) %>%
filter(!(time > 6))
filtered_high_light_s <- all_data %>%
filter(treatment == "high_light_s" | (time == 0 )) %>%
filter(!(time > 6))
filtered_standard_growth$condition <- factor(filtered_standard_growth$condition, levels = unique(filtered_standard_growth$condition))
filtered_standard_growth$treatment <- factor(rep("standard_growth", dim(filtered_standard_growth)[1]))
filtered_cold$condition <- factor(filtered_cold$condition, levels = unique(filtered_cold$condition))
filtered_cold$treatment <- factor(rep("cold", dim(filtered_cold)[1]))
filtered_heat$condition <- factor(filtered_heat$condition, levels = unique(filtered_heat$condition))
filtered_heat$treatment <- factor(rep("heat", dim(filtered_heat)[1]))
filtered_high_light_s$condition <- factor(filtered_high_light_s$condition, levels = unique(filtered_high_light_s$condition))
filtered_high_light_s$treatment <- factor(rep("high_light_s", dim(filtered_high_light_s)[1]))
# Extract polynomial regression coefficients
poly_coefs <- coef(lm((get("FvFm")) ~ poly(time, 2), data = filtered_standard_growth))
# Create a string representing the formula
poly_formula_standard_growth <- sprintf("Standard growth: y = %.2f + %.2fx + %.2fx^2", poly_coefs[1], poly_coefs[2], poly_coefs[3])
# Extract polynomial regression coefficients
poly_coefs <- coef(lm((get("FvFm")) ~ poly(time, 2), data = filtered_cold))
# Create a string representing the formula
poly_formula_cold <- sprintf("Cold: y = %.2f + %.2fx + %.2fx^2", poly_coefs[1], poly_coefs[2], poly_coefs[3])
# Extract polynomial regression coefficients
poly_coefs <- coef(lm((get("FvFm")) ~ poly(time, 2), data = filtered_heat))
# Create a string representing the formula
poly_formula_heat <- sprintf("Heat: y = %.2f + %.2fx + %.2fx^2", poly_coefs[1], poly_coefs[2], poly_coefs[3])
# Extract polynomial regression coefficients
poly_coefs <- coef(lm((get("FvFm")) ~ poly(time, 2), data = filtered_high_light_s))
# Create a string representing the formula
poly_formula_high_light_s <- sprintf("High light: y = %.2f + %.2fx + %.2fx^2", poly_coefs[1], poly_coefs[2], poly_coefs[3])
p <- ggplot(all_data, aes(time, get("FvFm"))) +
geom_point(aes(fill=treatment), shape=21, size=2, alpha = .5) +
stat_smooth(data = filtered_standard_growth, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = lm, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
stat_smooth(data = filtered_cold, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = lm, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
stat_smooth(data = filtered_heat, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = lm, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
stat_smooth(data = filtered_high_light_s, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = lm, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
ggtitle(paste0(organism, " - ", "FvFm values")) +
scale_fill_manual(values = my_colors_treatments) +
scale_color_manual(values = my_colors_treatments) +
scale_y_continuous(limits = c(0, 1), breaks = seq(0, 1, 0.2)) +
scale_x_continuous(breaks = unique(all_data[["time"]])) +
geom_text(x = Inf, y = Inf, hjust = 1, vjust = 1, label = poly_formula_standard_growth, size = 2, color = "black", aes(label = paste("Formula: ", poly_formula_standard_growth))) +
geom_text(x = Inf, y = Inf, hjust = 1, vjust = 3, label = poly_formula_cold, size = 2, color = "black", aes(label = paste("Formula: ", poly_formula_cold))) +
geom_text(x = Inf, y = Inf, hjust = 1, vjust = 5, label = poly_formula_heat, size = 2, color = "black", aes(label = paste("Formula: ", poly_formula_heat))) +
geom_text(x = Inf, y = Inf, hjust = 1, vjust = 7, label = poly_formula_high_light_s, size = 2, color = "black", aes(label = paste("Formula: ", poly_formula_high_light_s))) +
labs(y = "FvFm", x = "Time") +
theme_bw() +
theme(axis.text.x = element_text(angle = 90), legend.position = "none")
plots[[i]] <- p
i <- i + 1
}
# Step 1: Call the pdf command to start the plot
pdf(file = "plots/FvFm_scatter_up_to_six_hours2.pdf",   # The directory you want to save the file in
width = 3.5, # The width of the plot in inches
height = 9) # The height of the plot in inches
# Step 2: Create the plot with R code
p <- plot_grid(plots[[1]], plots[[2]], plots[[3]], labels = "AUTO", ncol = 1)
print(p)
# Step 3: Run dev.off() to create the file!
dev.off()
plots <- list()
i <- 1
for (organism in species) {
all_data <- get(organism) %>%
filter(!(time > 6))
reg_standard_growth <- 1
reg_cold <- 4
reg_heat <- 7
reg_high_light <- 10
filtered_standard_growth <- all_data %>%
filter(treatment == "standard_growth" | (time == 0 )) %>%
filter(!(time > 6))
filtered_cold <- all_data %>%
filter(treatment == "cold" | (time == 0 )) %>%
filter(!(time > 6))
filtered_heat <- all_data %>%
filter(treatment == "heat" | (time == 0)) %>%
filter(!(time > 6))
filtered_high_light_s <- all_data %>%
filter(treatment == "high_light_s" | (time == 0 )) %>%
filter(!(time > 6))
filtered_standard_growth$condition <- factor(filtered_standard_growth$condition, levels = unique(filtered_standard_growth$condition))
filtered_standard_growth$treatment <- factor(rep("standard_growth", dim(filtered_standard_growth)[1]))
filtered_cold$condition <- factor(filtered_cold$condition, levels = unique(filtered_cold$condition))
filtered_cold$treatment <- factor(rep("cold", dim(filtered_cold)[1]))
filtered_heat$condition <- factor(filtered_heat$condition, levels = unique(filtered_heat$condition))
filtered_heat$treatment <- factor(rep("heat", dim(filtered_heat)[1]))
filtered_high_light_s$condition <- factor(filtered_high_light_s$condition, levels = unique(filtered_high_light_s$condition))
filtered_high_light_s$treatment <- factor(rep("high_light_s", dim(filtered_high_light_s)[1]))
# Extract polynomial regression coefficients
poly_coefs <- coef(lm((get("FvFm")) ~ poly(time, 2), data = filtered_standard_growth))
# Create a string representing the formula
poly_formula_standard_growth <- sprintf("Standard growth: y = %.2f + %.2fx + %.2fx^2", poly_coefs[1], poly_coefs[2], poly_coefs[3])
# Extract polynomial regression coefficients
poly_coefs <- coef(lm((get("FvFm")) ~ poly(time, 2), data = filtered_cold))
# Create a string representing the formula
poly_formula_cold <- sprintf("Cold: y = %.2f + %.2fx + %.2fx^2", poly_coefs[1], poly_coefs[2], poly_coefs[3])
# Extract polynomial regression coefficients
poly_coefs <- coef(lm((get("FvFm")) ~ poly(time, 2), data = filtered_heat))
# Create a string representing the formula
poly_formula_heat <- sprintf("Heat: y = %.2f + %.2fx + %.2fx^2", poly_coefs[1], poly_coefs[2], poly_coefs[3])
# Extract polynomial regression coefficients
poly_coefs <- coef(lm((get("FvFm")) ~ poly(time, 2), data = filtered_high_light_s))
# Create a string representing the formula
poly_formula_high_light_s <- sprintf("High light: y = %.2f + %.2fx + %.2fx^2", poly_coefs[1], poly_coefs[2], poly_coefs[3])
p <- ggplot(all_data, aes(time, get("FvFm"))) +
geom_point(aes(fill=treatment), shape=21, size=2, alpha = .5) +
stat_smooth(data = filtered_standard_growth, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = lm, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
stat_smooth(data = filtered_cold, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = lm, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
stat_smooth(data = filtered_heat, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = lm, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
stat_smooth(data = filtered_high_light_s, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = lm, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
ggtitle(paste0(organism, " - ", "FvFm values")) +
scale_fill_manual(values = my_colors_treatments) +
scale_color_manual(values = my_colors_treatments) +
scale_y_continuous(limits = c(0, 1), breaks = seq(0, 1, 0.2)) +
scale_x_continuous(breaks = unique(all_data[["time"]])) +
geom_text(x = Inf, y = Inf, hjust = 1, vjust = 1, label = poly_formula_standard_growth, size = 2, color = "black", aes(label = paste("Formula: ", poly_formula_standard_growth))) +
geom_text(x = Inf, y = Inf, hjust = 1, vjust = 3, label = poly_formula_cold, size = 2, color = "black", aes(label = paste("Formula: ", poly_formula_cold))) +
geom_text(x = Inf, y = Inf, hjust = 1, vjust = 5, label = poly_formula_heat, size = 2, color = "black", aes(label = paste("Formula: ", poly_formula_heat))) +
geom_text(x = Inf, y = Inf, hjust = 1, vjust = 7, label = poly_formula_high_light_s, size = 2, color = "black", aes(label = paste("Formula: ", poly_formula_high_light_s))) +
labs(y = "FvFm", x = "Time") +
theme_bw() +
theme(axis.text.x = element_blank(), legend.position = "none")
plots[[i]] <- p
i <- i + 1
}
# Step 1: Call the pdf command to start the plot
pdf(file = "plots/FvFm_scatter_up_to_six_hours2.pdf",   # The directory you want to save the file in
width = 3.5, # The width of the plot in inches
height = 9) # The height of the plot in inches
# Step 2: Create the plot with R code
p <- plot_grid(plots[[1]], plots[[2]], plots[[3]], labels = "AUTO", ncol = 1)
print(p)
# Step 3: Run dev.off() to create the file!
dev.off()
plots <- list()
i <- 1
for (organism in species) {
all_data <- get(organism) %>%
filter(!(time > 6))
reg_standard_growth <- 1
reg_cold <- 4
reg_heat <- 7
reg_high_light <- 10
filtered_standard_growth <- all_data %>%
filter(treatment == "standard_growth" | (time == 0 )) %>%
filter(!(time > 6))
filtered_cold <- all_data %>%
filter(treatment == "cold" | (time == 0 )) %>%
filter(!(time > 6))
filtered_heat <- all_data %>%
filter(treatment == "heat" | (time == 0)) %>%
filter(!(time > 6))
filtered_high_light_s <- all_data %>%
filter(treatment == "high_light_s" | (time == 0 )) %>%
filter(!(time > 6))
filtered_standard_growth$condition <- factor(filtered_standard_growth$condition, levels = unique(filtered_standard_growth$condition))
filtered_standard_growth$treatment <- factor(rep("standard_growth", dim(filtered_standard_growth)[1]))
filtered_cold$condition <- factor(filtered_cold$condition, levels = unique(filtered_cold$condition))
filtered_cold$treatment <- factor(rep("cold", dim(filtered_cold)[1]))
filtered_heat$condition <- factor(filtered_heat$condition, levels = unique(filtered_heat$condition))
filtered_heat$treatment <- factor(rep("heat", dim(filtered_heat)[1]))
filtered_high_light_s$condition <- factor(filtered_high_light_s$condition, levels = unique(filtered_high_light_s$condition))
filtered_high_light_s$treatment <- factor(rep("high_light_s", dim(filtered_high_light_s)[1]))
p <- ggplot(all_data, aes(time, get("FvFm"))) +
geom_point(aes(fill=treatment), shape=21, size=2, alpha = .5) +
stat_smooth(data = filtered_standard_growth, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = loess, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
stat_smooth(data = filtered_cold, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = loess, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
stat_smooth(data = filtered_heat, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = loess, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
stat_smooth(data = filtered_high_light_s, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = loess, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
ggtitle(paste0(organism, " - ", "FvFm values")) +
scale_fill_manual(values = my_colors_treatments) +
scale_color_manual(values = my_colors_treatments) +
scale_y_continuous(limits = c(0, 1), breaks = seq(0, 1, 0.2)) +
scale_x_continuous(breaks = unique(all_data[["time"]])) +
labs(y = "FvFm", x = "Time") +
theme_bw() +
theme(axis.text.x = element_text(angle = 90), legend.position = "none")
plots[[i]] <- p
i <- i + 1
}
# Step 1: Call the pdf command to start the plot
pdf(file = "plots/FvFm_scatter_up_to_six_hours_loess.pdf",   # The directory you want to save the file in
width = 3.5, # The width of the plot in inches
height = 9) # The height of the plot in inches
# Step 2: Create the plot with R code
p <- plot_grid(plots[[1]], plots[[2]], plots[[3]], labels = "AUTO", ncol = 1)
print(p)
# Step 3: Run dev.off() to create the file!
dev.off()
plots <- list()
i <- 1
for (organism in species) {
all_data <- get(organism) %>%
filter(!(time > 6))
reg_standard_growth <- 1
reg_cold <- 4
reg_heat <- 7
reg_high_light <- 10
filtered_standard_growth <- all_data %>%
filter(treatment == "standard_growth" | (time == 0 )) %>%
filter(!(time > 6))
filtered_cold <- all_data %>%
filter(treatment == "cold" | (time == 0 )) %>%
filter(!(time > 6))
filtered_heat <- all_data %>%
filter(treatment == "heat" | (time == 0)) %>%
filter(!(time > 6))
filtered_high_light_s <- all_data %>%
filter(treatment == "high_light_s" | (time == 0 )) %>%
filter(!(time > 6))
filtered_standard_growth$condition <- factor(filtered_standard_growth$condition, levels = unique(filtered_standard_growth$condition))
filtered_standard_growth$treatment <- factor(rep("standard_growth", dim(filtered_standard_growth)[1]))
filtered_cold$condition <- factor(filtered_cold$condition, levels = unique(filtered_cold$condition))
filtered_cold$treatment <- factor(rep("cold", dim(filtered_cold)[1]))
filtered_heat$condition <- factor(filtered_heat$condition, levels = unique(filtered_heat$condition))
filtered_heat$treatment <- factor(rep("heat", dim(filtered_heat)[1]))
filtered_high_light_s$condition <- factor(filtered_high_light_s$condition, levels = unique(filtered_high_light_s$condition))
filtered_high_light_s$treatment <- factor(rep("high_light_s", dim(filtered_high_light_s)[1]))
p <- ggplot(all_data, aes(time, get("FvFm"))) +
geom_point(aes(fill=treatment), shape=21, size=2, alpha = .5) +
stat_smooth(data = filtered_standard_growth, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = loess, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
stat_smooth(data = filtered_cold, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = loess, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
stat_smooth(data = filtered_heat, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = loess, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
stat_smooth(data = filtered_high_light_s, aes(x = time, y = get("FvFm"), color = treatment, group = treatment), method = loess, formula = y ~ poly(x, 2),  linewidth = 0.5, alpha = .5, se = FALSE, show.legend = FALSE) +
ggtitle(paste0(organism, " - ", "FvFm values")) +
scale_fill_manual(values = my_colors_treatments) +
scale_color_manual(values = my_colors_treatments) +
scale_y_continuous(limits = c(0, 1), breaks = seq(0, 1, 0.2)) +
scale_x_continuous(breaks = unique(all_data[["time"]])) +
labs(y = "FvFm", x = "Time") +
theme_bw() +
theme(axis.text.x = element_blank(), legend.position = "none")
plots[[i]] <- p
i <- i + 1
}
# Step 1: Call the pdf command to start the plot
pdf(file = "plots/FvFm_scatter_up_to_six_hours_loess2.pdf",   # The directory you want to save the file in
width = 3.5, # The width of the plot in inches
height = 9) # The height of the plot in inches
# Step 2: Create the plot with R code
p <- plot_grid(plots[[1]], plots[[2]], plots[[3]], labels = "AUTO", ncol = 1)
print(p)
# Step 3: Run dev.off() to create the file!
dev.off()
??ggdist::stat_halfeye
