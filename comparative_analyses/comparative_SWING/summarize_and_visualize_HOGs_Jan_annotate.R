#-------------------------------------------------------------------------------
# Setup
#-------------------------------------------------------------------------------
library(tidyverse)
library(igraph)
#-------------------------------------------------------------------------------
# load the data
#-------------------------------------------------------------------------------
meso_heat <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/tables/Me_heat.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
meso_cold <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/tables/Me_cold.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
meso_highlight <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/tables/Me_highlight.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
zygnema_cold <- read_tsv("../../zygnema_circumcarinatum_1b/Analyses/SWING/tables/Zc_cold.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
zygnema_heat <- read_tsv("../../zygnema_circumcarinatum_1b/Analyses/SWING/tables/Zc_heat.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
zygnema_highlight <- read_tsv("../../zygnema_circumcarinatum_1b/Analyses/SWING/tables/Zc_highlight.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
physco_cold <- read_tsv("../../physcomitrium_patens/Analyses/SWING/tables/Pp_cold.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
physco_heat <- read_tsv("../../physcomitrium_patens/Analyses/SWING/tables/Pp_heat.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
physco_highlight <- read_tsv("../../physcomitrium_patens/Analyses/SWING/tables/Pp_highlight.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
# Functional annotation
hog_meso <- read_tsv("../../orthofinder/Phylogenetic_Hierarchical_Orthogroups/N0.tsv") %>%
    select(c(HOG, M_endlicherianum)) %>%
    drop_na(M_endlicherianum) %>%
    separate_rows(M_endlicherianum, sep = ", ") %>%
    distinct()
hog_physco <- read_tsv("../../orthofinder/Phylogenetic_Hierarchical_Orthogroups/N0.tsv") %>%
    select(c(HOG, P_patens)) %>%
    drop_na(P_patens) %>%
    separate_rows(P_patens, sep = ", ") %>%
    distinct() %>%
    mutate(P_patens = paste0(P_patens, "V3"))
hog_zygnema <- read_tsv("../../orthofinder/Phylogenetic_Hierarchical_Orthogroups/N0.tsv") %>%
    select(c(HOG, Z_circumcarinatum_SAG698_1b)) %>%
    drop_na(Z_circumcarinatum_SAG698_1b) %>%
    separate_rows(Z_circumcarinatum_SAG698_1b, sep = ", ") %>%
    distinct()

metabolites <- c("viol",	"9_cis_neo",	"anthera",	"Chlb",	"Lut",	"Zeax",	"Chla",	"alpha_car",	"11_cis",	"beta_car",	"9_cis",	"6MHO",	"beta_CC",	"beta_Io",	"DHA",	"DHA_per_beta_Io",	"Chla_per_b",	"beta_car_per_9_cis_neox",	"V_A_Z_per_Chla_b",	"A_Z_per_V_A_Z",	"beta_Car_per_beta_CC",	"beta_Car_per_beta_Io",	"beta_Car_per_DHA",	"beta_Car_per_beta_CC_beta_Io_DHA")
metabolite_tibble_meso <- tibble(
    HOG = metabolites,
    M_endlicherianum = metabolites
)
metabolite_tibble_physco <- tibble(
    HOG = metabolites,
    P_patens = metabolites
)
metabolite_tibble_zygnema <- tibble(
    HOG = metabolites,
    Z_circumcarinatum_SAG698_1b = metabolites
)
hog_meso <- bind_rows(hog_meso, metabolite_tibble_meso)
hog_physco <- bind_rows(hog_physco, metabolite_tibble_physco)
hog_zygnema <- bind_rows(hog_zygnema, metabolite_tibble_zygnema)
#-------------------------------------------------------------------------------
# Merging data using inner join
#-------------------------------------------------------------------------------
# mesotaenium
meso <- meso_heat %>%
    full_join(meso_cold, by = "pair") %>%
    full_join(meso_highlight, by = "pair") %>%
    mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
    mutate(regulator.x = ifelse(is.na(regulator.x), ifelse(is.na(regulator.y), regulator, regulator.y), regulator.x)) %>%
    mutate(target.x = ifelse(is.na(target.x), ifelse(is.na(target.y), target, target.y), target.x)) %>%
    select(c(regulator.x, target.x, mean_weight)) %>%
    distinct() %>%
    arrange(desc(mean_weight))
colnames(meso) <- c("regulator", "target", "weight")
meso <- meso %>%
    left_join(hog_meso, by = join_by("regulator" == "M_endlicherianum")) %>%
    select(c(HOG, target, weight)) %>%
    drop_na(HOG)
colnames(meso) <- c("regulator", "target", "weight")
meso <- meso %>%
    left_join(hog_meso, by = join_by("target" == "M_endlicherianum")) %>%
    select(c(regulator, HOG, weight))
colnames(meso) <- c("regulator", "target", "weight")
meso <- meso %>%
    mutate(pair = paste0(regulator, "-",target)) %>%
    drop_na() %>%
    filter(regulator != target) %>%
    distinct(pair, .keep_all = T)
# zygnema
zygnema <- zygnema_cold %>%
    full_join(zygnema_heat, by = "pair") %>%
    full_join(zygnema_highlight, by = "pair") %>%
    mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
    mutate(regulator.x = ifelse(is.na(regulator.x), ifelse(is.na(regulator.y), regulator, regulator.y), regulator.x)) %>%
    mutate(target.x = ifelse(is.na(target.x), ifelse(is.na(target.y), target, target.y), target.x)) %>%
    select(c(regulator.x, target.x, mean_weight)) %>%
    distinct() %>%
    arrange(desc(mean_weight))
colnames(zygnema) <- c("regulator", "target", "weight")
zygnema <- zygnema %>%
    left_join(hog_zygnema, by = join_by("regulator" == "Z_circumcarinatum_SAG698_1b")) %>%
    select(c(HOG, target, weight)) %>%
    drop_na(HOG)
colnames(zygnema) <- c("regulator", "target", "weight")
zygnema <- zygnema %>%
    left_join(hog_zygnema, by = join_by("target" == "Z_circumcarinatum_SAG698_1b")) %>%
    select(c(regulator, HOG, weight))
colnames(zygnema) <- c("regulator", "target", "weight")
zygnema <- zygnema %>%
    mutate(pair = paste0(regulator, "-",target)) %>%
    drop_na() %>%
    filter(regulator != target) %>%
    distinct(pair, .keep_all = T)
# physco
physco <- physco_cold %>%
    full_join(physco_heat, by = "pair") %>%
    full_join(physco_highlight, by = "pair") %>%
    mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
    mutate(regulator.x = ifelse(is.na(regulator.x), ifelse(is.na(regulator.y), regulator, regulator.y), regulator.x)) %>%
    mutate(target.x = ifelse(is.na(target.x), ifelse(is.na(target.y), target, target.y), target.x)) %>%
    select(c(regulator.x, target.x, mean_weight)) %>%
    distinct() %>%
    arrange(desc(mean_weight))
colnames(physco) <- c("regulator", "target", "weight")
physco <- physco %>%
    left_join(hog_physco, by = join_by("regulator" == "P_patens")) %>%
    select(c(HOG, target, weight)) %>%
    drop_na(HOG)
colnames(physco) <- c("regulator", "target", "weight")
physco <- physco %>%
    left_join(hog_physco, by = join_by("target" == "P_patens")) %>%
    select(c(regulator, HOG, weight))
colnames(physco) <- c("regulator", "target", "weight")
physco <- physco %>%
    mutate(pair = paste0(regulator, "-",target)) %>%
    drop_na() %>%
    filter(regulator != target) %>%
    distinct(pair, .keep_all = T)
# Due to the out of memory error
rm(hog_meso, hog_physco, hog_zygnema, meso_cold, meso_heat, meso_highlight,
   zygnema_cold, zygnema_heat, zygnema_highlight, physco_cold, physco_heat, physco_highlight, swing_all)
# merge all
swing_all <- meso %>%
    inner_join(zygnema, by = "pair") %>%
    inner_join(physco, by = "pair")  %>%
    mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
    mutate(pair = paste0(regulator, "-", "target")) %>%
    distinct(pair, .keep_all = TRUE) %>%
    select(c(regulator, target, mean_weight))
colnames(swing_all) <- c("regulator", "target", "weight")
swing_all <- swing_all %>%
    filter(regulator != target) %>%
    arrange(desc(weight))
#write_tsv(x = swing_all, file = "tables/swing_all_merged_via_HOGs.tsv")
#-------------------------------------------------------------------------------
# make the graph
#-------------------------------------------------------------------------------
hog_ath <- read_tsv("../../orthofinder/Phylogenetic_Hierarchical_Orthogroups/N0.tsv") %>%
    select(c(HOG, A_thaliana)) %>%
    drop_na(A_thaliana) %>%
    separate_rows(A_thaliana, sep = ", ") %>%
    distinct()
ath_symbol <- read_tsv("../../best_blast_hit/Araport11_pep_representative.fa", col_names = F)
colnames(ath_symbol) <- c("A_thaliana", "symbols")
hog_ath_annotation <- hog_ath %>%
    left_join(ath_symbol, by = "A_thaliana") %>%
    select(c(HOG, symbols)) %>%
    filter(symbols != "-") %>%
    distinct() %>%
    group_by(HOG) %>%
    summarise(symbols = paste(symbols, collapse = ", ")) %>%
    distinct()
jan_annotation <- read_tsv("SuperJannotate.txt")
top_predictions <- swing_all %>%
    slice_head(n=100) %>%
    left_join(jan_annotation, by = join_by("regulator" == "HOG")) %>%
    mutate(regulator = if_else(is.na(Symbol), regulator, Symbol)) %>%
    select(-Symbol) %>%
    left_join(jan_annotation, by = join_by("target" == "HOG")) %>%
    mutate(target = if_else(is.na(Symbol), target, Symbol)) %>%
    select(-Symbol)
#top_predictions <- swing_all %>%
#    slice_head(n=100) %>%
#    gather(key, value, regulator, target) %>%
#    select(value) %>%
#    left_join(jan_annotation, by = join_by("value" == "HOG")) %>%
#    left_join(hog_ath_annotation, by = join_by("value" == "HOG")) %>%
#    distinct(value, .keep_all = TRUE)
#colnames(top_predictions) <- c("HOG", "Jannotate", "ath_blast_symbol")
#write_tsv(x = top_predictions, file = "tables/top_100_pair_predictors_HOGs_annotation.tsv")

#top_predictions <- swing_all %>%
#    slice_head(n=100) %>%
#    gather(key, value, regulator, target) %>%
#    select(value)
node_names <- swing_all %>%
    gather(key, gene, regulator, target) %>%
    select(gene) %>%
    left_join(jan_annotation, by = join_by("gene" == "HOG")) %>%
    #select(Symbol) %>%
    #left_join(top_predictions, by = join_by("Symbol" == "value")) %>%
    mutate(vertex_size = if_else((Symbol %in% top_predictions$regulator) | (Symbol %in% top_predictions$target), 3, 1)) %>%
    distinct(.keep_all = T)

# make the network
network <- graph_from_data_frame(d=swing_all, directed=T) 
network_layout <- layout_with_kk(network)

# Find connected components
components <- clusters(network)
top_components <- components$csize[order(components$csize, decreasing = TRUE)][1:5]
top_indices <- which(components$csize %in% top_components)
V(network)$color <- node_names %>%
    filter(gene %in% V(network)$name) %>%
    mutate(color = if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][1]]), "#FFBE0B",
                           if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][2]]), "#FB5607",
                                   if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][3]]), "#FF006E",
                                           if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][4]]), "#8338EC",
                                                   if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][5]]), "#3A86FF",
                                                           "#ece4db"
                                                   )))))) %>%
    select(color) %>%
    unlist() %>%
    as.vector()
V(network)$labelColor <- node_names %>%
    filter(gene %in% V(network)$name) %>%
    mutate(color = if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][1]]), "#8f6900",
                           if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][2]]), "#a13502",
                                   if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][3]]), "#66002c",
                                           if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][4]]), "#460e95",
                                                   if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][5]]), "#003fa3",
                                                           "#000000"
                                                   )))))) %>%
    select(color) %>%
    unlist() %>%
    as.vector()
V(network)$size <- node_names %>%
    filter(gene %in% V(network)$name) %>%
    select(vertex_size) %>%
    unlist() %>%
    as.vector()
V(network)$name <- node_names %>%
    filter(gene %in% V(network)$name) %>%
    mutate(label = ifelse(vertex_size == 3, Symbol, NA)) %>%
    select(label) %>%
    unlist() %>%
    as.vector()

edge_colors <- colorRampPalette(c("#e5e5e5", "#000000"))(100)[as.numeric(cut(swing_all$weight*1e10, breaks = 100))]

#Applying the color scale to edge weights.
#rgb method is to convert colors to a character vector.

#-------------------------------------------------------------------------------
# plot the network
#-------------------------------------------------------------------------------
pdf(file = paste0("plots/swing_top_100_annotated_HOGs_Jannotate.pdf"),   # The directory you want to save the file in
    width = 15, # The width of the plot in inches
    height = 15) # The height of the plot in inches
plot(network,
     # vertex
     vertex.shape = "circle",
     vertex.size = V(network)$size,
     vertex.size = 1,
     vertex.color = V(network)$color,
     vertex.label = V(network)$name,
     vertex.label.cex = 0.4,
     vertex.frame.color = NA,
     vertex.frame.width = 1,
     vertex.label.color=V(network)$labelColor,
     #edge
     #edge.color = "#e5e5e5",
     edge.color = edge_colors,
     edge.width = 0.5,
     dge.arrow.width = 0.1,
     edge.arrow.size= 0.1,
     edge.lty = "solid",
     edge.label = NA,
     # other
     main = paste0("SWING all shared connections - top 100 annotated"),
     layout=network_layout*0.2,
)
dev.off()    
## anotate all
top_predictions <- swing_all %>%
    gather(key, value, regulator, target) %>%
    select(value)
node_names <- swing_all %>%
    gather(key, gene, regulator, target) %>%
    select(gene) %>%
    left_join(top_predictions, by = join_by("gene" == "value")) %>%
    mutate(vertex_size = if_else(gene %in% top_predictions$value, 1, 1)) %>%
    distinct()

# make the network
network <- graph_from_data_frame(d=swing_all, directed=T) 
network_layout <- layout_with_kk(network)

# Find connected components
components <- clusters(network)
top_components <- components$csize[order(components$csize, decreasing = TRUE)][1:5]
top_indices <- which(components$csize %in% top_components)
V(network)$color <- node_names %>%
    filter(gene %in% V(network)$name) %>%
    mutate(color = if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][1]]), "#FFBE0B",
                           if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][2]]), "#FB5607",
                                   if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][3]]), "#FF006E",
                                           if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][4]]), "#8338EC",
                                                   if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][5]]), "#3A86FF",
                                                           "#ece4db"
                                                   )))))) %>%
    select(color) %>%
    unlist() %>%
    as.vector()
V(network)$labelColor <- node_names %>%
    filter(gene %in% V(network)$name) %>%
    mutate(color = if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][1]]), "#8f6900",
                           if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][2]]), "#a13502",
                                   if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][3]]), "#66002c",
                                           if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][4]]), "#460e95",
                                                   if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][5]]), "#003fa3",
                                                           "#000000"
                                                   )))))) %>%
    select(color) %>%
    unlist() %>%
    as.vector()
V(network)$size <- node_names %>%
    filter(gene %in% V(network)$name) %>%
    select(vertex_size) %>%
    unlist() %>%
    as.vector()
V(network)$name <- node_names %>%
    filter(gene %in% V(network)$name) %>%
    mutate(label = ifelse(vertex_size == 1, gene, NA)) %>%
    select(label) %>%
    unlist() %>%
    as.vector()

edge_colors <- colorRampPalette(c("#e5e5e5", "#000000"))(100)[as.numeric(cut(swing_all$weight*1e10, breaks = 100))]

#Applying the color scale to edge weights.
#rgb method is to convert colors to a character vector.

#-------------------------------------------------------------------------------
# plot the network
#-------------------------------------------------------------------------------
pdf(file = paste0("plots/swing_top_all_annotated_HOGs.pdf"),   # The directory you want to save the file in
    width = 15, # The width of the plot in inches
    height = 15) # The height of the plot in inches
plot(network,
     # vertex
     vertex.shape = "circle",
     vertex.size = V(network)$size,
     vertex.size = 1,
     vertex.color = V(network)$color,
     vertex.label = V(network)$name,
     vertex.label.cex = 0.4,
     vertex.frame.color = NA,
     vertex.frame.width = 1,
     vertex.label.color=V(network)$labelColor,
     #edge
     #edge.color = "#e5e5e5",
     edge.color = edge_colors,
     edge.width = 0.5,
     dge.arrow.width = 0.1,
     edge.arrow.size= 0.1,
     edge.lty = "solid",
     edge.label = NA,
     # other
     main = paste0("SWING all shared connections"),
     layout=network_layout*0.7,
)
dev.off()    

