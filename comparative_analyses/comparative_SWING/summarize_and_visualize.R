#-------------------------------------------------------------------------------
# Setup
#-------------------------------------------------------------------------------
library(tidyverse)
library(igraph)
#-------------------------------------------------------------------------------
# load the data
#-------------------------------------------------------------------------------
meso_heat <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/tables/Me_heat.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
meso_cold <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/tables/Me_cold.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
meso_highlight <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/tables/Me_highlight.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
zygnema_cold <- read_tsv("../../zygnema_circumcarinatum_1b/Analyses/SWING/tables/Zc_cold.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
zygnema_heat <- read_tsv("../../zygnema_circumcarinatum_1b/Analyses/SWING/tables/Zc_heat.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
zygnema_highlight <- read_tsv("../../zygnema_circumcarinatum_1b/Analyses/SWING/tables/Zc_highlight.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
physco_cold <- read_tsv("../../physcomitrium_patens/Analyses/SWING/tables/Pp_cold.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
physco_heat <- read_tsv("../../physcomitrium_patens/Analyses/SWING/tables/Pp_heat.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
physco_highlight <- read_tsv("../../physcomitrium_patens/Analyses/SWING/tables/Pp_highlight.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
# Functional annotation
blast_meso <- read_tsv("../../best_blast_hit/meso_swing_labels.tsv", col_names = T) %>%
    distinct(meso, .keep_all = TRUE)
blast_physco <- read_tsv("../../best_blast_hit/physco_swing_labels.tsv", col_names = T) %>%
    distinct(physco, .keep_all = TRUE)
blast_zygnema <- read_tsv("../../best_blast_hit/zygnema_swing_labels.tsv", col_names = T) %>%
    distinct(zygnema, .keep_all = TRUE)
metabolites <- c("viol",	"9_cis_neo",	"anthera",	"Chlb",	"Lut",	"Zeax",	"Chla",	"alpha_car",	"11_cis",	"beta_car",	"9_cis",	"6MHO",	"beta_CC",	"beta_Io",	"DHA",	"DHA_per_beta_Io",	"Chla_per_b",	"beta_car_per_9_cis_neox",	"V_A_Z_per_Chla_b",	"A_Z_per_V_A_Z",	"beta_Car_per_beta_CC",	"beta_Car_per_beta_Io",	"beta_Car_per_DHA",	"beta_Car_per_beta_CC_beta_Io_DHA")
metabolite_tibble_meso <- tibble(
    meso = metabolites,
    ath = metabolites,
    symbol = metabolites,
    use_me = metabolites
)
metabolite_tibble_physco <- tibble(
    physco = metabolites,
    ath = metabolites,
    symbol = metabolites,
    use_me = metabolites
)
metabolite_tibble_zygnema <- tibble(
    zygnema = metabolites,
    ath = metabolites,
    symbol = metabolites,
    use_me = metabolites
)
blast_meso <- bind_rows(blast_meso, metabolite_tibble_meso)
blast_physco <- bind_rows(blast_physco, metabolite_tibble_physco)
blast_zygnema <- bind_rows(blast_zygnema, metabolite_tibble_zygnema)
ath_carotenoid <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/inputs/unique_ATH/carotenoid.txt", col_names = F)
ath_apocarotenoid <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/inputs/unique_ATH/apocarotenoid.txt", col_names = F)
ath_cold <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/inputs/unique_ATH/cold.txt", col_names = F)
ath_heat <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/inputs/unique_ATH/heat.txt", col_names = F)
ath_highlight <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/inputs/unique_ATH/high_light.txt", col_names = F)
ath_oxidative <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/inputs/unique_ATH/oxidative.txt", col_names = F)
#-------------------------------------------------------------------------------
# Merging data using inner join
#-------------------------------------------------------------------------------
# mesotaenium
meso <- meso_heat %>%
    full_join(meso_cold, by = "pair") %>%
    full_join(meso_highlight, by = "pair") %>%
    mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
    mutate(regulator.x = ifelse(is.na(regulator.x), ifelse(is.na(regulator.y), regulator, regulator.y), regulator.x)) %>%
    mutate(target.x = ifelse(is.na(target.x), ifelse(is.na(target.y), target, target.y), target.x)) %>%
    select(c(regulator.x, target.x, mean_weight)) %>%
    distinct() %>%
    arrange(desc(mean_weight))
colnames(meso) <- c("regulator", "target", "weight")
meso <- meso %>%
    left_join(blast_meso, by = join_by("regulator" == "meso")) %>%
    select(c(use_me, target, weight)) %>%
    drop_na(use_me)
colnames(meso) <- c("regulator", "target", "weight")
meso <- meso %>%
    left_join(blast_meso, by = join_by("target" == "meso")) %>%
    select(c(regulator, use_me, weight))
colnames(meso) <- c("regulator", "target", "weight")
meso <- meso %>%
    mutate(pair = paste0(regulator, "-",target)) %>%
    drop_na()
# zygnema
zygnema <- zygnema_cold %>%
    full_join(zygnema_heat, by = "pair") %>%
    full_join(zygnema_highlight, by = "pair") %>%
    mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
    mutate(regulator.x = ifelse(is.na(regulator.x), ifelse(is.na(regulator.y), regulator, regulator.y), regulator.x)) %>%
    mutate(target.x = ifelse(is.na(target.x), ifelse(is.na(target.y), target, target.y), target.x)) %>%
    select(c(regulator.x, target.x, mean_weight)) %>%
    distinct() %>%
    arrange(desc(mean_weight))
colnames(zygnema) <- c("regulator", "target", "weight")
zygnema <- zygnema %>%
    left_join(blast_zygnema, by = join_by("regulator" == "zygnema")) %>%
    select(c(use_me, target, weight)) %>%
    drop_na(use_me)
colnames(zygnema) <- c("regulator", "target", "weight")
zygnema <- zygnema %>%
    left_join(blast_zygnema, by = join_by("target" == "zygnema")) %>%
    select(c(regulator, use_me, weight))
colnames(zygnema) <- c("regulator", "target", "weight")
zygnema <- zygnema %>%
    mutate(pair = paste0(regulator, "-",target)) %>%
    drop_na()
# physco
physco <- physco_cold %>%
    full_join(physco_heat, by = "pair") %>%
    full_join(physco_highlight, by = "pair") %>%
    mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
    mutate(regulator.x = ifelse(is.na(regulator.x), ifelse(is.na(regulator.y), regulator, regulator.y), regulator.x)) %>%
    mutate(target.x = ifelse(is.na(target.x), ifelse(is.na(target.y), target, target.y), target.x)) %>%
    select(c(regulator.x, target.x, mean_weight)) %>%
    distinct() %>%
    arrange(desc(mean_weight))
colnames(physco) <- c("regulator", "target", "weight")
physco <- physco %>%
    left_join(blast_physco, by = join_by("regulator" == "physco")) %>%
    select(c(use_me, target, weight)) %>%
    drop_na(use_me)
colnames(physco) <- c("regulator", "target", "weight")
physco <- physco %>%
    left_join(blast_physco, by = join_by("target" == "physco")) %>%
    select(c(regulator, use_me, weight))
colnames(physco) <- c("regulator", "target", "weight")
physco <- physco %>%
    mutate(pair = paste0(regulator, "-",target)) %>%
    drop_na()
# merge all
swing_all <- meso %>%
    inner_join(zygnema, by = "pair") %>%
    inner_join(physco, by = "pair")  %>%
    mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
    mutate(pair = paste0(regulator, "-", "target")) %>%
    distinct(pair, .keep_all = TRUE) %>%
    select(c(regulator, target, mean_weight))
colnames(swing_all) <- c("regulator", "target", "weight")
swing_all <- swing_all %>%
    filter(regulator != target) %>%
    arrange(desc(weight))
write_tsv(x = swing_all, file = "tables/swing_all_merged_via_ath_blastp.tsv")
#-------------------------------------------------------------------------------
# make the graph
#-------------------------------------------------------------------------------
top_predictions <- swing_all %>%
    slice_head(n=100) %>%
    gather(key, value, regulator, target) %>%
    select(value)
node_names <- swing_all %>%
    gather(key, gene, regulator, target) %>%
    select(gene) %>%
    left_join(top_predictions, by = join_by("gene" == "value")) %>%
    mutate(vertex_size = if_else(gene %in% top_predictions$value, 3, 1)) %>%
    distinct()

# make the network
network <- graph_from_data_frame(d=swing_all, directed=T) 
network_layout <- layout_with_kk(network)

# Find connected components
components <- clusters(network)
top_components <- components$csize[order(components$csize, decreasing = TRUE)][1:5]
top_indices <- which(components$csize %in% top_components)
V(network)$color <- node_names %>%
    filter(gene %in% V(network)$name) %>%
    mutate(color = if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][1]]), "#FFBE0B",
                           if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][2]]), "#FB5607",
                                   if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][3]]), "#FF006E",
                                           if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][4]]), "#8338EC",
                                                   if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][5]]), "#3A86FF",
                                                           "#ece4db"
                                                   )))))) %>%
    select(color) %>%
    unlist() %>%
    as.vector()
V(network)$labelColor <- node_names %>%
    filter(gene %in% V(network)$name) %>%
    mutate(color = if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][1]]), "#8f6900",
                           if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][2]]), "#a13502",
                                   if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][3]]), "#66002c",
                                           if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][4]]), "#460e95",
                                                   if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][5]]), "#003fa3",
                                                           "#000000"
                                                   )))))) %>%
    select(color) %>%
    unlist() %>%
    as.vector()
V(network)$size <- node_names %>%
    filter(gene %in% V(network)$name) %>%
    select(vertex_size) %>%
    unlist() %>%
    as.vector()
V(network)$name <- node_names %>%
    filter(gene %in% V(network)$name) %>%
    mutate(label = ifelse(vertex_size == 3, gene, NA)) %>%
    select(label) %>%
    unlist() %>%
    as.vector()

edge_colors <- colorRampPalette(c("#e5e5e5", "#000000"))(100)[as.numeric(cut(swing_all$weight*1e10, breaks = 100))]

#Applying the color scale to edge weights.
#rgb method is to convert colors to a character vector.

#-------------------------------------------------------------------------------
# plot the network
#-------------------------------------------------------------------------------
pdf(file = paste0("plots/swing_top_100_annotated.pdf"),   # The directory you want to save the file in
    width = 15, # The width of the plot in inches
    height = 15) # The height of the plot in inches
plot(network,
     # vertex
     vertex.shape = "circle",
     vertex.size = V(network)$size,
     vertex.size = 1,
     vertex.color = V(network)$color,
     vertex.label = V(network)$name,
     vertex.label.cex = 0.4,
     vertex.frame.color = NA,
     vertex.frame.width = 1,
     vertex.label.color=V(network)$labelColor,
     #edge
     #edge.color = "#e5e5e5",
     edge.color = edge_colors,
     edge.width = 0.5,
     dge.arrow.width = 0.1,
     edge.arrow.size= 0.1,
     edge.lty = "solid",
     edge.label = NA,
     # other
     main = paste0("SWING all shared connections - top 100 annotated"),
     layout=network_layout*0.2,
)
dev.off()    
## anotate all
top_predictions <- swing_all %>%
    gather(key, value, regulator, target) %>%
    select(value)
node_names <- swing_all %>%
    gather(key, gene, regulator, target) %>%
    select(gene) %>%
    left_join(top_predictions, by = join_by("gene" == "value")) %>%
    mutate(vertex_size = if_else(gene %in% top_predictions$value, 1, 1)) %>%
    distinct()

# make the network
network <- graph_from_data_frame(d=swing_all, directed=T) 
network_layout <- layout_with_kk(network)

# Find connected components
components <- clusters(network)
top_components <- components$csize[order(components$csize, decreasing = TRUE)][1:5]
top_indices <- which(components$csize %in% top_components)
V(network)$color <- node_names %>%
    filter(gene %in% V(network)$name) %>%
    mutate(color = if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][1]]), "#FFBE0B",
                           if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][2]]), "#FB5607",
                                   if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][3]]), "#FF006E",
                                           if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][4]]), "#8338EC",
                                                   if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][5]]), "#3A86FF",
                                                           "#ece4db"
                                                   )))))) %>%
    select(color) %>%
    unlist() %>%
    as.vector()
V(network)$labelColor <- node_names %>%
    filter(gene %in% V(network)$name) %>%
    mutate(color = if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][1]]), "#8f6900",
                           if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][2]]), "#a13502",
                                   if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][3]]), "#66002c",
                                           if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][4]]), "#460e95",
                                                   if_else(gene %in% names(components$membership[components$membership == order(components$csize, decreasing = TRUE)[1:5][5]]), "#003fa3",
                                                           "#000000"
                                                   )))))) %>%
    select(color) %>%
    unlist() %>%
    as.vector()
V(network)$size <- node_names %>%
    filter(gene %in% V(network)$name) %>%
    select(vertex_size) %>%
    unlist() %>%
    as.vector()
V(network)$name <- node_names %>%
    filter(gene %in% V(network)$name) %>%
    mutate(label = ifelse(vertex_size == 1, gene, NA)) %>%
    select(label) %>%
    unlist() %>%
    as.vector()

edge_colors <- colorRampPalette(c("#e5e5e5", "#000000"))(100)[as.numeric(cut(swing_all$weight*1e10, breaks = 100))]

#Applying the color scale to edge weights.
#rgb method is to convert colors to a character vector.

#-------------------------------------------------------------------------------
# plot the network
#-------------------------------------------------------------------------------
pdf(file = paste0("plots/swing_top_all_annotated.pdf"),   # The directory you want to save the file in
    width = 15, # The width of the plot in inches
    height = 15) # The height of the plot in inches
plot(network,
     # vertex
     vertex.shape = "circle",
     vertex.size = V(network)$size,
     vertex.size = 1,
     vertex.color = V(network)$color,
     vertex.label = V(network)$name,
     vertex.label.cex = 0.4,
     vertex.frame.color = NA,
     vertex.frame.width = 1,
     vertex.label.color=V(network)$labelColor,
     #edge
     #edge.color = "#e5e5e5",
     edge.color = edge_colors,
     edge.width = 0.5,
     dge.arrow.width = 0.1,
     edge.arrow.size= 0.1,
     edge.lty = "solid",
     edge.label = NA,
     # other
     main = paste0("SWING all shared connections"),
     layout=network_layout*0.7,
)
dev.off()    

