####
# Setup
####
library(tidyverse)
library(igraph)
library(pheatmap)
library(RColorBrewer)
library(cowplot)
library(scales)

#-------------------------------------------------------------------------------
# load the data
#-------------------------------------------------------------------------------
meso_heat <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/tables/Me_heat.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
meso_cold <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/tables/Me_cold.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
meso_highlight <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/tables/Me_highlight.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
zygnema_cold <- read_tsv("../../zygnema_circumcarinatum_1b/Analyses/SWING/tables/Zc_cold.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
zygnema_heat <- read_tsv("../../zygnema_circumcarinatum_1b/Analyses/SWING/tables/Zc_heat.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
zygnema_highlight <- read_tsv("../../zygnema_circumcarinatum_1b/Analyses/SWING/tables/Zc_highlight.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
physco_cold <- read_tsv("../../physcomitrium_patens/Analyses/SWING/tables/Pp_cold.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
physco_heat <- read_tsv("../../physcomitrium_patens/Analyses/SWING/tables/Pp_heat.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
physco_highlight <- read_tsv("../../physcomitrium_patens/Analyses/SWING/tables/Pp_highlight.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
# Functional annotation
# Functional annotation
hog_meso <- read_tsv("../../orthofinder/Phylogenetic_Hierarchical_Orthogroups/N0.tsv") %>%
    select(c(HOG, M_endlicherianum)) %>%
    drop_na(M_endlicherianum) %>%
    separate_rows(M_endlicherianum, sep = ", ") %>%
    distinct()
hog_physco <- read_tsv("../../orthofinder/Phylogenetic_Hierarchical_Orthogroups/N0.tsv") %>%
    select(c(HOG, P_patens)) %>%
    drop_na(P_patens) %>%
    separate_rows(P_patens, sep = ", ") %>%
    distinct() %>%
    mutate(P_patens = paste0(P_patens, "V3"))
hog_zygnema <- read_tsv("../../orthofinder/Phylogenetic_Hierarchical_Orthogroups/N0.tsv") %>%
    select(c(HOG, Z_circumcarinatum_SAG698_1b)) %>%
    drop_na(Z_circumcarinatum_SAG698_1b) %>%
    separate_rows(Z_circumcarinatum_SAG698_1b, sep = ", ") %>%
    distinct()
hog_ath <- read_tsv("../../orthofinder/Phylogenetic_Hierarchical_Orthogroups/N0.tsv") %>%
    select(c(HOG, A_thaliana)) %>%
    drop_na(A_thaliana) %>%
    separate_rows(A_thaliana, sep = ", ") %>%
    distinct()
ath_symbol <- read_tsv("../../best_blast_hit/Araport11_pep_representative.fa", col_names = F)
colnames(ath_symbol) <- c("A_thaliana", "symbols")
hog_ath_annotation <- hog_ath %>%
    left_join(ath_symbol, by = "A_thaliana") %>%
    select(c(HOG, symbols)) %>%
    filter(symbols != "-") %>%
    distinct() %>%
    group_by(HOG) %>%
    summarise(symbols = paste(symbols, collapse = ", ")) %>%
    distinct()

metabolites <- c("viol",	"9_cis_neo",	"anthera",	"Chlb",	"Lut",	"Zeax",	"Chla",	"alpha_car",	"11_cis",	"beta_car",	"9_cis",	"6MHO",	"beta_CC",	"beta_Io",	"DHA",	"DHA_per_beta_Io",	"Chla_per_b",	"beta_car_per_9_cis_neox",	"V_A_Z_per_Chla_b",	"A_Z_per_V_A_Z",	"beta_Car_per_beta_CC",	"beta_Car_per_beta_Io",	"beta_Car_per_DHA",	"beta_Car_per_beta_CC_beta_Io_DHA")
metabolite_tibble_meso <- tibble(
    HOG = metabolites,
    M_endlicherianum = metabolites
)
metabolite_tibble_physco <- tibble(
    HOG = metabolites,
    P_patens = metabolites
)
metabolite_tibble_zygnema <- tibble(
    HOG = metabolites,
    Z_circumcarinatum_SAG698_1b = metabolites
)
hog_meso <- bind_rows(hog_meso, metabolite_tibble_meso)
hog_physco <- bind_rows(hog_physco, metabolite_tibble_physco)
hog_zygnema <- bind_rows(hog_zygnema, metabolite_tibble_zygnema)
#-------------------------------------------------------------------------------
# Merging data using inner join
#-------------------------------------------------------------------------------
# mesotaenium
meso <- meso_heat %>%
    full_join(meso_cold, by = "pair") %>%
    full_join(meso_highlight, by = "pair") %>%
    mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
    mutate(regulator.x = ifelse(is.na(regulator.x), ifelse(is.na(regulator.y), regulator, regulator.y), regulator.x)) %>%
    mutate(target.x = ifelse(is.na(target.x), ifelse(is.na(target.y), target, target.y), target.x)) %>%
    select(c(regulator.x, target.x, mean_weight)) %>%
    distinct() %>%
    arrange(desc(mean_weight))
colnames(meso) <- c("regulator", "target", "weight")
meso <- meso %>%
    left_join(hog_meso, by = join_by("regulator" == "M_endlicherianum")) %>%
    select(c(HOG, target, weight)) %>%
    drop_na(HOG)
colnames(meso) <- c("regulator", "target", "weight")
meso <- meso %>%
    left_join(hog_meso, by = join_by("target" == "M_endlicherianum")) %>%
    select(c(regulator, HOG, weight))
colnames(meso) <- c("regulator", "target", "weight")
meso <- meso %>%
    mutate(pair = paste0(regulator, "-",target)) %>%
    drop_na() %>%
    filter(regulator != target) %>%
    distinct(pair, .keep_all = T)
# zygnema
zygnema <- zygnema_cold %>%
    full_join(zygnema_heat, by = "pair") %>%
    full_join(zygnema_highlight, by = "pair") %>%
    mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
    mutate(regulator.x = ifelse(is.na(regulator.x), ifelse(is.na(regulator.y), regulator, regulator.y), regulator.x)) %>%
    mutate(target.x = ifelse(is.na(target.x), ifelse(is.na(target.y), target, target.y), target.x)) %>%
    select(c(regulator.x, target.x, mean_weight)) %>%
    distinct() %>%
    arrange(desc(mean_weight))
colnames(zygnema) <- c("regulator", "target", "weight")
zygnema <- zygnema %>%
    left_join(hog_zygnema, by = join_by("regulator" == "Z_circumcarinatum_SAG698_1b")) %>%
    select(c(HOG, target, weight)) %>%
    drop_na(HOG)
colnames(zygnema) <- c("regulator", "target", "weight")
zygnema <- zygnema %>%
    left_join(hog_zygnema, by = join_by("target" == "Z_circumcarinatum_SAG698_1b")) %>%
    select(c(regulator, HOG, weight))
colnames(zygnema) <- c("regulator", "target", "weight")
zygnema <- zygnema %>%
    mutate(pair = paste0(regulator, "-",target)) %>%
    drop_na() %>%
    filter(regulator != target) %>%
    distinct(pair, .keep_all = T)
# physco
physco <- physco_cold %>%
    full_join(physco_heat, by = "pair") %>%
    full_join(physco_highlight, by = "pair") %>%
    mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
    mutate(regulator.x = ifelse(is.na(regulator.x), ifelse(is.na(regulator.y), regulator, regulator.y), regulator.x)) %>%
    mutate(target.x = ifelse(is.na(target.x), ifelse(is.na(target.y), target, target.y), target.x)) %>%
    select(c(regulator.x, target.x, mean_weight)) %>%
    distinct() %>%
    arrange(desc(mean_weight))
colnames(physco) <- c("regulator", "target", "weight")
physco <- physco %>%
    left_join(hog_physco, by = join_by("regulator" == "P_patens")) %>%
    select(c(HOG, target, weight)) %>%
    drop_na(HOG)
colnames(physco) <- c("regulator", "target", "weight")
physco <- physco %>%
    left_join(hog_physco, by = join_by("target" == "P_patens")) %>%
    select(c(regulator, HOG, weight))
colnames(physco) <- c("regulator", "target", "weight")
physco <- physco %>%
    mutate(pair = paste0(regulator, "-",target)) %>%
    drop_na() %>%
    filter(regulator != target) %>%
    distinct(pair, .keep_all = T)
# Due to the out of memory error
rm(hog_meso, hog_physco, hog_zygnema, meso_cold, meso_heat, meso_highlight,
   zygnema_cold, zygnema_heat, zygnema_highlight, physco_cold, physco_heat, physco_highlight, swing_all)
# merge all
swing_all <- meso %>%
    inner_join(zygnema, by = "pair") %>%
    inner_join(physco, by = "pair")  %>%
    mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
    mutate(pair = paste0(regulator, "-", "target")) %>%
    distinct(pair, .keep_all = TRUE) %>%
    select(c(regulator, target, mean_weight))
colnames(swing_all) <- c("regulator", "target", "weight")
swing_all <- swing_all %>%
    filter(regulator != target) %>%
    arrange(desc(weight))
#-------------------------------------------------------------------------------
# Connectivity analysis
#-------------------------------------------------------------------------------
network_all <- graph_from_data_frame(d=swing_all, directed=T) 
degree_all_all <- degree(network_all, normalized = TRUE, loops = FALSE, mode = "all")
network_meso <- graph_from_data_frame(d=meso[,1:3], directed=T) 
degree_meso_all <- degree(network_meso, normalized = TRUE, loops = FALSE, mode = "all")
network_physco <- graph_from_data_frame(d=physco[,1:3], directed=T) 
degree_physco_all <- degree(network_physco, normalized = TRUE, loops = FALSE, mode = "all")
network_zygnema <- graph_from_data_frame(d=zygnema[,1:3], directed=T) 
degree_zygnema_all <- degree(network_zygnema, normalized = TRUE, loops = FALSE, mode = "all")

#-------------------------------------------------------------------------------
# Histogram of degrees
#-------------------------------------------------------------------------------
p1 <- ggplot(as.data.frame(degree_all_all), aes(x = degree_all_all)) +
    geom_histogram() +
    labs( x = "degree", title = "Distribution of degrees in the conserved network | HOGs") +
    theme_bw()
p2 <- ggplot(as.data.frame(degree_meso_all), aes(x = degree_meso_all)) +
    geom_histogram() +
    labs( x = "degree", title = "Distribution of degrees in the Mesotaenium network | HOGs") +
    theme_bw()
p3 <- ggplot(as.data.frame(degree_zygnema_all), aes(x = degree_zygnema_all)) +
    geom_histogram() +
    labs( x = "degree", title = "Distribution of degrees in the Zygnema network | HOGs") +
    theme_bw()
p4 <- ggplot(as.data.frame(degree_physco_all), aes(x = degree_physco_all)) +
    geom_histogram() +
    labs( x = "degree", title = "Distribution of degrees in the Physcomitirum network | HOGs") +
    theme_bw()

pdf(file = "plots/degree_distribution_HOGs.pdf",   # The directory you want to save the file in
    width = 5, # The width of the plot in inches
    height = 15) # The height of the plot in inches
p <- plot_grid(p1, p3, p2, p4,
               nrow = 4,
               ncol = 1,
               labels = "AUTO")
print(p)
# Step 3: Run dev.off() to create the file!
dev.off()

# Convert each list into a tibble with two columns: names and values
tibble_all <- tibble(names = names(degree_all_all), all = unlist(degree_all_all))
tibble_zygnema <- tibble(names = names(degree_zygnema_all), zygnema = unlist(degree_zygnema_all))
tibble_mesotaenium <- tibble(names = names(degree_meso_all), mesotaenium = unlist(degree_meso_all))
tibble_physcomitrium <- tibble(names = names(degree_physco_all), physcomitrium = unlist(degree_physco_all))

# Merge the lists by the 'names' column
degree_table <- full_join(tibble_all, tibble_zygnema, by = "names") %>%
    full_join(tibble_mesotaenium, by = "names") %>%
    full_join(tibble_physcomitrium, by = "names") %>%
    arrange(desc(all))
write_tsv(x = degree_table, file = "tables/degree_values_sorted_by_conserved_network_HOGs.tsv")

#####
# Do the ranked list table
#####
rank_list_all <- rank(-degree_all_all, ties.method = "min")
rank_list_zygnema <- rank(-degree_zygnema_all, ties.method = "min")
rank_list_meso <- rank(-degree_meso_all, ties.method = "min")
rank_list_physco <- rank(-degree_physco_all, ties.method = "min")

# Convert each list into a tibble with two columns: names and values
tibble_all <- tibble(names = names(rank_list_all), rank_all = unlist(rank_list_all))
tibble_zygnema <- tibble(names = names(rank_list_zygnema), rank_zygnema = unlist(rank_list_zygnema))
tibble_mesotaenium <- tibble(names = names(rank_list_meso), rank_mesotaenium = unlist(rank_list_meso))
tibble_physcomitrium <- tibble(names = names(rank_list_physco), rank_physcomitrium = unlist(rank_list_physco))
# Merge the lists by the 'names' column
rank_table <- full_join(tibble_all, tibble_zygnema, by = "names") %>%
    full_join(tibble_mesotaenium, by = "names") %>%
    full_join(tibble_physcomitrium, by = "names") %>%
    arrange(rank_all)
write_tsv(x = degree_table, file = "tables/connectivity_ranked_sorted_by_conserved_network_HOGs.tsv")

top50_all_conserved <- names(head(sort(degree_all_all, decreasing = T), n = 50))

rank_table_no_na <- rank_table %>%
    filter(names %in% top50_all_conserved) 

rank_table_no_na$rank_all <- rescale(rank_table_no_na$rank_all, to = c(0, 1))
rank_table_no_na$rank_zygnema <- rescale(rank_table_no_na$rank_zygnema, to = c(0, 1))
rank_table_no_na$rank_mesotaenium <- rescale(rank_table_no_na$rank_mesotaenium, to = c(0, 1))
rank_table_no_na$rank_physcomitrium <- rescale(rank_table_no_na$rank_physcomitrium, to = c(0, 1))

rank_table_no_na_symbols <- rank_table_no_na %>%
    left_join(hog_ath_annotation, by = join_by("names" == "HOG")) %>%
    mutate(names = paste0(names, " - ", symbols)) %>%
    select(-symbols)

rank_table_no_na_names <- rank_table_no_na_symbols$names
rank_table_no_na_symbols <- rank_table_no_na_symbols[, -1]
rownames(rank_table_no_na_symbols) <- rank_table_no_na_names
rank_table_no_na_matrix <- as.matrix(rank_table_no_na_symbols)
## rescale the numbers columnwise before converting it to a matrix
myheatcolors <- c("#FFFFFF","#f8f9fa", "#e9ecef", "#dee2e6", "#ced4da",
                  "#adb5bd", "#6c757d", "#495057", "#343a40", "#212529")
pdf(file = "plots/connectivity_ranked_top_50_from_conserved_network_HOGs_with_symbols_added.pdf", width = 30, height = 5)
pheatmap(rank_table_no_na_matrix,
         color = rev(myheatcolors),
         cellwidth = 5, cellheight = 5,   # changed to 3
         border_color="grey",
         scale = "none",
         cluster_rows = T,
         cluster_cols = F,
         fontsize = 2,
         main = "The normalized between 0 and 1 ranked position of top 50 most connected nodes in the conserved network among all networks | HOGs",
)
dev.off()
#####
# Filter for conserved DPGP clusters
#####
meso_heat <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/tables/Me_heat.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
meso_cold <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/tables/Me_cold.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
meso_highlight <- read_tsv("../../mesotaenium_endlicherianum/Analyses/SWING/tables/Me_highlight.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
zygnema_cold <- read_tsv("../../zygnema_circumcarinatum_1b/Analyses/SWING/tables/Zc_cold.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
zygnema_heat <- read_tsv("../../zygnema_circumcarinatum_1b/Analyses/SWING/tables/Zc_heat.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
zygnema_highlight <- read_tsv("../../zygnema_circumcarinatum_1b/Analyses/SWING/tables/Zc_highlight.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
physco_cold <- read_tsv("../../physcomitrium_patens/Analyses/SWING/tables/Pp_cold.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
physco_heat <- read_tsv("../../physcomitrium_patens/Analyses/SWING/tables/Pp_heat.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
physco_highlight <- read_tsv("../../physcomitrium_patens/Analyses/SWING/tables/Pp_highlight.tsv") %>%
    mutate(pair = paste0(regulator, "-", target))
# Functional annotation
hog_meso <- read_tsv("../../orthofinder/Phylogenetic_Hierarchical_Orthogroups/N0.tsv") %>%
    select(c(HOG, M_endlicherianum)) %>%
    drop_na(M_endlicherianum) %>%
    separate_rows(M_endlicherianum, sep = ", ") %>%
    distinct()
hog_physco <- read_tsv("../../orthofinder/Phylogenetic_Hierarchical_Orthogroups/N0.tsv") %>%
    select(c(HOG, P_patens)) %>%
    drop_na(P_patens) %>%
    separate_rows(P_patens, sep = ", ") %>%
    distinct() %>%
    mutate(P_patens = paste0(P_patens, "V3"))
hog_zygnema <- read_tsv("../../orthofinder/Phylogenetic_Hierarchical_Orthogroups/N0.tsv") %>%
    select(c(HOG, Z_circumcarinatum_SAG698_1b)) %>%
    drop_na(Z_circumcarinatum_SAG698_1b) %>%
    separate_rows(Z_circumcarinatum_SAG698_1b, sep = ", ") %>%
    distinct()

metabolites <- c("viol",	"9_cis_neo",	"anthera",	"Chlb",	"Lut",	"Zeax",	"Chla",	"alpha_car",	"11_cis",	"beta_car",	"9_cis",	"6MHO",	"beta_CC",	"beta_Io",	"DHA",	"DHA_per_beta_Io",	"Chla_per_b",	"beta_car_per_9_cis_neox",	"V_A_Z_per_Chla_b",	"A_Z_per_V_A_Z",	"beta_Car_per_beta_CC",	"beta_Car_per_beta_Io",	"beta_Car_per_DHA",	"beta_Car_per_beta_CC_beta_Io_DHA")
metabolite_tibble_meso <- tibble(
    HOG = metabolites,
    M_endlicherianum = metabolites
)
metabolite_tibble_physco <- tibble(
    HOG = metabolites,
    P_patens = metabolites
)
metabolite_tibble_zygnema <- tibble(
    HOG = metabolites,
    Z_circumcarinatum_SAG698_1b = metabolites
)
hog_meso <- bind_rows(hog_meso, metabolite_tibble_meso)
hog_physco <- bind_rows(hog_physco, metabolite_tibble_physco)
hog_zygnema <- bind_rows(hog_zygnema, metabolite_tibble_zygnema)
meso_dpgp_heat <- read_tsv("../../mesotaenium_endlicherianum/Analyses/DPGP/results/heat/_optimal_clustering.txt") %>%
    filter(cluster %in% c(3, 10, 11)) %>%
    filter(probability >= 0.7)
meso_dpgp_heat <- read_tsv("../../mesotaenium_endlicherianum/Analyses/DPGP/results/high_light_no_recovered/_optimal_clustering.txt") %>%
    filter(cluster %in% c(1, 2 , 7, 5)) %>%
    filter(probability >= 0.7)
mesoDPGP <- meso_dpgp_heat %>%
    bind_rows(meso_dpgp_heat) %>%
    select(gene) %>%
    distinct() %>%
    unlist() %>%
    as.vector()
zyg_dpgp_heat <- read_tsv("../../zygnema_circumcarinatum_1b/Analyses/DPGP/results/heat/_optimal_clustering.txt") %>%
    filter(cluster %in% c(3, 4)) %>%
    filter(probability >= 0.7)
zyg_dpgp_highlight <- read_tsv("../../zygnema_circumcarinatum_1b/Analyses/DPGP/results/high_light/_optimal_clustering.txt") %>%
    filter(cluster %in% c(1, 3, 4)) %>%
    filter(probability >= 0.7)
zygnemaDPGP <- zyg_dpgp_heat %>%
    bind_rows(zyg_dpgp_highlight) %>%
    select(gene) %>%
    distinct() %>%
    unlist() %>%
    as.vector()
physco_dpgp_cold <- read_tsv("../../physcomitrium_patens/Analyses/DPGP/results/cold/_optimal_clustering.txt") %>%
    filter(cluster %in% c(16)) %>%
    filter(probability >= 0.7)
physco_dpgp_heat <- read_tsv("../../physcomitrium_patens/Analyses/DPGP/results/heat/_optimal_clustering.txt") %>%
    filter(cluster %in% c(6)) %>%
    filter(probability >= 0.7)
physco_dpgp_highlight <- read_tsv("../../physcomitrium_patens/Analyses/DPGP/results/high_light/_optimal_clustering.txt") %>%
    filter(cluster %in% c(1, 4, 7)) %>%
    filter(probability >= 0.7)
physcoDPGP <- physco_dpgp_heat %>%
    bind_rows(physco_dpgp_highlight) %>%
    select(gene) %>%
    distinct() %>%
    unlist() %>%
    as.vector()
#-------------------------------------------------------------------------------
# Merging data using inner join
#-------------------------------------------------------------------------------
# mesotaenium
meso <- meso_heat %>%
    full_join(meso_cold, by = "pair") %>%
    full_join(meso_highlight, by = "pair") %>%
    mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
    mutate(regulator.x = ifelse(is.na(regulator.x), ifelse(is.na(regulator.y), regulator, regulator.y), regulator.x)) %>%
    mutate(target.x = ifelse(is.na(target.x), ifelse(is.na(target.y), target, target.y), target.x)) %>%
    select(c(regulator.x, target.x, mean_weight)) %>%
    distinct() %>%
    arrange(desc(mean_weight)) %>%
    filter((regulator.x %in% mesoDPGP) | (target.x %in% mesoDPGP))
colnames(meso) <- c("regulator", "target", "weight")
meso <- meso %>%
    left_join(hog_meso, by = join_by("regulator" == "M_endlicherianum")) %>%
    select(c(HOG, target, weight)) %>%
    drop_na(HOG)
colnames(meso) <- c("regulator", "target", "weight")
meso <- meso %>%
    left_join(hog_meso, by = join_by("target" == "M_endlicherianum")) %>%
    select(c(regulator, HOG, weight))
colnames(meso) <- c("regulator", "target", "weight")
meso <- meso %>%
    mutate(pair = paste0(regulator, "-",target)) %>%
    drop_na() %>%
    filter(regulator != target) %>%
    distinct(pair, .keep_all = T)
# zygnema
zygnema <- zygnema_cold %>%
    full_join(zygnema_heat, by = "pair") %>%
    full_join(zygnema_highlight, by = "pair") %>%
    mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
    mutate(regulator.x = ifelse(is.na(regulator.x), ifelse(is.na(regulator.y), regulator, regulator.y), regulator.x)) %>%
    mutate(target.x = ifelse(is.na(target.x), ifelse(is.na(target.y), target, target.y), target.x)) %>%
    select(c(regulator.x, target.x, mean_weight)) %>%
    distinct() %>%
    arrange(desc(mean_weight)) %>%
    filter((regulator.x %in% zygnemaDPGP) | (target.x %in% zygnemaDPGP))
colnames(zygnema) <- c("regulator", "target", "weight")
zygnema <- zygnema %>%
    left_join(hog_zygnema, by = join_by("regulator" == "Z_circumcarinatum_SAG698_1b")) %>%
    select(c(HOG, target, weight)) %>%
    drop_na(HOG)
colnames(zygnema) <- c("regulator", "target", "weight")
zygnema <- zygnema %>%
    left_join(hog_zygnema, by = join_by("target" == "Z_circumcarinatum_SAG698_1b")) %>%
    select(c(regulator, HOG, weight))
colnames(zygnema) <- c("regulator", "target", "weight")
zygnema <- zygnema %>%
    mutate(pair = paste0(regulator, "-",target)) %>%
    drop_na() %>%
    filter(regulator != target) %>%
    distinct(pair, .keep_all = T)
# physco
physco <- physco_cold %>%
    full_join(physco_heat, by = "pair") %>%
    full_join(physco_highlight, by = "pair") %>%
    mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
    mutate(regulator.x = ifelse(is.na(regulator.x), ifelse(is.na(regulator.y), regulator, regulator.y), regulator.x)) %>%
    mutate(target.x = ifelse(is.na(target.x), ifelse(is.na(target.y), target, target.y), target.x)) %>%
    select(c(regulator.x, target.x, mean_weight)) %>%
    distinct() %>%
    arrange(desc(mean_weight)) %>%
    filter((regulator.x %in% physcoDPGP) | (target.x %in% physcoDPGP))
colnames(physco) <- c("regulator", "target", "weight")
physco <- physco %>%
    left_join(hog_physco, by = join_by("regulator" == "P_patens")) %>%
    select(c(HOG, target, weight)) %>%
    drop_na(HOG)
colnames(physco) <- c("regulator", "target", "weight")
physco <- physco %>%
    left_join(hog_physco, by = join_by("target" == "P_patens")) %>%
    select(c(regulator, HOG, weight))
colnames(physco) <- c("regulator", "target", "weight")
physco <- physco %>%
    mutate(pair = paste0(regulator, "-",target)) %>%
    drop_na() %>%
    filter(regulator != target) %>%
    distinct(pair, .keep_all = T)
# Due to the out of memory error
rm(hog_meso, hog_physco, hog_zygnema, meso_cold, meso_heat, meso_highlight,
   zygnema_cold, zygnema_heat, zygnema_highlight, physco_cold, physco_heat, physco_highlight, swing_all)
# merge all
swing_all <- meso %>%
    inner_join(zygnema, by = "pair") %>%
    inner_join(physco, by = "pair")  %>%
    mutate(mean_weight = rowMeans(cbind(weight.x, weight.y, weight), na.rm = TRUE)) %>%
    mutate(pair = paste0(regulator, "-", "target")) %>%
    distinct(pair, .keep_all = TRUE) %>%
    select(c(regulator, target, mean_weight))
colnames(swing_all) <- c("regulator", "target", "weight")
swing_all <- swing_all %>%
    filter(regulator != target) %>%
    arrange(desc(weight))
#-------------------------------------------------------------------------------
# Connectivity analysis
#-------------------------------------------------------------------------------
network_all <- graph_from_data_frame(d=swing_all, directed=T) 
degree_all_all <- degree(network_all, normalized = TRUE, loops = FALSE, mode = "all")
network_meso <- graph_from_data_frame(d=meso[,1:3], directed=T) 
degree_meso_all <- degree(network_meso, normalized = TRUE, loops = FALSE, mode = "all")
network_physco <- graph_from_data_frame(d=physco[,1:3], directed=T) 
degree_physco_all <- degree(network_physco, normalized = TRUE, loops = FALSE, mode = "all")
network_zygnema <- graph_from_data_frame(d=zygnema[,1:3], directed=T) 
degree_zygnema_all <- degree(network_zygnema, normalized = TRUE, loops = FALSE, mode = "all")


# Convert each list into a tibble with two columns: names and values
tibble_all <- tibble(names = names(degree_all_all), all = unlist(degree_all_all))
tibble_zygnema <- tibble(names = names(degree_zygnema_all), zygnema = unlist(degree_zygnema_all))
tibble_mesotaenium <- tibble(names = names(degree_meso_all), mesotaenium = unlist(degree_meso_all))
tibble_physcomitrium <- tibble(names = names(degree_physco_all), physcomitrium = unlist(degree_physco_all))

# Merge the lists by the 'names' column
degree_table <- full_join(tibble_all, tibble_zygnema, by = "names") %>%
    full_join(tibble_mesotaenium, by = "names") %>%
    full_join(tibble_physcomitrium, by = "names") %>%
    arrange(desc(all))

#####
# Do the ranked list table
#####
rank_list_all <- rank(-degree_all_all, ties.method = "min")
rank_list_zygnema <- rank(-degree_zygnema_all, ties.method = "min")
rank_list_meso <- rank(-degree_meso_all, ties.method = "min")
rank_list_physco <- rank(-degree_physco_all, ties.method = "min")

# Convert each list into a tibble with two columns: names and values
tibble_all <- tibble(names = names(rank_list_all), rank_all = unlist(rank_list_all))
tibble_zygnema <- tibble(names = names(rank_list_zygnema), rank_zygnema = unlist(rank_list_zygnema))
tibble_mesotaenium <- tibble(names = names(rank_list_meso), rank_mesotaenium = unlist(rank_list_meso))
tibble_physcomitrium <- tibble(names = names(rank_list_physco), rank_physcomitrium = unlist(rank_list_physco))
# Merge the lists by the 'names' column
rank_table <- full_join(tibble_all, tibble_zygnema, by = "names") %>%
    full_join(tibble_mesotaenium, by = "names") %>%
    full_join(tibble_physcomitrium, by = "names") %>%
    arrange(rank_all)
write_tsv(x = degree_table, file = "tables/connectivity_ranked_sorted_by_conserved_network_HOGs_filtered_for_DPGP_interesting_clusters.tsv")

top50_all_conserved <- names(head(sort(degree_all_all, decreasing = T), n = 50))

rank_table_no_na <- rank_table %>%
    filter(names %in% top50_all_conserved) 

rank_table_no_na$rank_all <- rescale(rank_table_no_na$rank_all, to = c(0, 1))
rank_table_no_na$rank_zygnema <- rescale(rank_table_no_na$rank_zygnema, to = c(0, 1))
rank_table_no_na$rank_mesotaenium <- rescale(rank_table_no_na$rank_mesotaenium, to = c(0, 1))
rank_table_no_na$rank_physcomitrium <- rescale(rank_table_no_na$rank_physcomitrium, to = c(0, 1))

rank_table_no_na_symbols <- rank_table_no_na %>%
    left_join(hog_ath_annotation, by = join_by("names" == "HOG")) %>%
    mutate(names = paste0(names, " - ", symbols)) %>%
    select(-symbols)

rank_table_no_na_names <- rank_table_no_na_symbols$names
rank_table_no_na_symbols <- rank_table_no_na_symbols[, -1]
rownames(rank_table_no_na_symbols) <- rank_table_no_na_names
rank_table_no_na_matrix <- as.matrix(rank_table_no_na_symbols)
## rescale the numbers columnwise before converting it to a matrix
myheatcolors <- c("#FFFFFF","#f8f9fa", "#e9ecef", "#dee2e6", "#ced4da",
                  "#adb5bd", "#6c757d", "#495057", "#343a40", "#212529")
pdf(file = "plots/connectivity_ranked_top_50_from_conserved_network_HOGs_with_symbols_addedfiltered_for_DPGP_interesting_clusters.pdf", width = 30, height = 5)
pheatmap(rank_table_no_na_matrix,
         color = rev(myheatcolors),
         cellwidth = 5, cellheight = 5,   # changed to 3
         border_color="grey",
         scale = "none",
         cluster_rows = T,
         cluster_cols = F,
         fontsize = 2,
         main = "The normalized between 0 and 1 ranked position of top 50 most connected nodes in the conserved network among all networks filtered_for_DPGP_interesting_clusters | HOGs",
)
dev.off()
# top 100
top100_all_conserved <- names(head(sort(degree_all_all, decreasing = T), n = 100))

rank_table_no_na <- rank_table %>%
    filter(names %in% top100_all_conserved) 

rank_table_no_na$rank_all <- rescale(rank_table_no_na$rank_all, to = c(0, 1))
rank_table_no_na$rank_zygnema <- rescale(rank_table_no_na$rank_zygnema, to = c(0, 1))
rank_table_no_na$rank_mesotaenium <- rescale(rank_table_no_na$rank_mesotaenium, to = c(0, 1))
rank_table_no_na$rank_physcomitrium <- rescale(rank_table_no_na$rank_physcomitrium, to = c(0, 1))

rank_table_no_na_symbols <- rank_table_no_na %>%
    left_join(hog_ath_annotation, by = join_by("names" == "HOG")) %>%
    mutate(names = paste0(names, " - ", symbols)) %>%
    select(-symbols)

rank_table_no_na_names <- rank_table_no_na_symbols$names
rank_table_no_na_symbols <- rank_table_no_na_symbols[, -1]
rownames(rank_table_no_na_symbols) <- rank_table_no_na_names
rank_table_no_na_matrix <- as.matrix(rank_table_no_na_symbols)
## rescale the numbers columnwise before converting it to a matrix
myheatcolors <- c("#FFFFFF","#f8f9fa", "#e9ecef", "#dee2e6", "#ced4da",
                  "#adb5bd", "#6c757d", "#495057", "#343a40", "#212529")
pdf(file = "plots/connectivity_ranked_top_100_from_conserved_network_HOGs_with_symbols_addedfiltered_for_DPGP_interesting_clusters.pdf", width = 30, height = 10)
pheatmap(rank_table_no_na_matrix,
         color = rev(myheatcolors),
         cellwidth = 5, cellheight = 5,   # changed to 3
         border_color="grey",
         scale = "none",
         cluster_rows = T,
         cluster_cols = F,
         fontsize = 2,
         main = "The normalized between 0 and 1 ranked position of top 100 most connected nodes in the conserved network among all networks filtered_for_DPGP_interesting_clusters | HOGs",
)
dev.off()
