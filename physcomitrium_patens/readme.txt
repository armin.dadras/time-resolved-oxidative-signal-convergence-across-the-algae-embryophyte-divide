####################
RNA-Seq quantification
####################

The transcriptome file was downloaded from Phytozome for P. patens (Ppatens_318_v3.3.transcript.fa.gz).

Cite: Lang, D., Ullrich, K. K., Murat, F., Fuchs, J., Jenkins, J., Haas, F. B., … Rensing, S. A. (2018). ThePhyscomitrella patenschromosome-scale assembly reveals moss genome structure and evolution. The Plant Journal, 93(3), 515–533. https://doi.org/10.1111/tpj.13801

Then, the strandness of reads were checked using the snakemake pipeline and this is a "reverse" library.


For some samples, we had more than two files per sample since the sequencing facility sequenced them on more than one lane. I merged the forward runs to one forward and reverse files into one revese file for those samples (9, 11, 12, 13, 25, 26, 28, 29, 38, 43, 44, 52, 55, 56). Afterwards, the reads got quantified using the snakemake pipeline.
