################################################################################
########## Load packages
################################################################################
library(tidyverse)
library(cowplot)
################################################################################
########## Load the data and set parameters
################################################################################
# Gene expression viewer
# Define a scale function for z-score transformation
scale_z_score <- function(x){
    (x - mean(x, na.rm=TRUE)) / sd(x, na.rm=TRUE)
}
# Define a palette
my_colors_samples <- c(#"standard_growth_0" = "#e2e6e6",
                       "standard_growth_0.5" = "#c6cdce",
                       "standard_growth_1" = "#a8b4b5",
                       "standard_growth_2" = "#8b9b9d",
                       "standard_growth_4" = "#6e8284",
                       "standard_growth_6" = "#51696c",
                       "standard_growth_24" = "#345053",
                       "cold_0.5" = "#91e0ef",
                       "cold_1" = "#48cae4",
                       "cold_2" = "#00b4d8",
                       "cold_4" = "#0296c7",
                       "cold_6" = "#0077b6",
                       "cold_24" = "#023e8a",
                       "heat_0.5" = "#fdc3c3",
                       "heat_1" = "#feaeae",
                       "heat_2" = "#fe9a9a",
                       "heat_4" = "#fe8585",
                       "heat_6" = "#fe5d5d",
                       "heat_24" = "#fe4848",
                       "high_light_s_0.25" = "#ffea01",
                       "high_light_s_0.5" = "#ffdd02",
                       "high_light_s_2" = "#ffd002",
                       "high_light_s_4" = "#ffb700",
                       "high_light_s_6" = "#ffa201",
                       "high_light_r_0.25" = "#dee602",
                       "high_light_r_0.5" = "#c3d205",
                       "high_light_r_1" = "#a7be07",
                       "high_light_r_2" = "#8daa0b",
                       "high_light_r_4" = "#70960e"
)
my_colors_treatments <- c("standard_growth" = "#345053",
                          "cold" = "#023e8a",
                          "heat" = "#fe4848",
                          "high_light_s" = "#ffa201",
                          "high_light_r" = "#70960e"
)
# There is an error and it can be solved via:
Sys.setenv(VROOM_CONNECTION_SIZE=500072)
mesotaenium_expr <- read_tsv("../../mesotaenium_endlicherianum/Analyses/EDA/tables/count_voom_transformed_qsmooth.tsv")
mesotaenium_expr_mat <- t(mesotaenium_expr[,-1])
colnames(mesotaenium_expr_mat) <- mesotaenium_expr$geneID
mesotaenium_expr_mat_tibble <- as_tibble(mesotaenium_expr_mat,rownames = "sample_name")
mesotaenium_design <- read_csv("../../mesotaenium_endlicherianum/study_design.csv")
mesotaenium <- mesotaenium_expr_mat_tibble %>%
    left_join(mesotaenium_design, by = "sample_name")

physcomitrium_expr <- read_tsv("../../physcomitrium_patens/Analyses/EDA/tables/count_voom_transformed_qsmooth.tsv")
physcomitrium_expr_mat <- t(physcomitrium_expr[,-1])
colnames(physcomitrium_expr_mat) <- physcomitrium_expr$geneID
physcomitrium_expr_mat_tibble <- as_tibble(physcomitrium_expr_mat,rownames = "sample_name")
physcomitrium_design <- read_csv("../../physcomitrium_patens/study_design.csv")
physcomitrium <- physcomitrium_expr_mat_tibble %>%
    left_join(physcomitrium_design, by = "sample_name")

zygnema_expr <- read_tsv("../../zygnema_circumcarinatum_1b/Analyses/EDA/tables/count_voom_transformed_qsmooth.tsv")
zygnema_expr_mat <- t(zygnema_expr[,-1])
colnames(zygnema_expr_mat) <- zygnema_expr$geneID
zygnema_expr_mat_tibble <- as_tibble(zygnema_expr_mat,rownames = "sample_name")
zygnema_design <- read_csv("../../zygnema_circumcarinatum_1b/study_design.csv")
zygnema <- zygnema_expr_mat_tibble %>%
    left_join(zygnema_design, by = "sample_name")
################################################################################
########## Plot
################################################################################
# Step 2: Create the plot with R code
#par(mfcol = c(3,4)) # rows, columns
#par(mar = c(2.7,1.2,2.7,1.2))
species <- c("mesotaenium", "physcomitrium", "zygnema")
treatments <- c("standard_growth", "cold", "heat", "high_light")
metabolites <- c("viol_micromol_per_mgDW", "9_cis_neo_micromol_per_mgDW", "anthera_micromol_per_mgDW", "Chlb_micromol_per_mgDW", "Lut_micromol_per_mgDW", "Zeax_micromol_per_mgDW", "Chla_micromol_per_mgDW", "alpha_car_micromol_per_mgDW", "11_cis_micromol_per_mgDW", "beta_car_micromol_per_mgDW", "9_cis_micromol_per_mgDW", "6MHO_nmol_per_mgDW", "beta_CC_nmol_per_mgDW", "beta_Io_nmol_per_mgDW", "DHA_nmol_per_mgDW", "DHA_per_beta_Io", "Chla_per_b", "beta_car_per_9_cis_neox", "V_A_Z_per_Chla_b", "A_Z_per_V_A_Z", "beta_Car_per_beta_CC", "beta_Car_per_beta_Io", "beta_Car_per_DHA", "beta_Car_per_beta_CC_beta_Io_DHA")
for (metabolite in metabolites){
    metabolite_min <- 0
    metabolite_max <- 0
    plots <- list()
    i <- 1
    # min
    if (metabolite_min == 0) {
        metabolite_min <- (min(mesotaenium[[metabolite]], na.rm = TRUE)/mean(mesotaenium[[metabolite]][mesotaenium[["time"]]== 0], na.rm = TRUE))
    }
    if (metabolite_min > (min(physcomitrium[[metabolite]], na.rm = TRUE)/mean(physcomitrium[[metabolite]][physcomitrium[["time"]]== 0], na.rm = TRUE))) {
        metabolite_min <- (min(physcomitrium[[metabolite]], na.rm = TRUE)/mean(physcomitrium[[metabolite]][physcomitrium[["time"]]== 0], na.rm = TRUE))
    }
    if (mean(zygnema[[metabolite]][zygnema[["time"]]== 0], na.rm = TRUE) == 0){
        metabolite_min <- 0
    } else if (metabolite_min > (min(zygnema[[metabolite]], na.rm = TRUE)/mean(zygnema[[metabolite]][zygnema[["time"]]== 0], na.rm = TRUE))) {
        metabolite_min <- (min(zygnema[[metabolite]], na.rm = TRUE)/mean(zygnema[[metabolite]][zygnema[["time"]]== 0], na.rm = TRUE))
    }
    # max
    if (metabolite_max == 0) {
        metabolite_max <- (max(mesotaenium[[metabolite]], na.rm = TRUE)/mean(mesotaenium[[metabolite]][mesotaenium[["time"]]== 0], na.rm = TRUE))
    }
    if (metabolite_max < (max(physcomitrium[[metabolite]], na.rm = TRUE)/mean(physcomitrium[[metabolite]][physcomitrium[["time"]]== 0], na.rm = TRUE))) {
        metabolite_max <- (max(physcomitrium[[metabolite]], na.rm = TRUE)/mean(physcomitrium[[metabolite]][physcomitrium[["time"]]== 0], na.rm = TRUE))
    }
    if (mean(zygnema[[metabolite]][zygnema[["time"]]== 0], na.rm = TRUE) == 0){
        metabolite_max <- metabolite_max
    } else if (metabolite_max < (max(zygnema[[metabolite]], na.rm = TRUE)/mean(zygnema[[metabolite]][zygnema[["time"]]== 0], na.rm = TRUE))) {
        metabolite_max <- (max(zygnema[[metabolite]], na.rm = TRUE)/mean(zygnema[[metabolite]][zygnema[["time"]]== 0], na.rm = TRUE))
    }
    for (organism in species) {
        for (treat in treatments) {
            if (treat != "high_light") {
                filtered <- get(organism) %>%
                    filter(treatment == treat) %>%
                    filter(time != 0) %>%
                    drop_na()
                box_color <- my_colors_samples[names(my_colors_samples) %in% filtered$condition]
                line_color <- my_colors_treatments[names(my_colors_treatments) %in% filtered$treatment]
                filtered$condition <- factor(filtered$condition, levels = unique(filtered$condition))
                filtered$treatment <- factor(filtered$treatment, levels = unique(filtered$treatment))
                
                if (mean(get(organism)[[metabolite]][get(organism)[["time"]]== 0], na.rm = TRUE) != 0){
                    filtered[[metabolite]] <- filtered[[metabolite]]/mean(get(organism)[[metabolite]][get(organism)[["time"]]== 0], na.rm = TRUE)
                } else {
                    filtered[[metabolite]] <- filtered[[metabolite]]/(1e-5) # one Zygnema measurement probably failed and it mean is 0
                }
                
                # Extract polynomial regression coefficients
                poly_coefs <- coef(lm((get(metabolite)) ~ condition, data = filtered))
                # Create a string representing the formula
                poly_formula <- sprintf("y = %.2f + %.2fx", poly_coefs[1], poly_coefs[2])
                
                p <- ggplot(filtered, aes(condition, get(metabolite))) +
                    geom_boxplot(width = .3, outlier.shape = NA, fill= box_color,  alpha = 0.3) +
                    geom_point(aes(fill=treatment), shape=21, size=2, alpha = .3) +
                    stat_smooth(aes(x = condition, y = get(metabolite), color = treatment, group = treatment), method = lm, formula = y ~ x,  linewidth = 0.5, alpha = .2, se = FALSE, show.legend = FALSE) +
                    ggtitle(paste0(organism, " - ", metabolite)) +
                    scale_fill_manual(values = box_color) +
                    scale_color_manual(values = line_color) +
                    scale_y_continuous(limits = c(metabolite_min, metabolite_max), breaks = seq(metabolite_min, metabolite_max, (metabolite_max-metabolite_min)/10)) +
                    geom_text(x = Inf, y = Inf, hjust = 1, vjust = 1, label = poly_formula, size = 4, color = "black", aes(label = paste("Formula: ", poly_formula))) +
                    labs(y = paste0(metabolite), x = "Condition") +
                    theme_bw() +
                    theme(axis.text.x = element_text(angle = 90))
                
                plots[[i]] <- p
                i <- i + 1
            } else if (treat == "high_light") {
                filtered <- get(organism) %>%
                    filter(treatment == "high_light_s" | treatment == "high_light_r") %>%
                    drop_na()
                
                if (mean(get(organism)[[metabolite]][get(organism)[["time"]]== 0], na.rm = TRUE) != 0){
                    filtered[[metabolite]] <- filtered[[metabolite]]/mean(get(organism)[[metabolite]][get(organism)[["time"]]== 0], na.rm = TRUE)
                } else {
                    filtered[[metabolite]] <- filtered[[metabolite]]/(1e-5) # one Zygnema measurement probably failed and it mean is 0
                }  
                
                box_color <- my_colors_samples[names(my_colors_samples) %in% filtered$condition]
                line_color <- my_colors_treatments[names(my_colors_treatments) %in% filtered$treatment]
                filtered$condition <- factor(filtered$condition, levels = c("high_light_s_0.25",
                                                                            "high_light_s_0.5",
                                                                            "high_light_s_2",
                                                                            "high_light_s_4",
                                                                            "high_light_s_6",
                                                                            "high_light_r_0.25",
                                                                            "high_light_r_0.5",
                                                                            "high_light_r_1",
                                                                            "high_light_r_2",
                                                                            "high_light_r_4"))
                filtered$treatment <- factor(filtered$treatment, levels = unique(filtered$treatment))
                
                # Your existing code...
                filtered_s <- get(organism) %>%
                    filter(treatment == "high_light_s")
                filtered_r <- get(organism) %>%
                    filter(treatment == "high_light_r")
                filtered_s$condition <- factor(filtered_s$condition, levels = c("high_light_s_0.25",
                                                                                "high_light_s_0.5",
                                                                                "high_light_s_2",
                                                                                "high_light_s_4",
                                                                                "high_light_s_6"))
                filtered_s$treatment <- factor(filtered_s$treatment, levels = unique(filtered_s$treatment))
                filtered_r$condition <- factor(filtered_r$condition, levels = c("high_light_r_0.25",
                                                                                "high_light_r_0.5",
                                                                                "high_light_r_1",
                                                                                "high_light_r_2",
                                                                                "high_light_r_4"))
                filtered_r$treatment <- factor(filtered_r$treatment, levels = unique(filtered_r$treatment))
                
                if (mean(get(organism)[[metabolite]][get(organism)[["time"]]== 0], na.rm = TRUE) != 0){
                    filtered_s[[metabolite]] <- filtered_s[[metabolite]]/mean(get(organism)[[metabolite]][get(organism)[["time"]]== 0], na.rm = TRUE)
                } else {
                    filtered_s[[metabolite]] <- filtered_s[[metabolite]]/(1e-5) # one Zygnema measurement probably failed and it mean is 0
                }
                if (mean(get(organism)[[metabolite]][get(organism)[["time"]]== 0], na.rm = TRUE) != 0){
                    filtered_r[[metabolite]] <- filtered_r[[metabolite]]/mean(get(organism)[[metabolite]][get(organism)[["time"]]== 0], na.rm = TRUE)
                } else {
                    filtered_r[[metabolite]] <- filtered_r[[metabolite]]/(1e-5) # one Zygnema measurement probably failed and it mean is 0
                }
                
                # Extract polynomial regression coefficients
                poly_coefs <- coef(lm((get(metabolite)) ~ condition, data = filtered_s))
                # Create a string representing the formula
                poly_formula_s <- sprintf("y = %.2f + %.2fx", poly_coefs[1], poly_coefs[2])
                # Extract polynomial regression coefficients
                poly_coefs <- coef(lm((get(metabolite)) ~ condition, data = filtered_r))
                # Create a string representing the formula
                poly_formula_r <- sprintf("y = %.2f + %.2fx", poly_coefs[1], poly_coefs[2])
                
                p <- ggplot(filtered, aes(condition, get(metabolite))) +
                    geom_boxplot(width = .3, outlier.shape = NA, fill= box_color,  alpha = 0.3) +
                    geom_point(aes(fill=treatment), shape=21, size=2, alpha = .3) +
                    stat_smooth(aes(x = condition, y = get(metabolite), color = treatment, group = treatment), method = lm, formula = y ~ x,  linewidth = 0.5, alpha = .2, se = FALSE, show.legend = FALSE) +
                    ggtitle(paste0(organism, " - ", metabolite)) +
                    scale_fill_manual(values = box_color) +
                    scale_color_manual(values = line_color) +
                    scale_y_continuous(limits = c(metabolite_min, metabolite_max), breaks = seq(metabolite_min, metabolite_max, (metabolite_max-metabolite_min)/10)) +
                    geom_text(x = Inf, y = Inf, hjust = 1, vjust = 1, label = poly_formula_s, size = 4, color = "black", aes(label = paste("Formula: ", poly_formula))) +
                    geom_text(x = Inf, y = Inf, hjust = 1, vjust = 4, label = poly_formula_r, size = 4, color = "black", aes(label = paste("Formula: ", poly_formula))) +
                    labs(y = paste0(metabolite), x = "Condition") +
                    theme_bw() +
                    theme(axis.text.x = element_text(angle = 90))
                
                plots[[i]] <- p
                i <- i + 1
            }
        }
    }
    
    # Step 1: Call the pdf command to start the plot
    pdf(file = paste0("plots/", metabolite, ".pdf"),   # The directory you want to save the file in
        width = 15, # The width of the plot in inches
        height = 9) # The height of the plot in inches
    # Step 2: Create the plot with R code
    p <- cowplot::plot_grid(plots[[1]], plots[[2]], plots[[3]], plots[[4]], plots[[5]], plots[[6]], plots[[7]],
                   plots[[8]], plots[[9]], plots[[10]], plots[[11]], plots[[12]], labels = "AUTO")
    print(p)
    # Step 3: Run dev.off() to create the file!
    dev.off()
    
    species <- c("mesotaenium", "physcomitrium", "zygnema")
    treatments <- c("standard_growth", "cold", "heat", "high_light")
    plots <- list()
    i <- 1
    
    for (organism in species) {
        for (treat in treatments) {
            if (treat != "high_light") {
                filtered <- get(organism) %>%
                    filter(treatment == treat) %>%
                    filter(time != 0) %>%
                    drop_na()
                
                if (mean(get(organism)[[metabolite]][get(organism)[["time"]]== 0], na.rm = TRUE) != 0){
                    filtered[[metabolite]] <- filtered[[metabolite]]/mean(get(organism)[[metabolite]][get(organism)[["time"]]== 0], na.rm = TRUE)
                } else {
                    filtered[[metabolite]] <- filtered[[metabolite]]/(1e-5) # one Zygnema measurement probably failed and it mean is 0
                }
                                
                box_color <- my_colors_samples[names(my_colors_samples) %in% filtered$condition]
                line_color <- my_colors_treatments[names(my_colors_treatments) %in% filtered$treatment]
                filtered$condition <- factor(filtered$condition, levels = unique(filtered$condition))
                filtered$treatment <- factor(filtered$treatment, levels = unique(filtered$treatment))
                
                # Extract polynomial regression coefficients
                poly_coefs <- coef(lm((get(metabolite)) ~ poly(condition, 2), data = filtered))
                # Create a string representing the formula
                poly_formula <- sprintf("y = %.2f + %.2fx", poly_coefs[1], poly_coefs[2])
                
                p <- ggplot(filtered, aes(condition, get(metabolite))) +
                    geom_boxplot(width = .3, outlier.shape = NA, fill= box_color,  alpha = 0.3) +
                    geom_point(aes(fill=treatment), shape=21, size=2, alpha = .3) +
                    stat_smooth(aes(x = condition, y = get(metabolite), color = treatment, group = treatment), method = lm, formula = y ~ x,  linewidth = 0.5, alpha = .2, se = FALSE, show.legend = FALSE) +
                    ggtitle(paste0(organism, " - ", metabolite)) +
                    scale_fill_manual(values = box_color) +
                    scale_color_manual(values = line_color) +
                    scale_y_continuous(limits = c(metabolite_min, metabolite_max), breaks = seq(metabolite_min, metabolite_max, (metabolite_max-metabolite_min)/10)) +
                    geom_text(x = Inf, y = Inf, hjust = 1, vjust = 1, label = poly_formula, size = 4, color = "black", aes(label = paste("Formula: ", poly_formula))) +
                    labs(y = paste0(metabolite), x = "Condition") +
                    theme_bw() +
                    theme(axis.text.x=element_blank())
                
                plots[[i]] <- p
                i <- i + 1
            } else if (treat == "high_light") {
                filtered <- get(organism) %>%
                    filter(treatment == "high_light_s" | treatment == "high_light_r") %>%
                    drop_na()
                
                if (mean(get(organism)[[metabolite]][get(organism)[["time"]]== 0], na.rm = TRUE) != 0){
                    filtered[[metabolite]] <- filtered[[metabolite]]/mean(get(organism)[[metabolite]][get(organism)[["time"]]== 0], na.rm = TRUE)
                } else {
                    filtered[[metabolite]] <- filtered[[metabolite]]/(1e-5) # one Zygnema measurement probably failed and it mean is 0
                }
                
                box_color <- my_colors_samples[names(my_colors_samples) %in% filtered$condition]
                line_color <- my_colors_treatments[names(my_colors_treatments) %in% filtered$treatment]
                filtered$condition <- factor(filtered$condition, levels = c("high_light_s_0.25",
                                                                            "high_light_s_0.5",
                                                                            "high_light_s_2",
                                                                            "high_light_s_4",
                                                                            "high_light_s_6",
                                                                            "high_light_r_0.25",
                                                                            "high_light_r_0.5",
                                                                            "high_light_r_1",
                                                                            "high_light_r_2",
                                                                            "high_light_r_4"))
                filtered$treatment <- factor(filtered$treatment, levels = unique(filtered$treatment))
                
                # Your existing code...
                filtered_s <- get(organism) %>%
                    filter(treatment == "high_light_s")
                filtered_r <- get(organism) %>%
                    filter(treatment == "high_light_r")
                filtered_s$condition <- factor(filtered_s$condition, levels = c("high_light_s_0.25",
                                                                                "high_light_s_0.5",
                                                                                "high_light_s_2",
                                                                                "high_light_s_4",
                                                                                "high_light_s_6"))
                filtered_s$treatment <- factor(filtered_s$treatment, levels = unique(filtered_s$treatment))
                filtered_r$condition <- factor(filtered_r$condition, levels = c("high_light_r_0.25",
                                                                                "high_light_r_0.5",
                                                                                "high_light_r_1",
                                                                                "high_light_r_2",
                                                                                "high_light_r_4"))
                filtered_r$treatment <- factor(filtered_r$treatment, levels = unique(filtered_r$treatment))
                
                if (mean(get(organism)[[metabolite]][get(organism)[["time"]]== 0], na.rm = TRUE) != 0){
                    filtered_s[[metabolite]] <- filtered_s[[metabolite]]/mean(get(organism)[[metabolite]][get(organism)[["time"]]== 0], na.rm = TRUE)
                } else {
                    filtered_s[[metabolite]] <- filtered_s[[metabolite]]/(1e-5) # one Zygnema measurement probably failed and it mean is 0
                }
                if (mean(get(organism)[[metabolite]][get(organism)[["time"]]== 0], na.rm = TRUE) != 0){
                    filtered_r[[metabolite]] <- filtered_r[[metabolite]]/mean(get(organism)[[metabolite]][get(organism)[["time"]]== 0], na.rm = TRUE)
                } else {
                    filtered_r[[metabolite]] <- filtered_r[[metabolite]]/(1e-5) # one Zygnema measurement probably failed and it mean is 0
                }
                
                # Extract polynomial regression coefficients
                poly_coefs <- coef(lm((get(metabolite)) ~ condition, data = filtered_s))
                # Create a string representing the formula
                poly_formula_s <- sprintf("y = %.2f + %.2fx", poly_coefs[1], poly_coefs[2])
                # Extract polynomial regression coefficients
                poly_coefs <- coef(lm((get(metabolite)) ~ condition, data = filtered_r))
                # Create a string representing the formula
                poly_formula_r <- sprintf("y = %.2f + %.2fx", poly_coefs[1], poly_coefs[2])
                
                p <- ggplot(filtered, aes(condition, get(metabolite))) +
                    geom_boxplot(width = .3, outlier.shape = NA, fill= box_color,  alpha = 0.3) +
                    geom_point(aes(fill=treatment), shape=21, size=2, alpha = .3) +
                    stat_smooth(aes(x = condition, y = get(metabolite), color = treatment, group = treatment), method = lm, formula = y ~ x,  linewidth = 0.5, alpha = .2, se = FALSE, show.legend = FALSE) +
                    ggtitle(paste0(organism, " - ", metabolite)) +
                    scale_fill_manual(values = box_color) +
                    scale_color_manual(values = line_color) +
                    scale_y_continuous(limits = c(metabolite_min, metabolite_max), breaks = seq(metabolite_min, metabolite_max, (metabolite_max-metabolite_min)/10)) +
                    geom_text(x = Inf, y = Inf, hjust = 1, vjust = 1, label = poly_formula_s, size = 4, color = "black", aes(label = paste("Formula: ", poly_formula))) +
                    geom_text(x = Inf, y = Inf, hjust = 1, vjust = 4, label = poly_formula_r, size = 4, color = "black", aes(label = paste("Formula: ", poly_formula))) +
                    labs(y = paste0(metabolite), x = "Condition") +
                    theme_bw() +
                    theme(axis.text.x=element_blank())
                
                plots[[i]] <- p
                i <- i + 1
            }
        }
    }
    
    # Step 1: Call the pdf command to start the plot
    pdf(file = paste0("plots/", metabolite, "2.pdf"),   # The directory you want to save the file in
        width = 15, # The width of the plot in inches
        height = 9) # The height of the plot in inches
    # Step 2: Create the plot with R code
    p <- plot_grid(plots[[1]], plots[[2]], plots[[3]],plots[[4]], plots[[5]], plots[[6]], plots[[7]],
                   plots[[8]], plots[[9]], plots[[10]], plots[[11]], plots[[12]], labels = "AUTO")
    print(p)
    # Step 3: Run dev.off() to create the file!
    dev.off()
    
    species <- c("mesotaenium", "physcomitrium", "zygnema")
    plots <- list()
    i <- 1
    for (organism in species) {
        filtered <- get(organism) %>%
            filter(time != 0)
        filtered$condition <- factor(filtered$condition, levels = unique(filtered$condition))
        filtered$treatment <- factor(filtered$treatment, levels = c("standard_growth",
                                                                    "cold",
                                                                    "heat",
                                                                    "high_light_s",
                                                                    "high_light_r"))
        
        filtered[[metabolite]] <- filtered[[metabolite]]/mean(get(organism)[[metabolite]][get(organism)[["time"]]== 0], na.rm = TRUE)
        
        
        p <- ggplot(filtered, aes(treatment, get(metabolite))) +
            ggdist::stat_halfeye(adjust = .5, width = .5, .width = 0, justification = -.4) +
            geom_boxplot(width = .3, outlier.shape = NA, fill= my_colors_treatments,  alpha = 0.3) +
            geom_jitter(aes(fill=treatment), shape=21, size=2, alpha = .3, width = 0.1) +
            ggtitle(paste0("Aggregated", metabolite, " - ", organism)) +
            labs(y = paste0(metabolite)) +
            scale_fill_manual(values = my_colors_treatments) +
            scale_color_manual(values = my_colors_treatments) +
            scale_y_continuous(limits = c(metabolite_min, metabolite_max), breaks = seq(metabolite_min, metabolite_max, (metabolite_max-metabolite_min)/10)) +
            theme_bw() +
            theme(axis.text.x = element_text(angle = 90),
                  legend.position = "none")
        plots[[i]] <- p
        i <- i + 1
    }
    
    # Step 1: Call the pdf command to start the plot
    pdf(file = paste0("plots/", metabolite, "_aggregated.pdf"),   # The directory you want to save the file in
        width = 3, # The width of the plot in inches
        height = 9) # The height of the plot in inches
    # Step 2: Create the plot with R code
    p <- plot_grid(plots[[1]], plots[[2]], plots[[3]], labels = "AUTO", ncol = 1)
    print(p)
    # Step 3: Run dev.off() to create the file!
    dev.off()
    
    species <- c("mesotaenium", "physcomitrium", "zygnema")
    plots <- list()
    i <- 1
    for (organism in species) {
        filtered <- get(organism) %>%
            filter(time != 0)
        filtered$condition <- factor(filtered$condition, levels = unique(filtered$condition))
        filtered$treatment <- factor(filtered$treatment, levels = c("standard_growth",
                                                                    "cold",
                                                                    "heat",
                                                                    "high_light_s",
                                                                    "high_light_r"))
        
        filtered[[metabolite]] <- filtered[[metabolite]]/mean(get(organism)[[metabolite]][get(organism)[["time"]]== 0], na.rm = TRUE)
        
        p <- ggplot(filtered, aes(treatment, get(metabolite))) +
            ggdist::stat_halfeye(adjust = .5, width = .5, .width = 0, justification = -.4) +
            geom_boxplot(width = .3, outlier.shape = NA, fill= my_colors_treatments,  alpha = 0.3) +
            geom_jitter(aes(fill=treatment), shape=21, size=2, alpha = .3, width = 0.1) +
            ggtitle(paste0("Aggregated metabolite", " - ", organism)) +
            labs(y = paste0(metabolite)) +
            scale_fill_manual(values = my_colors_treatments) +
            scale_color_manual(values = my_colors_treatments) +
            scale_y_continuous(limits = c(metabolite_min, metabolite_max), breaks = seq(metabolite_min, metabolite_max, (metabolite_max-metabolite_min)/10)) +
            theme_bw() +
            theme(legend.position = "none",
                  axis.text.x=element_blank())
        plots[[i]] <- p
        i <- i + 1
    }
    
    # Step 1: Call the pdf command to start the plot
    pdf(file = paste0("plots/", metabolite, "_aggregated2.pdf") ,   # The directory you want to save the file in
        width = 3, # The width of the plot in inches
        height = 9) # The height of the plot in inches
    # Step 2: Create the plot with R code
    p <- plot_grid(plots[[1]], plots[[2]], plots[[3]], labels = "AUTO", ncol = 1)
    print(p)
    # Step 3: Run dev.off() to create the file!
    dev.off()
}
