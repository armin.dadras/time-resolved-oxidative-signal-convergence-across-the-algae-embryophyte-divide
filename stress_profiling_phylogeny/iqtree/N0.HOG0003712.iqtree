IQ-TREE 1.6.12 built Dec  4 2019

Input file name: /data/armin/stress_profiling_phylogeny/msa/N0.HOG0003712.fa
Type of analysis: ModelFinder + tree reconstruction + ultrafast bootstrap (1000 replicates)
Random seed number: 1099

REFERENCES
----------

To cite ModelFinder please use: 

Subha Kalyaanamoorthy, Bui Quang Minh, Thomas KF Wong, Arndt von Haeseler,
and Lars S Jermiin (2017) ModelFinder: Fast model selection for
accurate phylogenetic estimates. Nature Methods, 14:587–589.
https://doi.org/10.1038/nmeth.4285

To cite IQ-TREE please use:

Lam-Tung Nguyen, Heiko A. Schmidt, Arndt von Haeseler, and Bui Quang Minh
(2015) IQ-TREE: A fast and effective stochastic algorithm for estimating
maximum likelihood phylogenies. Mol Biol Evol, 32:268-274.
https://doi.org/10.1093/molbev/msu300

Since you used ultrafast bootstrap (UFBoot) please also cite: 

Diep Thi Hoang, Olga Chernomor, Arndt von Haeseler, Bui Quang Minh,
and Le Sy Vinh (2017) UFBoot2: Improving the ultrafast bootstrap
approximation. Mol Biol Evol, in press.
https://doi.org/10.1093/molbev/msx281

SEQUENCE ALIGNMENT
------------------

Input data: 42 sequences with 2654 amino-acid sites
Number of constant sites: 870 (= 32.7807% of all sites)
Number of invariant (constant or ambiguous constant) sites: 870 (= 32.7807% of all sites)
Number of parsimony informative sites: 1178
Number of distinct site patterns: 2031

ModelFinder
-----------

Best-fit model according to BIC: JTT+F+I+G4

List of models sorted by BIC scores: 

Model             LogL          AIC      w-AIC      AICc     w-AICc       BIC      w-BIC
JTT+F+I+G4      -54590.8888 109385.7775 + 1.0000 109394.0143 + 1.0000 109985.9275 + 1.0000
JTTDCMut+F+I+G4 -54608.1345 109420.2690 - 0.0000 109428.5058 - 0.0000 110020.4190 - 0.0000
JTT+F+G4        -54620.2709 109442.5418 - 0.0000 109450.6155 - 0.0000 110036.8080 - 0.0000
JTTDCMut+F+G4   -54637.4990 109476.9981 - 0.0000 109485.0717 - 0.0000 110071.2642 - 0.0000
LG+F+I+G4       -54688.3616 109580.7231 - 0.0000 109588.9599 - 0.0000 110180.8731 - 0.0000
LG+F+G4         -54710.8565 109623.7131 - 0.0000 109631.7867 - 0.0000 110217.9792 - 0.0000
VT+F+I+G4       -54764.0770 109732.1540 - 0.0000 109740.3908 - 0.0000 110332.3040 - 0.0000
WAG+F+I+G4      -54782.2145 109768.4289 - 0.0000 109776.6657 - 0.0000 110368.5789 - 0.0000
VT+F+G4         -54791.8485 109785.6970 - 0.0000 109793.7707 - 0.0000 110379.9631 - 0.0000
JTT+I+G4        -54867.2258 109900.4516 - 0.0000 109905.8773 - 0.0000 110388.8090 - 0.0000
WAG+F+G4        -54810.1792 109822.3583 - 0.0000 109830.4320 - 0.0000 110416.6244 - 0.0000
JTTDCMut+I+G4   -54884.3040 109934.6080 - 0.0000 109940.0337 - 0.0000 110422.9653 - 0.0000
JTT+G4          -54897.8204 109959.6408 - 0.0000 109964.9353 - 0.0000 110442.1143 - 0.0000
JTTDCMut+G4     -54915.1923 109994.3846 - 0.0000 109999.6790 - 0.0000 110476.8581 - 0.0000
mtInv+F+I+G4    -54871.7838 109947.5677 - 0.0000 109955.8045 - 0.0000 110547.7177 - 0.0000
mtInv+F+G4      -54894.0547 109990.1094 - 0.0000 109998.1831 - 0.0000 110584.3755 - 0.0000
rtREV+F+I+G4    -54940.7962 110085.5924 - 0.0000 110093.8292 - 0.0000 110685.7424 - 0.0000
Dayhoff+F+I+G4  -54954.9706 110113.9412 - 0.0000 110122.1780 - 0.0000 110714.0912 - 0.0000
DCMut+F+I+G4    -54955.9118 110115.8237 - 0.0000 110124.0604 - 0.0000 110715.9736 - 0.0000
rtREV+F+G4      -54962.5258 110127.0516 - 0.0000 110135.1253 - 0.0000 110721.3177 - 0.0000
Dayhoff+F+G4    -54990.3722 110182.7444 - 0.0000 110190.8180 - 0.0000 110777.0105 - 0.0000
DCMut+F+G4      -54991.3103 110184.6206 - 0.0000 110192.6943 - 0.0000 110778.8867 - 0.0000
mtMet+F+I+G4    -55005.5693 110215.1386 - 0.0000 110223.3754 - 0.0000 110815.2886 - 0.0000
cpREV+F+I+G4    -55007.3706 110218.7412 - 0.0000 110226.9780 - 0.0000 110818.8912 - 0.0000
WAG+I+G4        -55090.1718 110346.3436 - 0.0000 110351.7693 - 0.0000 110834.7009 - 0.0000
mtMet+F+G4      -55026.7881 110255.5761 - 0.0000 110263.6498 - 0.0000 110849.8423 - 0.0000
cpREV+F+G4      -55030.6578 110263.3156 - 0.0000 110271.3893 - 0.0000 110857.5818 - 0.0000
WAG+G4          -55119.9014 110403.8029 - 0.0000 110409.0973 - 0.0000 110886.2764 - 0.0000
VT+I+G4         -55175.9942 110517.9884 - 0.0000 110523.4141 - 0.0000 111006.3457 - 0.0000
VT+G4           -55205.0243 110574.0486 - 0.0000 110579.3430 - 0.0000 111056.5221 - 0.0000
PMB+F+I+G4      -55158.2078 110520.4156 - 0.0000 110528.6524 - 0.0000 111120.5656 - 0.0000
Blosum62+F+I+G4 -55172.8941 110549.7881 - 0.0000 110558.0249 - 0.0000 111149.9381 - 0.0000
HIVb+F+I+G4     -55176.4974 110556.9948 - 0.0000 110565.2315 - 0.0000 111157.1447 - 0.0000
PMB+F+G4        -55184.4244 110570.8489 - 0.0000 110578.9225 - 0.0000 111165.1150 - 0.0000
LG+I+G4         -55264.4334 110694.8667 - 0.0000 110700.2924 - 0.0000 111183.2241 - 0.0000
Dayhoff+I+G4    -55266.1968 110698.3936 - 0.0000 110703.8193 - 0.0000 111186.7509 - 0.0000
DCMut+I+G4      -55267.2034 110700.4068 - 0.0000 110705.8325 - 0.0000 111188.7641 - 0.0000
Blosum62+F+G4   -55196.2813 110594.5627 - 0.0000 110602.6363 - 0.0000 111188.8288 - 0.0000
FLU+F+I+G4      -55198.1462 110600.2924 - 0.0000 110608.5291 - 0.0000 111200.4423 - 0.0000
HIVb+F+G4       -55211.5477 110625.0955 - 0.0000 110633.1691 - 0.0000 111219.3616 - 0.0000
LG+G4           -55288.9782 110741.9564 - 0.0000 110747.2509 - 0.0000 111224.4299 - 0.0000
FLU+F+G4        -55223.0965 110648.1931 - 0.0000 110656.2667 - 0.0000 111242.4592 - 0.0000
Dayhoff+G4      -55302.8235 110769.6471 - 0.0000 110774.9415 - 0.0000 111252.1206 - 0.0000
DCMut+G4        -55303.8121 110771.6242 - 0.0000 110776.9186 - 0.0000 111254.0977 - 0.0000
mtZOA+F+I+G4    -55262.4171 110728.8342 - 0.0000 110737.0709 - 0.0000 111328.9841 - 0.0000
mtREV+F+I+G4    -55273.7246 110751.4493 - 0.0000 110759.6861 - 0.0000 111351.5993 - 0.0000
mtZOA+F+G4      -55278.2129 110758.4258 - 0.0000 110766.4995 - 0.0000 111352.6919 - 0.0000
mtREV+F+G4      -55296.6202 110795.2405 - 0.0000 110803.3142 - 0.0000 111389.5066 - 0.0000
mtVer+F+I+G4    -55437.3208 111078.6416 - 0.0000 111086.8784 - 0.0000 111678.7916 - 0.0000
mtVer+F+G4      -55468.0711 111138.1422 - 0.0000 111146.2158 - 0.0000 111732.4083 - 0.0000
cpREV+I+G4      -55548.7484 111263.4967 - 0.0000 111268.9224 - 0.0000 111751.8540 - 0.0000
cpREV+G4        -55571.1577 111306.3154 - 0.0000 111311.6098 - 0.0000 111788.7889 - 0.0000
PMB+I+G4        -55580.7438 111327.4876 - 0.0000 111332.9133 - 0.0000 111815.8449 - 0.0000
PMB+G4          -55607.5276 111379.0552 - 0.0000 111384.3497 - 0.0000 111861.5287 - 0.0000
Blosum62+I+G4   -55773.3184 111712.6367 - 0.0000 111718.0624 - 0.0000 112200.9941 - 0.0000
Blosum62+G4     -55797.6853 111759.3707 - 0.0000 111764.6651 - 0.0000 112241.8442 - 0.0000
JTT+F+I         -55794.0447 111790.0894 - 0.0000 111798.1630 - 0.0000 112384.3555 - 0.0000
VT+F+I          -55798.4614 111798.9228 - 0.0000 111806.9965 - 0.0000 112393.1890 - 0.0000
JTTDCMut+F+I    -55818.0037 111838.0074 - 0.0000 111846.0811 - 0.0000 112432.2736 - 0.0000
rtREV+I+G4      -55914.0624 111994.1248 - 0.0000 111999.5504 - 0.0000 112482.4821 - 0.0000
mtART+F+I+G4    -55849.7487 111903.4974 - 0.0000 111911.7342 - 0.0000 112503.6474 - 0.0000
rtREV+G4        -55937.4570 112038.9140 - 0.0000 112044.2084 - 0.0000 112521.3875 - 0.0000
mtMAM+F+I+G4    -55860.6264 111925.2527 - 0.0000 111933.4895 - 0.0000 112525.4027 - 0.0000
mtART+F+G4      -55864.9479 111931.8958 - 0.0000 111939.9695 - 0.0000 112526.1620 - 0.0000
mtMAM+F+G4      -55880.9966 111963.9933 - 0.0000 111972.0669 - 0.0000 112558.2594 - 0.0000
WAG+F+I         -55926.4563 112054.9126 - 0.0000 112062.9862 - 0.0000 112649.1787 - 0.0000
JTT+I           -56023.2476 112210.4953 - 0.0000 112215.7897 - 0.0000 112692.9688 - 0.0000
JTTDCMut+I      -56045.2041 112254.4083 - 0.0000 112259.7027 - 0.0000 112736.8818 - 0.0000
VT+F            -56028.7670 112257.5340 - 0.0000 112265.4463 - 0.0000 112845.9163 - 0.0000
LG+F+I          -56041.3011 112284.6022 - 0.0000 112292.6759 - 0.0000 112878.8684 - 0.0000
HIVb+I+G4       -56114.9307 112395.8614 - 0.0000 112401.2871 - 0.0000 112884.2187 - 0.0000
JTT+F           -56049.2391 112298.4782 - 0.0000 112306.3904 - 0.0000 112886.8605 - 0.0000
JTTDCMut+F      -56072.7287 112345.4575 - 0.0000 112353.3697 - 0.0000 112933.8398 - 0.0000
HIVb+G4         -56148.8993 112461.7985 - 0.0000 112467.0930 - 0.0000 112944.2720 - 0.0000
VT+I            -56190.8823 112545.7646 - 0.0000 112551.0590 - 0.0000 113028.2381 - 0.0000
WAG+I           -56212.6747 112589.3493 - 0.0000 112594.6438 - 0.0000 113071.8228 - 0.0000
WAG+F           -56164.0132 112528.0264 - 0.0000 112535.9386 - 0.0000 113116.4087 - 0.0000
DCMut+F+I       -56173.7699 112549.5399 - 0.0000 112557.6135 - 0.0000 113143.8060 - 0.0000
Dayhoff+F+I     -56176.4007 112554.8014 - 0.0000 112562.8751 - 0.0000 113149.0675 - 0.0000
JTT             -56271.8982 112705.7965 - 0.0000 112710.9613 - 0.0000 113182.3862 - 0.0000
JTTDCMut        -56293.9549 112749.9097 - 0.0000 112755.0746 - 0.0000 113226.4994 - 0.0000
PMB+F+I         -56231.9681 112665.9362 - 0.0000 112674.0098 - 0.0000 113260.2023 - 0.0000
HIVw+F+I+G4     -56243.8716 112691.7432 - 0.0000 112699.9800 - 0.0000 113291.8932 - 0.0000
Blosum62+F+I    -56255.5986 112713.1973 - 0.0000 112721.2709 - 0.0000 113307.4634 - 0.0000
LG+F            -56275.7138 112751.4276 - 0.0000 112759.3398 - 0.0000 113339.8099 - 0.0000
FLU+I+G4        -56344.3992 112854.7984 - 0.0000 112860.2240 - 0.0000 113343.1557 - 0.0000
HIVw+F+G4       -56275.7638 112753.5277 - 0.0000 112761.6013 - 0.0000 113347.7938 - 0.0000
FLU+G4          -56369.9966 112903.9932 - 0.0000 112909.2877 - 0.0000 113386.4667 - 0.0000
rtREV+F+I       -56298.3209 112798.6418 - 0.0000 112806.7154 - 0.0000 113392.9079 - 0.0000
VT              -56421.2442 113004.4884 - 0.0000 113009.6533 - 0.0000 113481.0781 - 0.0000
DCMut+I         -56434.1369 113032.2737 - 0.0000 113037.5682 - 0.0000 113514.7472 - 0.0000
Dayhoff+I       -56436.7184 113037.4367 - 0.0000 113042.7312 - 0.0000 113519.9102 - 0.0000
WAG             -56450.1194 113062.2387 - 0.0000 113067.4036 - 0.0000 113538.8284 - 0.0000
mtInv+F+I       -56405.6350 113013.2700 - 0.0000 113021.3437 - 0.0000 113607.5362 - 0.0000
cpREV+F+I       -56419.2510 113040.5020 - 0.0000 113048.5757 - 0.0000 113634.7682 - 0.0000
DCMut+F         -56440.1055 113080.2109 - 0.0000 113088.1232 - 0.0000 113668.5932 - 0.0000
Dayhoff+F       -56442.9282 113085.8563 - 0.0000 113093.7686 - 0.0000 113674.2387 - 0.0000
PMB+F           -56463.3285 113126.6571 - 0.0000 113134.5693 - 0.0000 113715.0394 - 0.0000
Blosum62+F      -56473.3220 113146.6441 - 0.0000 113154.5563 - 0.0000 113735.0264 - 0.0000
rtREV+F         -56524.2456 113248.4912 - 0.0000 113256.4034 - 0.0000 113836.8735 - 0.0000
LG+I            -56604.5277 113373.0554 - 0.0000 113378.3498 - 0.0000 113855.5289 - 0.0000
PMB+I           -56649.5909 113463.1817 - 0.0000 113468.4762 - 0.0000 113945.6552 - 0.0000
DCMut           -56692.4035 113546.8070 - 0.0000 113551.9718 - 0.0000 114023.3966 - 0.0000
Dayhoff         -56695.0928 113552.1857 - 0.0000 113557.3505 - 0.0000 114028.7753 - 0.0000
cpREV+F         -56642.3357 113484.6715 - 0.0000 113492.5838 - 0.0000 114073.0538 - 0.0000
mtInv+F         -56647.7725 113495.5449 - 0.0000 113503.4572 - 0.0000 114083.9272 - 0.0000
mtMet+F+I       -56720.3885 113642.7770 - 0.0000 113650.8507 - 0.0000 114237.0431 - 0.0000
Blosum62+I      -56832.4233 113828.8465 - 0.0000 113834.1410 - 0.0000 114311.3200 - 0.0000
LG              -56844.2931 113850.5863 - 0.0000 113855.7512 - 0.0000 114327.1760 - 0.0000
PMB             -56880.4186 113922.8372 - 0.0000 113928.0020 - 0.0000 114399.4268 - 0.0000
cpREV+I         -56936.7452 114037.4904 - 0.0000 114042.7848 - 0.0000 114519.9639 - 0.0000
FLU+F+I         -56878.7052 113959.4103 - 0.0000 113967.4840 - 0.0000 114553.6765 - 0.0000
HIVb+F+I        -56895.7961 113993.5922 - 0.0000 114001.6658 - 0.0000 114587.8583 - 0.0000
mtMet+F         -56973.8991 114147.7982 - 0.0000 114155.7104 - 0.0000 114736.1805 - 0.0000
Blosum62        -57048.8930 114259.7860 - 0.0000 114264.9509 - 0.0000 114736.3757 - 0.0000
mtREV+F+I       -56988.0956 114178.1912 - 0.0000 114186.2648 - 0.0000 114772.4573 - 0.0000
cpREV           -57152.7941 114467.5882 - 0.0000 114472.7531 - 0.0000 114944.1779 - 0.0000
rtREV+I         -57220.2324 114604.4649 - 0.0000 114609.7593 - 0.0000 115086.9384 - 0.0000
FLU+F           -57152.7332 114505.4664 - 0.0000 114513.3787 - 0.0000 115093.8488 - 0.0000
HIVb+F          -57210.0316 114620.0633 - 0.0000 114627.9755 - 0.0000 115208.4456 - 0.0000
mtREV+F         -57226.0769 114652.1538 - 0.0000 114660.0661 - 0.0000 115240.5362 - 0.0000
mtZOA+I+G4      -57346.0614 114858.1227 - 0.0000 114863.5484 - 0.0000 115346.4801 - 0.0000
mtZOA+G4        -57360.0433 114884.0867 - 0.0000 114889.3811 - 0.0000 115366.5602 - 0.0000
mtVer+F+I       -57313.9578 114829.9157 - 0.0000 114837.9894 - 0.0000 115424.1818 - 0.0000
mtZOA+F+I       -57355.9739 114913.9479 - 0.0000 114922.0215 - 0.0000 115508.2140 - 0.0000
rtREV           -57443.3586 115048.7171 - 0.0000 115053.8820 - 0.0000 115525.3068 - 0.0000
mtZOA+F         -57590.1402 115380.2805 - 0.0000 115388.1928 - 0.0000 115968.6628 - 0.0000
mtVer+F         -57626.0598 115452.1197 - 0.0000 115460.0319 - 0.0000 116040.5020 - 0.0000
HIVb+I          -57748.7030 115661.4059 - 0.0000 115666.7004 - 0.0000 116143.8794 - 0.0000
FLU+I           -57999.0910 116162.1820 - 0.0000 116167.4764 - 0.0000 116644.6555 - 0.0000
HIVb            -58054.5168 116271.0335 - 0.0000 116276.1984 - 0.0000 116747.6232 - 0.0000
HIVw+F+I        -58040.9510 116283.9021 - 0.0000 116291.9757 - 0.0000 116878.1682 - 0.0000
mtREV+I+G4      -58145.9763 116457.9525 - 0.0000 116463.3782 - 0.0000 116946.3099 - 0.0000
mtREV+G4        -58161.5114 116487.0227 - 0.0000 116492.3172 - 0.0000 116969.4962 - 0.0000
FLU             -58267.8182 116697.6365 - 0.0000 116702.8014 - 0.0000 117174.2262 - 0.0000
HIVw+I+G4       -58301.2673 116768.5346 - 0.0000 116773.9603 - 0.0000 117256.8919 - 0.0000
HIVw+G4         -58332.4429 116828.8858 - 0.0000 116834.1802 - 0.0000 117311.3593 - 0.0000
mtMAM+F+I       -58288.6488 116779.2977 - 0.0000 116787.3714 - 0.0000 117373.5638 - 0.0000
mtVer+I+G4      -58383.0866 116932.1733 - 0.0000 116937.5990 - 0.0000 117420.5306 - 0.0000
mtVer+G4        -58398.3040 116960.6080 - 0.0000 116965.9024 - 0.0000 117443.0815 - 0.0000
HIVw+F          -58371.1585 116942.3170 - 0.0000 116950.2292 - 0.0000 117530.6993 - 0.0000
mtMet+I+G4      -58464.8424 117095.6849 - 0.0000 117101.1106 - 0.0000 117584.0422 - 0.0000
mtMet+G4        -58479.3914 117122.7828 - 0.0000 117128.0772 - 0.0000 117605.2563 - 0.0000
mtART+F+I       -58482.6390 117167.2781 - 0.0000 117175.3517 - 0.0000 117761.5442 - 0.0000
mtMAM+F         -58579.2961 117358.5922 - 0.0000 117366.5044 - 0.0000 117946.9745 - 0.0000
mtART+F         -58733.0588 117666.1175 - 0.0000 117674.0298 - 0.0000 118254.4998 - 0.0000
mtART+I+G4      -58860.2394 117886.4788 - 0.0000 117891.9045 - 0.0000 118374.8361 - 0.0000
mtART+G4        -58877.4586 117918.9173 - 0.0000 117924.2117 - 0.0000 118401.3908 - 0.0000
mtMAM+I+G4      -58973.4862 118112.9725 - 0.0000 118118.3982 - 0.0000 118601.3298 - 0.0000
mtMAM+G4        -58986.9826 118137.9653 - 0.0000 118143.2597 - 0.0000 118620.4388 - 0.0000
mtInv+I+G4      -59128.5148 118423.0297 - 0.0000 118428.4554 - 0.0000 118911.3870 - 0.0000
mtInv+G4        -59144.6886 118453.3772 - 0.0000 118458.6716 - 0.0000 118935.8507 - 0.0000
mtZOA+I         -59963.8285 120091.6570 - 0.0000 120096.9514 - 0.0000 120574.1305 - 0.0000
HIVw+I          -60015.0729 120194.1457 - 0.0000 120199.4402 - 0.0000 120676.6192 - 0.0000
mtZOA           -60190.7064 120543.4127 - 0.0000 120548.5776 - 0.0000 121020.0024 - 0.0000
HIVw            -60312.4274 120786.8549 - 0.0000 120792.0197 - 0.0000 121263.4446 - 0.0000
mtREV+I         -60353.0194 120870.0388 - 0.0000 120875.3332 - 0.0000 121352.5123 - 0.0000
mtREV           -60576.8852 121315.7704 - 0.0000 121320.9352 - 0.0000 121792.3601 - 0.0000
mtMet+I         -60617.6455 121399.2910 - 0.0000 121404.5854 - 0.0000 121881.7645 - 0.0000
mtMet           -60853.8241 121869.6481 - 0.0000 121874.8130 - 0.0000 122346.2378 - 0.0000
mtVer+I         -60884.9517 121933.9033 - 0.0000 121939.1978 - 0.0000 122416.3768 - 0.0000
mtInv+I         -60984.7717 122133.5433 - 0.0000 122138.8377 - 0.0000 122616.0168 - 0.0000
mtVer           -61169.6441 122501.2882 - 0.0000 122506.4531 - 0.0000 122977.8779 - 0.0000
mtInv           -61213.8820 122589.7641 - 0.0000 122594.9289 - 0.0000 123066.3537 - 0.0000
mtART+I         -62132.3352 124428.6704 - 0.0000 124433.9649 - 0.0000 124911.1439 - 0.0000
mtMAM+I         -62284.9853 124733.9707 - 0.0000 124739.2651 - 0.0000 125216.4442 - 0.0000
mtART           -62393.1361 124948.2721 - 0.0000 124953.4370 - 0.0000 125424.8618 - 0.0000
mtMAM           -62576.5315 125315.0629 - 0.0000 125320.2278 - 0.0000 125791.6526 - 0.0000

AIC, w-AIC   : Akaike information criterion scores and weights.
AICc, w-AICc : Corrected AIC scores and weights.
BIC, w-BIC   : Bayesian information criterion scores and weights.

Plus signs denote the 95% confidence sets.
Minus signs denote significant exclusion.

SUBSTITUTION PROCESS
--------------------

Model of substitution: JTT+F+I+G4

State frequencies: (empirical counts from alignment)

  pi(A) = 0.0975
  pi(R) = 0.0621
  pi(N) = 0.0340
  pi(D) = 0.0427
  pi(C) = 0.0203
  pi(Q) = 0.0361
  pi(E) = 0.0578
  pi(G) = 0.0698
  pi(H) = 0.0253
  pi(I) = 0.0390
  pi(L) = 0.0992
  pi(K) = 0.0478
  pi(M) = 0.0224
  pi(F) = 0.0385
  pi(P) = 0.0634
  pi(S) = 0.0995
  pi(T) = 0.0499
  pi(W) = 0.0087
  pi(Y) = 0.0200
  pi(V) = 0.0659

Model of rate heterogeneity: Invar+Gamma with 4 categories
Proportion of invariable sites: 0.0180
Gamma shape alpha: 1.4890

 Category  Relative_rate  Proportion
  0         0              0.0180
  1         0.2277         0.2455
  2         0.5974         0.2455
  3         1.0690         0.2455
  4         2.1791         0.2455
Relative rates are computed as MEAN of the portion of the Gamma distribution falling in the category.

MAXIMUM LIKELIHOOD TREE
-----------------------

Log-likelihood of the tree: -54554.1639 (s.e. 1092.6661)
Unconstrained log-likelihood (without tree): -19500.1430
Number of free parameters (#branches + #model parameters): 102
Akaike information criterion (AIC) score: 109312.3278
Corrected Akaike information criterion (AICc) score: 109320.5646
Bayesian information criterion (BIC) score: 109912.4778

Total tree length (sum of branch lengths): 28.1028
Sum of internal branch lengths: 7.7595 (27.6110% of tree length)

WARNING: 1 near-zero internal branches (<0.0004) should be treated with caution
         Such branches are denoted by '**' in the figure below

NOTE: Tree is UNROOTED although outgroup taxon 'AagrOXF_evm.TU.utg000027l.341' is drawn at root
Numbers in parentheses are  ultrafast bootstrap support (%)

+----AagrOXF_evm.TU.utg000027l.341
|
|                 +-----Azfi_s0007.g011185
|              +--| (100)
|              |  +-----Azfi_s0075.g037586
|           +--| (88)
|           |  |        +--AT3G12280
|           |  |     +--| (100)
|           |  |     |  +--Solyc09g091280
|           |  |  +--| (81)
|           |  |  |  |     +--Bradi3g41630
|           |  |  |  |  +--| (95)
|           |  |  |  |  |  +--LOC_Os08g42600
|           |  |  |  +--| (100)
|           |  |  |     |  +--Zm00001d031678
|           |  |  |     +--| (100)
|           |  |  |        +--Zm00001d052695
|           |  +--| (100)
|           |     |       +--Bradi4g17210
|           |     |    +--| (89)
|           |     |    |  +--LOC_Os11g32900
|           |     +----| (100)
|           |          |  +--Zm00001d007407
|           |          +--| (100)
|           |             +--Zm00001d052666
|        +--| (83)
|        |  |     +--------CBR_g45825
|        |  |  +--| (87)
|        |  |  |  |        +-----------Chrsp27S04309
|        |  |  |  |     +--| (52)
|        |  |  |  |     |  |              +**MV_2135
|        |  |  |  |     |  |  +-----------| (100)
|        |  |  |  |     |  |  |           +**MV_2136
|        |  |  |  |     |  +--| (45)
|        |  |  |  |     |     +-------------PRCOL_00001859_RA
|        |  |  |  |  +--| (98)
|        |  |  |  |  |  |  +-----------------Cre06.g255450_4532
|        |  |  |  |  |  +--| (79)
|        |  |  |  |  |     +-----------------Olucimarinus_27862
|        |  |  |  +--| (92)
|        |  |  |     +------------KN_9100
|        |  +--| (100)
|        |     |         +----CLOP_g9832
|        |     |     +---| (92)
|        |     |     |   |                +--pm002761g0010
|        |     |     |   |             +**| (37)
|        |     |     |   |             |  +----------------------------------------pm024506g0030
|        |     |     |   |          +--| (67)
|        |     |     |   |          |  +--pm022739g0010
|        |     |     |   |       +--| (66)
|        |     |     |   |       |  +--pm013886.t2
|        |     |     |   +-------| (91)
|        |     |     |           +--pm041065g0010
|        |     |  +--| (74)
|        |     |  |  |  +----Me1_v2_0225420
|        |     |  |  +--| (97)
|        |     |  |     |   +---Zci1a_13456
|        |     |  |     +---| (100)
|        |     |  |         |  +**Zci_06674
|        |     |  |         +--| (100)
|        |     |  |            +**UTEX1559_10640
|        |     +--| (64)
|        |        |       +--SM000151S01519
|        |        +-------| (100)
|        |                |  +--SM000174S03343
|        |                +--| (99)
|        |                   +--SM000278S10005
|     +--| (79)
|     |  +----Mp8g18830
|  +--| (55)
|  |  |      +--Pp3c9_2440
|  |  |  +---| (100)
|  |  |  |   +--Pp3c15_2800
|  |  +--| (49)
|  |     +----Pp3c19_9596
+--| (86)
|  |     +--SM_442657
|  +-----| (100)
|        +--SM_97707
|
|   +--Pp3c19_9580
+---| (100)
    +--Pp3c19_9550

Tree in newick format:

(AagrOXF_evm.TU.utg000027l.341:0.5119037561,((((((Azfi_s0007.g011185:0.6422888751,Azfi_s0075.g037586:0.5900641325)100:0.2089694865,(((AT3G12280:0.2946418549,Solyc09g091280:0.2239345343)100:0.1450568394,((Bradi3g41630:0.1406580177,LOC_Os08g42600:0.0563473625)95:0.0162998060,(Zm00001d031678:0.0345203333,Zm00001d052695:0.0622362379)100:0.0815570132)100:0.2413006547)81:0.0856570358,((Bradi4g17210:0.1076419251,LOC_Os11g32900:0.1381317834)89:0.0372081745,(Zm00001d007407:0.0610777909,Zm00001d052666:0.0646660180)100:0.1137871430)100:0.5610918826)100:0.3466111498)88:0.0609572887,((CBR_g45825:0.9637706510,(((Chrsp27S04309:1.2335529742,((MV_2135:0.0000022399,MV_2136:0.0000022399)100:1.2014098130,PRCOL_00001859_RA:1.4277225727)45:0.1202519546)52:0.1176056577,(Cre06.g255450_4532:1.8448064585,Olucimarinus_27862:1.7687778404)79:0.2565353828)98:0.3307972475,KN_9100:1.2943306592)92:0.1377037287)87:0.1450636920,(((CLOP_g9832:0.5237379835,((((pm002761g0010:0.0021745497,pm024506g0030:4.0378095675)37:0.0000026614,pm022739g0010:0.0332691043)67:0.0159525035,pm013886.t2:0.0067130497)66:0.0171406343,pm041065g0010:0.0478112908)91:0.7841523395)92:0.4382185284,(Me1_v2_0225420:0.5763339103,(Zci1a_13456:0.3986029482,(Zci_06674:0.0000022399,UTEX1559_10640:0.0000022399)100:0.3345906528)100:0.4831907877)97:0.1344197991)74:0.1151362198,(SM000151S01519:0.0199355425,(SM000174S03343:0.0096597558,SM000278S10005:0.0025032598)99:0.0071131845)100:0.7839066063)64:0.0647817062)100:0.1594878571)83:0.0611250639,Mp8g18830:0.4984199999)79:0.0350950188,((Pp3c9_2440:0.0883246169,Pp3c15_2800:0.0601506215)100:0.4068975259,Pp3c19_9596:0.4928802371)49:0.0626946053)55:0.0525713655,(SM_442657:0.3187761024,SM_97707:0.1661987675)100:0.6039986986)86:0.0596047612,(Pp3c19_9580:0.0108368107,Pp3c19_9550:0.0521166184)100:0.4675172864);

CONSENSUS TREE
--------------

Consensus tree is constructed from 1000bootstrap trees
Log-likelihood of consensus tree: -54554.165012
Robinson-Foulds distance between ML tree and consensus tree: 0

Branches with support >0.000000% are kept (extended consensus)
Branch lengths are optimized by maximum likelihood on original alignment
Numbers in parentheses are bootstrap supports (%)

+----AagrOXF_evm.TU.utg000027l.341
|
|                 +-----Azfi_s0007.g011185
|              +--| (100)
|              |  +-----Azfi_s0075.g037586
|           +--| (88)
|           |  |        +--AT3G12280
|           |  |     +--| (100)
|           |  |     |  +--Solyc09g091280
|           |  |  +--| (81)
|           |  |  |  |     +--Bradi3g41630
|           |  |  |  |  +--| (95)
|           |  |  |  |  |  +--LOC_Os08g42600
|           |  |  |  +--| (100)
|           |  |  |     |  +--Zm00001d031678
|           |  |  |     +--| (100)
|           |  |  |        +--Zm00001d052695
|           |  +--| (100)
|           |     |       +--Bradi4g17210
|           |     |    +--| (89)
|           |     |    |  +--LOC_Os11g32900
|           |     +----| (100)
|           |          |  +--Zm00001d007407
|           |          +--| (100)
|           |             +--Zm00001d052666
|        +--| (83)
|        |  |     +--------CBR_g45825
|        |  |  +--| (87)
|        |  |  |  |        +-----------Chrsp27S04309
|        |  |  |  |     +--| (52)
|        |  |  |  |     |  |              +--MV_2135
|        |  |  |  |     |  |  +-----------| (100)
|        |  |  |  |     |  |  |           +--MV_2136
|        |  |  |  |     |  +--| (45)
|        |  |  |  |     |     +-------------PRCOL_00001859_RA
|        |  |  |  |  +--| (98)
|        |  |  |  |  |  |  +-----------------Cre06.g255450_4532
|        |  |  |  |  |  +--| (79)
|        |  |  |  |  |     +-----------------Olucimarinus_27862
|        |  |  |  +--| (92)
|        |  |  |     +------------KN_9100
|        |  +--| (100)
|        |     |         +----CLOP_g9832
|        |     |     +---| (92)
|        |     |     |   |                +--pm002761g0010
|        |     |     |   |             +--| (37)
|        |     |     |   |             |  +----------------------------------------pm024506g0030
|        |     |     |   |          +--| (67)
|        |     |     |   |          |  +--pm022739g0010
|        |     |     |   |       +--| (66)
|        |     |     |   |       |  +--pm013886.t2
|        |     |     |   +-------| (91)
|        |     |     |           +--pm041065g0010
|        |     |  +--| (74)
|        |     |  |  |  +----Me1_v2_0225420
|        |     |  |  +--| (97)
|        |     |  |     |   +---Zci1a_13456
|        |     |  |     +---| (100)
|        |     |  |         |  +--Zci_06674
|        |     |  |         +--| (100)
|        |     |  |            +--UTEX1559_10640
|        |     +--| (64)
|        |        |       +--SM000151S01519
|        |        +-------| (100)
|        |                |  +--SM000174S03343
|        |                +--| (99)
|        |                   +--SM000278S10005
|     +--| (79)
|     |  +----Mp8g18830
|  +--| (55)
|  |  |      +--Pp3c9_2440
|  |  |  +---| (100)
|  |  |  |   +--Pp3c15_2800
|  |  +--| (49)
|  |     +----Pp3c19_9596
+--| (86)
|  |     +--SM_442657
|  +-----| (100)
|        +--SM_97707
|
|   +--Pp3c19_9580
+---| (100)
    +--Pp3c19_9550


Consensus tree in newick format: 

(AagrOXF_evm.TU.utg000027l.341:0.5118367014,((((((Azfi_s0007.g011185:0.6422442066,Azfi_s0075.g037586:0.5900625786)100:0.2089790941,(((AT3G12280:0.2946371041,Solyc09g091280:0.2239302333)100:0.1450609055,((Bradi3g41630:0.1406563021,LOC_Os08g42600:0.0563451224)95:0.0162994262,(Zm00001d031678:0.0345192407,Zm00001d052695:0.0622349626)100:0.0815556095)100:0.2412899591)81:0.0856581835,((Bradi4g17210:0.1076402984,LOC_Os11g32900:0.1381282571)89:0.0372077597,(Zm00001d007407:0.0610761142,Zm00001d052666:0.0646653416)100:0.1137847981)100:0.5610739253)100:0.3465766875)88:0.0609532135,((CBR_g45825:0.9636906901,(((Chrsp27S04309:1.2335133587,((MV_2135:0.0000024682,MV_2136:0.0000024682)100:1.2013594564,PRCOL_00001859_RA:1.4276519673)45:0.1202756012)52:0.1175993302,(Cre06.g255450_4532:1.8445462443,Olucimarinus_27862:1.7685233098)79:0.2568056419)98:0.3307397227,KN_9100:1.2942887540)92:0.1377624832)87:0.1450636483,(((CLOP_g9832:0.5237125833,((((pm002761g0010:0.0006781017,pm024506g0030:4.0364457782)37:0.0014981091,pm022739g0010:0.0332675539)67:0.0159584011,pm013886.t2:0.0067085793)66:0.0171431750,pm041065g0010:0.0478087408)91:0.7841499822)92:0.4382117026,(Me1_v2_0225420:0.5762976454,(Zci1a_13456:0.3985865802,(Zci_06674:0.0000024682,UTEX1559_10640:0.0000024682)100:0.3345791009)100:0.4831608984)97:0.1344382054)74:0.1151276835,(SM000151S01519:0.0199364140,(SM000174S03343:0.0096588132,SM000278S10005:0.0025019619)99:0.0071128466)100:0.7838686342)64:0.0647940806)100:0.1594704875)83:0.0611304980,Mp8g18830:0.4984013269)79:0.0349651825,((Pp3c9_2440:0.0883483577,Pp3c15_2800:0.0601303019)100:0.4060608421,Pp3c19_9596:0.4926630600)49:0.0637753794)55:0.0525726577,(SM_442657:0.3187849904,SM_97707:0.1661882289)100:0.6039861376)86:0.0596401881,(Pp3c19_9580:0.0108364821,Pp3c19_9550:0.0521154375)100:0.4675090183);

TIME STAMP
----------

Date and time: Sun Feb  4 08:48:15 2024
Total CPU time used: 266224.5891 seconds (73h:57m:4s)
Total wall-clock time used: 132138.4612 seconds (36h:42m:18s)

