IQ-TREE 1.6.12 built Dec  4 2019

Input file name: /data/armin/stress_profiling_phylogeny/msa/N0.HOG0008640.fa
Type of analysis: ModelFinder + tree reconstruction + ultrafast bootstrap (1000 replicates)
Random seed number: 11442

REFERENCES
----------

To cite ModelFinder please use: 

Subha Kalyaanamoorthy, Bui Quang Minh, Thomas KF Wong, Arndt von Haeseler,
and Lars S Jermiin (2017) ModelFinder: Fast model selection for
accurate phylogenetic estimates. Nature Methods, 14:587–589.
https://doi.org/10.1038/nmeth.4285

To cite IQ-TREE please use:

Lam-Tung Nguyen, Heiko A. Schmidt, Arndt von Haeseler, and Bui Quang Minh
(2015) IQ-TREE: A fast and effective stochastic algorithm for estimating
maximum likelihood phylogenies. Mol Biol Evol, 32:268-274.
https://doi.org/10.1093/molbev/msu300

Since you used ultrafast bootstrap (UFBoot) please also cite: 

Diep Thi Hoang, Olga Chernomor, Arndt von Haeseler, Bui Quang Minh,
and Le Sy Vinh (2017) UFBoot2: Improving the ultrafast bootstrap
approximation. Mol Biol Evol, in press.
https://doi.org/10.1093/molbev/msx281

SEQUENCE ALIGNMENT
------------------

Input data: 24 sequences with 911 amino-acid sites
Number of constant sites: 321 (= 35.236% of all sites)
Number of invariant (constant or ambiguous constant) sites: 321 (= 35.236% of all sites)
Number of parsimony informative sites: 432
Number of distinct site patterns: 741

ModelFinder
-----------

Best-fit model according to BIC: LG+F+I+G4

List of models sorted by BIC scores: 

Model             LogL          AIC      w-AIC      AICc     w-AICc       BIC      w-BIC
LG+F+I+G4       -16319.0745  32770.1491 + 1.0000  32780.6278 + 1.0000  33087.9089 + 0.9999
LG+F+G4         -16331.8500  32793.7000 - 0.0000  32803.8538 - 0.0000  33106.6452 - 0.0001
rtREV+F+I+G4    -16381.1361  32894.2721 - 0.0000  32904.7508 - 0.0000  33212.0320 - 0.0000
WAG+F+I+G4      -16384.5405  32901.0810 - 0.0000  32911.5597 - 0.0000  33218.8408 - 0.0000
rtREV+F+G4      -16392.7603  32915.5206 - 0.0000  32925.6744 - 0.0000  33228.4659 - 0.0000
WAG+F+G4        -16405.2481  32940.4961 - 0.0000  32950.6500 - 0.0000  33253.4414 - 0.0000
LG+I+G4         -16468.1065  33030.2130 - 0.0000  33035.4412 - 0.0000  33256.4965 - 0.0000
LG+G4           -16480.0289  33052.0578 - 0.0000  33057.0624 - 0.0000  33273.5268 - 0.0000
WAG+I+G4        -16479.1750  33052.3501 - 0.0000  33057.5784 - 0.0000  33278.6336 - 0.0000
VT+F+I+G4       -16427.3179  32986.6358 - 0.0000  32997.1145 - 0.0000  33304.3956 - 0.0000
WAG+G4          -16500.2873  33092.5747 - 0.0000  33097.5793 - 0.0000  33314.0436 - 0.0000
VT+F+G4         -16446.4701  33022.9402 - 0.0000  33033.0940 - 0.0000  33335.8854 - 0.0000
JTT+F+I+G4      -16455.4529  33042.9059 - 0.0000  33053.3846 - 0.0000  33360.6657 - 0.0000
JTTDCMut+F+I+G4 -16459.2102  33050.4203 - 0.0000  33060.8990 - 0.0000  33368.1802 - 0.0000
VT+I+G4         -16534.4486  33162.8973 - 0.0000  33168.1256 - 0.0000  33389.1808 - 0.0000
JTT+F+G4        -16473.4543  33076.9086 - 0.0000  33087.0624 - 0.0000  33389.8538 - 0.0000
JTTDCMut+F+G4   -16477.4363  33084.8727 - 0.0000  33095.0265 - 0.0000  33397.8180 - 0.0000
VT+G4           -16553.0449  33198.0899 - 0.0000  33203.0945 - 0.0000  33419.5589 - 0.0000
JTT+I+G4        -16555.5354  33205.0708 - 0.0000  33210.2991 - 0.0000  33431.3543 - 0.0000
JTTDCMut+I+G4   -16557.4372  33208.8745 - 0.0000  33214.1027 - 0.0000  33435.1580 - 0.0000
Dayhoff+F+I+G4  -16499.0153  33130.0305 - 0.0000  33140.5092 - 0.0000  33447.7904 - 0.0000
Blosum62+F+I+G4 -16499.7292  33131.4583 - 0.0000  33141.9370 - 0.0000  33449.2182 - 0.0000
DCMut+F+I+G4    -16499.7966  33131.5932 - 0.0000  33142.0719 - 0.0000  33449.3530 - 0.0000
cpREV+F+I+G4    -16504.6573  33141.3145 - 0.0000  33151.7932 - 0.0000  33459.0743 - 0.0000
JTT+G4          -16573.0556  33238.1113 - 0.0000  33243.1159 - 0.0000  33459.5802 - 0.0000
JTTDCMut+G4     -16575.3117  33242.6234 - 0.0000  33247.6281 - 0.0000  33464.0924 - 0.0000
Blosum62+F+G4   -16516.4341  33162.8682 - 0.0000  33173.0221 - 0.0000  33475.8135 - 0.0000
Dayhoff+F+G4    -16521.8634  33173.7269 - 0.0000  33183.8807 - 0.0000  33486.6722 - 0.0000
DCMut+F+G4      -16522.6292  33175.2585 - 0.0000  33185.4123 - 0.0000  33488.2038 - 0.0000
PMB+F+I+G4      -16519.7821  33171.5642 - 0.0000  33182.0429 - 0.0000  33489.3240 - 0.0000
cpREV+F+G4      -16527.8868  33185.7735 - 0.0000  33195.9274 - 0.0000  33498.7188 - 0.0000
PMB+F+G4        -16538.0632  33206.1264 - 0.0000  33216.2802 - 0.0000  33519.0716 - 0.0000
mtInv+F+I+G4    -16540.6746  33213.3492 - 0.0000  33223.8278 - 0.0000  33531.1090 - 0.0000
mtInv+F+G4      -16551.0805  33232.1609 - 0.0000  33242.3147 - 0.0000  33545.1062 - 0.0000
Dayhoff+I+G4    -16624.8339  33343.6677 - 0.0000  33348.8960 - 0.0000  33569.9513 - 0.0000
DCMut+I+G4      -16625.6931  33345.3863 - 0.0000  33350.6145 - 0.0000  33571.6698 - 0.0000
PMB+I+G4        -16633.5521  33361.1043 - 0.0000  33366.3326 - 0.0000  33587.3878 - 0.0000
Dayhoff+G4      -16648.0149  33388.0297 - 0.0000  33393.0343 - 0.0000  33609.4987 - 0.0000
DCMut+G4        -16648.8975  33389.7949 - 0.0000  33394.7995 - 0.0000  33611.2639 - 0.0000
PMB+G4          -16650.5510  33393.1021 - 0.0000  33398.1067 - 0.0000  33614.5711 - 0.0000
mtZOA+F+I+G4    -16587.3010  33306.6020 - 0.0000  33317.0807 - 0.0000  33624.3619 - 0.0000
mtZOA+F+G4      -16593.9678  33317.9355 - 0.0000  33328.0894 - 0.0000  33630.8808 - 0.0000
Blosum62+I+G4   -16657.6375  33409.2749 - 0.0000  33414.5032 - 0.0000  33635.5584 - 0.0000
Blosum62+G4     -16672.9287  33437.8575 - 0.0000  33442.8621 - 0.0000  33659.3265 - 0.0000
cpREV+I+G4      -16670.9016  33435.8033 - 0.0000  33441.0316 - 0.0000  33662.0868 - 0.0000
rtREV+I+G4      -16672.3049  33438.6097 - 0.0000  33443.8380 - 0.0000  33664.8932 - 0.0000
rtREV+G4        -16684.3133  33460.6267 - 0.0000  33465.6313 - 0.0000  33682.0957 - 0.0000
LG+F+I          -16620.3082  33370.6164 - 0.0000  33380.7703 - 0.0000  33683.5617 - 0.0000
WAG+F+I         -16624.4738  33378.9476 - 0.0000  33389.1014 - 0.0000  33691.8929 - 0.0000
cpREV+G4        -16691.5414  33475.0828 - 0.0000  33480.0874 - 0.0000  33696.5518 - 0.0000
mtMet+F+I+G4    -16624.8524  33381.7049 - 0.0000  33392.1835 - 0.0000  33699.4647 - 0.0000
mtMet+F+G4      -16633.0105  33396.0211 - 0.0000  33406.1749 - 0.0000  33708.9664 - 0.0000
VT+F+I          -16636.6904  33403.3808 - 0.0000  33413.5346 - 0.0000  33716.3261 - 0.0000
mtREV+F+I+G4    -16642.2127  33416.4253 - 0.0000  33426.9040 - 0.0000  33734.1852 - 0.0000
WAG+I           -16713.2130  33518.4259 - 0.0000  33523.4305 - 0.0000  33739.8949 - 0.0000
mtREV+F+G4      -16650.5568  33431.1137 - 0.0000  33441.2675 - 0.0000  33744.0589 - 0.0000
HIVb+F+I+G4     -16661.4997  33454.9994 - 0.0000  33465.4781 - 0.0000  33772.7592 - 0.0000
VT+I            -16745.2832  33582.5663 - 0.0000  33587.5710 - 0.0000  33804.0353 - 0.0000
HIVb+F+G4       -16681.3278  33492.6556 - 0.0000  33502.8095 - 0.0000  33805.6009 - 0.0000
rtREV+F+I       -16687.2916  33504.5833 - 0.0000  33514.7371 - 0.0000  33817.5285 - 0.0000
FLU+F+I+G4      -16694.1685  33520.3371 - 0.0000  33530.8158 - 0.0000  33838.0969 - 0.0000
FLU+F+G4        -16712.4923  33554.9847 - 0.0000  33565.1385 - 0.0000  33867.9299 - 0.0000
LG+I            -16779.5911  33651.1823 - 0.0000  33656.1869 - 0.0000  33872.6512 - 0.0000
Blosum62+F+I    -16717.6339  33565.2678 - 0.0000  33575.4217 - 0.0000  33878.2131 - 0.0000
JTT+F+I         -16737.7886  33605.5771 - 0.0000  33615.7309 - 0.0000  33918.5224 - 0.0000
PMB+F+I         -16739.0309  33608.0618 - 0.0000  33618.2156 - 0.0000  33921.0071 - 0.0000
JTTDCMut+F+I    -16741.4840  33612.9681 - 0.0000  33623.1219 - 0.0000  33925.9133 - 0.0000
mtART+F+I+G4    -16743.4308  33618.8615 - 0.0000  33629.3402 - 0.0000  33936.6214 - 0.0000
mtART+F+G4      -16747.5034  33625.0068 - 0.0000  33635.1607 - 0.0000  33937.9521 - 0.0000
JTT+I           -16835.0593  33762.1185 - 0.0000  33767.1232 - 0.0000  33983.5875 - 0.0000
LG+F            -16775.1012  33678.2024 - 0.0000  33688.0370 - 0.0000  33986.3332 - 0.0000
JTTDCMut+I      -16836.4634  33764.9267 - 0.0000  33769.9313 - 0.0000  33986.3957 - 0.0000
cpREV+F+I       -16787.6992  33705.3985 - 0.0000  33715.5523 - 0.0000  34018.3437 - 0.0000
PMB+I           -16853.4801  33798.9602 - 0.0000  33803.9649 - 0.0000  34020.4292 - 0.0000
mtVer+F+I+G4    -16786.9440  33705.8880 - 0.0000  33716.3667 - 0.0000  34023.6478 - 0.0000
VT+F            -16795.4328  33718.8655 - 0.0000  33728.7001 - 0.0000  34026.9963 - 0.0000
WAG+F           -16796.6553  33721.3105 - 0.0000  33731.1450 - 0.0000  34029.4413 - 0.0000
mtVer+F+G4      -16797.9023  33725.8045 - 0.0000  33735.9584 - 0.0000  34038.7498 - 0.0000
Blosum62+I      -16874.9412  33841.8825 - 0.0000  33846.8871 - 0.0000  34063.3514 - 0.0000
WAG             -16883.7293  33857.4585 - 0.0000  33862.2446 - 0.0000  34074.1129 - 0.0000
DCMut+F+I       -16817.3794  33764.7588 - 0.0000  33774.9126 - 0.0000  34077.7041 - 0.0000
Dayhoff+F+I     -16818.0089  33766.0178 - 0.0000  33776.1717 - 0.0000  34078.9631 - 0.0000
rtREV+F         -16833.8978  33795.7957 - 0.0000  33805.6302 - 0.0000  34103.9264 - 0.0000
VT              -16901.8495  33893.6990 - 0.0000  33898.4851 - 0.0000  34110.3534 - 0.0000
Blosum62+F      -16865.6990  33859.3979 - 0.0000  33869.2325 - 0.0000  34167.5287 - 0.0000
DCMut+I         -16927.0594  33946.1187 - 0.0000  33951.1234 - 0.0000  34167.5877 - 0.0000
Dayhoff+I       -16927.5602  33947.1205 - 0.0000  33952.1251 - 0.0000  34168.5895 - 0.0000
LG              -16933.8422  33957.6844 - 0.0000  33962.4705 - 0.0000  34174.3388 - 0.0000
HIVb+I+G4       -16935.2985  33964.5970 - 0.0000  33969.8253 - 0.0000  34190.8806 - 0.0000
cpREV+I         -16953.4497  33998.8995 - 0.0000  34003.9041 - 0.0000  34220.3684 - 0.0000
mtMAM+F+G4      -16890.6463  33911.2926 - 0.0000  33921.4464 - 0.0000  34224.2379 - 0.0000
mtMAM+F+I+G4    -16887.4026  33906.8052 - 0.0000  33917.2839 - 0.0000  34224.5651 - 0.0000
HIVb+G4         -16956.7079  34005.4159 - 0.0000  34010.4205 - 0.0000  34226.8849 - 0.0000
PMB+F           -16898.4418  33924.8835 - 0.0000  33934.7180 - 0.0000  34233.0142 - 0.0000
rtREV+I         -16972.2266  34036.4531 - 0.0000  34041.4577 - 0.0000  34257.9221 - 0.0000
JTT+F           -16919.9644  33967.9288 - 0.0000  33977.7633 - 0.0000  34276.0595 - 0.0000
JTTDCMut+F      -16924.4297  33976.8594 - 0.0000  33986.6939 - 0.0000  34284.9901 - 0.0000
PMB             -17007.2278  34104.4556 - 0.0000  34109.2417 - 0.0000  34321.1100 - 0.0000
JTT             -17011.9169  34113.8338 - 0.0000  34118.6199 - 0.0000  34330.4882 - 0.0000
JTTDCMut        -17014.2972  34118.5944 - 0.0000  34123.3806 - 0.0000  34335.2489 - 0.0000
Blosum62        -17016.5754  34123.1509 - 0.0000  34127.9370 - 0.0000  34339.8053 - 0.0000
mtInv+F+I       -16961.6249  34053.2499 - 0.0000  34063.4037 - 0.0000  34366.1952 - 0.0000
cpREV+F         -16971.6513  34071.3026 - 0.0000  34081.1371 - 0.0000  34379.4333 - 0.0000
FLU+I+G4        -17068.6024  34231.2049 - 0.0000  34236.4332 - 0.0000  34457.4884 - 0.0000
DCMut+F         -17019.0772  34166.1545 - 0.0000  34175.9890 - 0.0000  34474.2852 - 0.0000
Dayhoff+F       -17020.0368  34168.0736 - 0.0000  34177.9081 - 0.0000  34476.2043 - 0.0000
FLU+G4          -17086.7434  34265.4869 - 0.0000  34270.4915 - 0.0000  34486.9558 - 0.0000
rtREV           -17117.5847  34325.1694 - 0.0000  34329.9555 - 0.0000  34541.8239 - 0.0000
DCMut           -17123.7514  34337.5028 - 0.0000  34342.2889 - 0.0000  34554.1573 - 0.0000
Dayhoff         -17124.4893  34338.9785 - 0.0000  34343.7647 - 0.0000  34555.6330 - 0.0000
cpREV           -17125.0540  34340.1080 - 0.0000  34344.8942 - 0.0000  34556.7625 - 0.0000
HIVb+F+I        -17078.7621  34287.5243 - 0.0000  34297.6781 - 0.0000  34600.4695 - 0.0000
FLU+F+I         -17102.9970  34335.9939 - 0.0000  34346.1478 - 0.0000  34648.9392 - 0.0000
mtREV+F+I       -17106.4710  34342.9420 - 0.0000  34353.0959 - 0.0000  34655.8873 - 0.0000
mtInv+F         -17119.1915  34366.3831 - 0.0000  34376.2176 - 0.0000  34674.5138 - 0.0000
mtMet+F+I       -17123.6806  34377.3611 - 0.0000  34387.5149 - 0.0000  34690.3064 - 0.0000
HIVw+F+I+G4     -17166.7761  34465.5522 - 0.0000  34476.0309 - 0.0000  34783.3121 - 0.0000
mtZOA+F+I       -17171.8290  34473.6581 - 0.0000  34483.8119 - 0.0000  34786.6034 - 0.0000
HIVw+F+G4       -17188.8398  34507.6796 - 0.0000  34517.8334 - 0.0000  34820.6248 - 0.0000
mtZOA+G4        -17292.5069  34677.0138 - 0.0000  34682.0184 - 0.0000  34898.4828 - 0.0000
mtZOA+I+G4      -17290.6591  34675.3182 - 0.0000  34680.5465 - 0.0000  34901.6018 - 0.0000
mtREV+F         -17269.8760  34667.7519 - 0.0000  34677.5864 - 0.0000  34975.8826 - 0.0000
HIVb+I          -17347.6917  34787.3834 - 0.0000  34792.3881 - 0.0000  35008.8524 - 0.0000
mtMet+F         -17287.7491  34703.4983 - 0.0000  34713.3328 - 0.0000  35011.6290 - 0.0000
HIVb+F          -17308.9255  34745.8510 - 0.0000  34755.6856 - 0.0000  35053.9818 - 0.0000
FLU+F           -17312.2005  34752.4011 - 0.0000  34762.2356 - 0.0000  35060.5318 - 0.0000
mtZOA+F         -17330.7093  34789.4185 - 0.0000  34799.2530 - 0.0000  35097.5493 - 0.0000
mtVer+F+I       -17364.1280  34858.2560 - 0.0000  34868.4098 - 0.0000  35171.2012 - 0.0000
FLU+I           -17488.5360  35069.0720 - 0.0000  35074.0766 - 0.0000  35290.5410 - 0.0000
HIVb            -17575.4592  35240.9185 - 0.0000  35245.7046 - 0.0000  35457.5729 - 0.0000
mtART+F+I       -17516.6337  35163.2673 - 0.0000  35173.4212 - 0.0000  35476.2126 - 0.0000
mtREV+G4        -17605.9047  35303.8093 - 0.0000  35308.8139 - 0.0000  35525.2783 - 0.0000
mtREV+I+G4      -17603.3760  35300.7519 - 0.0000  35305.9802 - 0.0000  35527.0354 - 0.0000
mtVer+F         -17563.2666  35254.5332 - 0.0000  35264.3678 - 0.0000  35562.6640 - 0.0000
HIVw+F+I        -17623.2737  35376.5473 - 0.0000  35386.7012 - 0.0000  35689.4926 - 0.0000
FLU             -17700.5194  35491.0388 - 0.0000  35495.8249 - 0.0000  35707.6932 - 0.0000
mtMAM+F+I       -17635.5268  35401.0536 - 0.0000  35411.2074 - 0.0000  35713.9989 - 0.0000
mtART+G4        -17735.3495  35562.6991 - 0.0000  35567.7037 - 0.0000  35784.1681 - 0.0000
mtART+I+G4      -17733.9573  35561.9147 - 0.0000  35567.1430 - 0.0000  35788.1982 - 0.0000
mtMet+G4        -17742.2899  35576.5798 - 0.0000  35581.5844 - 0.0000  35798.0488 - 0.0000
mtMet+I+G4      -17740.1452  35574.2903 - 0.0000  35579.5186 - 0.0000  35800.5738 - 0.0000
mtART+F         -17686.2328  35500.4656 - 0.0000  35510.3001 - 0.0000  35808.5963 - 0.0000
mtVer+G4        -17753.6351  35599.2702 - 0.0000  35604.2748 - 0.0000  35820.7392 - 0.0000
mtVer+I+G4      -17752.2346  35598.4692 - 0.0000  35603.6974 - 0.0000  35824.7527 - 0.0000
HIVw+I+G4       -17820.6932  35735.3864 - 0.0000  35740.6147 - 0.0000  35961.6700 - 0.0000
HIVw+G4         -17841.6345  35775.2689 - 0.0000  35780.2736 - 0.0000  35996.7379 - 0.0000
mtMAM+F         -17817.3637  35762.7273 - 0.0000  35772.5619 - 0.0000  36070.8581 - 0.0000
mtInv+I+G4      -17883.9181  35861.8362 - 0.0000  35867.0645 - 0.0000  36088.1197 - 0.0000
mtInv+G4        -17887.3423  35866.6846 - 0.0000  35871.6892 - 0.0000  36088.1535 - 0.0000
mtMAM+G4        -17900.1614  35892.3229 - 0.0000  35897.3275 - 0.0000  36113.7919 - 0.0000
mtMAM+I+G4      -17900.4442  35894.8883 - 0.0000  35900.1166 - 0.0000  36121.1718 - 0.0000
HIVw+F          -17870.3332  35868.6663 - 0.0000  35878.5008 - 0.0000  36176.7971 - 0.0000
mtZOA+I         -18081.0879  36254.1758 - 0.0000  36259.1804 - 0.0000  36475.6448 - 0.0000
mtZOA           -18215.4609  36520.9218 - 0.0000  36525.7080 - 0.0000  36737.5763 - 0.0000
mtREV+I         -18240.8519  36573.7039 - 0.0000  36578.7085 - 0.0000  36795.1728 - 0.0000
HIVw+I          -18277.8327  36647.6654 - 0.0000  36652.6700 - 0.0000  36869.1344 - 0.0000
mtREV           -18380.2487  36850.4974 - 0.0000  36855.2835 - 0.0000  37067.1518 - 0.0000
mtMet+I         -18405.1277  36902.2555 - 0.0000  36907.2601 - 0.0000  37123.7245 - 0.0000
mtInv+I         -18431.9455  36955.8910 - 0.0000  36960.8957 - 0.0000  37177.3600 - 0.0000
HIVw            -18519.6250  37129.2500 - 0.0000  37134.0361 - 0.0000  37345.9044 - 0.0000
mtMet           -18544.6813  37179.3626 - 0.0000  37184.1487 - 0.0000  37396.0171 - 0.0000
mtInv           -18566.8265  37223.6530 - 0.0000  37228.4391 - 0.0000  37440.3074 - 0.0000
mtVer+I         -18567.2928  37226.5855 - 0.0000  37231.5901 - 0.0000  37448.0545 - 0.0000
mtVer           -18734.7174  37559.4349 - 0.0000  37564.2210 - 0.0000  37776.0893 - 0.0000
mtART+I         -18752.5825  37597.1650 - 0.0000  37602.1696 - 0.0000  37818.6340 - 0.0000
mtART           -18911.9788  37913.9577 - 0.0000  37918.7438 - 0.0000  38130.6121 - 0.0000
mtMAM+I         -18987.8199  38067.6399 - 0.0000  38072.6445 - 0.0000  38289.1089 - 0.0000
mtMAM           -19153.5883  38397.1765 - 0.0000  38401.9626 - 0.0000  38613.8310 - 0.0000

AIC, w-AIC   : Akaike information criterion scores and weights.
AICc, w-AICc : Corrected AIC scores and weights.
BIC, w-BIC   : Bayesian information criterion scores and weights.

Plus signs denote the 95% confidence sets.
Minus signs denote significant exclusion.

SUBSTITUTION PROCESS
--------------------

Model of substitution: LG+F+I+G4

State frequencies: (empirical counts from alignment)

  pi(A) = 0.1091
  pi(R) = 0.0666
  pi(N) = 0.0298
  pi(D) = 0.0601
  pi(C) = 0.0125
  pi(Q) = 0.0429
  pi(E) = 0.0674
  pi(G) = 0.0618
  pi(H) = 0.0097
  pi(I) = 0.0337
  pi(L) = 0.1157
  pi(K) = 0.0461
  pi(M) = 0.0211
  pi(F) = 0.0465
  pi(P) = 0.0572
  pi(S) = 0.0760
  pi(T) = 0.0446
  pi(W) = 0.0101
  pi(Y) = 0.0220
  pi(V) = 0.0670

Model of rate heterogeneity: Invar+Gamma with 4 categories
Proportion of invariable sites: 0.0474
Gamma shape alpha: 1.5920

 Category  Relative_rate  Proportion
  0         0              0.0474
  1         0.2511         0.2381
  2         0.6332         0.2381
  3         1.1079         0.2381
  4         2.2069         0.2381
Relative rates are computed as MEAN of the portion of the Gamma distribution falling in the category.

MAXIMUM LIKELIHOOD TREE
-----------------------

Log-likelihood of the tree: -16308.0664 (s.e. 443.3466)
Unconstrained log-likelihood (without tree): -5872.3770
Number of free parameters (#branches + #model parameters): 66
Akaike information criterion (AIC) score: 32748.1327
Corrected Akaike information criterion (AICc) score: 32758.6114
Bayesian information criterion (BIC) score: 33065.8926

Total tree length (sum of branch lengths): 13.3967
Sum of internal branch lengths: 3.2940 (24.5878% of tree length)

WARNING: 2 near-zero internal branches (<0.0011) should be treated with caution
         Such branches are denoted by '**' in the figure below

NOTE: Tree is UNROOTED although outgroup taxon 'AagrOXF_evm.TU.utg000035l.173' is drawn at root
Numbers in parentheses are  ultrafast bootstrap support (%)

+----------AagrOXF_evm.TU.utg000035l.173
|
|        +----------------Azfi_s0003.g007639
|     +--| (79)
|     |  +------------SM_406852
|  +--| (69)
|  |  |             +------------------CLOP_g14955
|  |  |      +------| (100)
|  |  |      |      |                   +**pm002701.t1
|  |  |      |      |                +--| (38)
|  |  |      |      |                |  |  +--pm003322g0030
|  |  |      |      |                |  +**| (10)
|  |  |      |      |                |     +--pm099190g0010
|  |  |      |      |             +**| (20)
|  |  |      |      |             |  +**pm025096g0050
|  |  |      |      +-------------| (100)
|  |  |      |                    +--pm013088g0030
|  |  |   +--| (67)
|  |  |   |  |         +-------------------------Chrsp17S02659
|  |  |   |  |      +--| (69)
|  |  |   |  |      |  |         +---------------------Cre12.g524500_4532
|  |  |   |  |      |  |   +-----| (95)
|  |  |   |  |      |  |   |     +---------------------------Olucimarinus_30782
|  |  |   |  |      |  +---| (91)
|  |  |   |  |      |      +----------------------------------PRCOL_00000951_RA
|  |  |   |  |  +---| (97)
|  |  |   |  |  |   +-------------------MV_23825
|  |  |   |  +--| (77)
|  |  |   |     +----------------KN_2143
|  |  +---| (99)
|  |      |     +--------------Me1_v2_0034690
|  |      |  +--| (62)
|  |      |  |  |  +----------Me1_v2_0141790
|  |      |  |  +--| (56)
|  |      |  |     |               +**Zci_11135
|  |      |  |     |            +--| (94)
|  |      |  |     |            |  +**UTEX1559_05278
|  |      |  |     +------------| (100)
|  |      |  |                  +--UTEX1560_10211
|  |      +--| (91)
|  |         |            +--SM000402S15227
|  |         +------------| (100)
|  |                      +--SM000429S16082
+--| (56)
|  +-------------Mp6g09920
|
+---------Pp3c22_6390

Tree in newick format:

(AagrOXF_evm.TU.utg000035l.173:0.4176018620,(((Azfi_s0003.g007639:0.6416258624,SM_406852:0.5142184046)79:0.1421354079,(((CLOP_g14955:0.7210175352,(((pm002701.t1:0.0000025433,(pm003322g0030:0.0042181684,pm099190g0010:0.0159575253)10:0.0000020196)38:0.0052379782,pm025096g0050:0.0000020705)20:0.0000028476,pm013088g0030:0.0036556464)100:0.5377743857)100:0.2812151847,(((Chrsp17S02659:0.9780232555,((Cre12.g524500_4532:0.8439807099,Olucimarinus_30782:1.0560661589)95:0.2286129787,PRCOL_00000951_RA:1.3140948267)91:0.1728748973)69:0.0703790003,MV_23825:0.7731995965)97:0.1779100406,KN_2143:0.6647848669)77:0.0884719287)67:0.0639078821,((Me1_v2_0034690:0.5788972892,(Me1_v2_0141790:0.4419754522,((Zci_11135:0.0000020705,UTEX1559_05278:0.0000020705)94:0.0153350044,UTEX1560_10211:0.0367335035)100:0.4927167421)56:0.0281494899)62:0.0651909617,(SM000402S15227:0.0619205437,SM000429S16082:0.0763712930)100:0.4970134265)91:0.1116129689)99:0.1630279504)69:0.1010160922,Mp6g09920:0.5525397717)56:0.0666988077,Pp3c22_6390:0.3904988139);

CONSENSUS TREE
--------------

Consensus tree is constructed from 1000bootstrap trees
Log-likelihood of consensus tree: -16308.067024
Robinson-Foulds distance between ML tree and consensus tree: 4

Branches with support >0.000000% are kept (extended consensus)
Branch lengths are optimized by maximum likelihood on original alignment
Numbers in parentheses are bootstrap supports (%)

+----------AagrOXF_evm.TU.utg000035l.173
|
|        +----------------Azfi_s0003.g007639
|     +--| (79)
|     |  +------------SM_406852
|  +--| (69)
|  |  |             +------------------CLOP_g14955
|  |  |      +------| (100)
|  |  |      |      |                   +--pm002701.t1
|  |  |      |      |                +--| (60)
|  |  |      |      |                |  +--pm003322g0030
|  |  |      |      |             +--| (38)
|  |  |      |      |             |  +--pm099190g0010
|  |  |      |      +-------------| (100)
|  |  |      |                    |  +--pm013088g0030
|  |  |      |                    +--| (29)
|  |  |      |                       +--pm025096g0050
|  |  |   +--| (67)
|  |  |   |  |         +-------------------------Chrsp17S02659
|  |  |   |  |      +--| (69)
|  |  |   |  |      |  |         +---------------------Cre12.g524500_4532
|  |  |   |  |      |  |   +-----| (95)
|  |  |   |  |      |  |   |     +---------------------------Olucimarinus_30782
|  |  |   |  |      |  +---| (91)
|  |  |   |  |      |      +----------------------------------PRCOL_00000951_RA
|  |  |   |  |  +---| (97)
|  |  |   |  |  |   +-------------------MV_23825
|  |  |   |  +--| (77)
|  |  |   |     +----------------KN_2143
|  |  +---| (99)
|  |      |     +--------------Me1_v2_0034690
|  |      |  +--| (62)
|  |      |  |  |  +----------Me1_v2_0141790
|  |      |  |  +--| (56)
|  |      |  |     |               +--Zci_11135
|  |      |  |     |            +--| (94)
|  |      |  |     |            |  +--UTEX1559_05278
|  |      |  |     +------------| (100)
|  |      |  |                  +--UTEX1560_10211
|  |      +--| (91)
|  |         |            +--SM000402S15227
|  |         +------------| (100)
|  |                      +--SM000429S16082
+--| (56)
|  +-------------Mp6g09920
|
+---------Pp3c22_6390


Consensus tree in newick format: 

(AagrOXF_evm.TU.utg000035l.173:0.4175470415,(((Azfi_s0003.g007639:0.6415021551,SM_406852:0.5141459699)79:0.1421384770,(((CLOP_g14955:0.7209016352,(((pm002701.t1:0.0000022310,pm003322g0030:0.0042230752)60:0.0000020416,pm099190g0010:0.0159548343)38:0.0052337128,(pm013088g0030:0.0036531230,pm025096g0050:0.0000020716)29:0.0000026787)100:0.5376961765)100:0.2810863797,(((Chrsp17S02659:0.9777948102,((Cre12.g524500_4532:0.8437976624,Olucimarinus_30782:1.0558764879)95:0.2286339194,PRCOL_00000951_RA:1.3138152897)91:0.1728781378)69:0.0703868135,MV_23825:0.7730837251)97:0.1778368400,KN_2143:0.6646894822)77:0.0884980056)67:0.0639008006,((Me1_v2_0034690:0.5798258588,(Me1_v2_0141790:0.4419629906,((Zci_11135:0.0000020716,UTEX1559_05278:0.0000020716)94:0.0153349409,UTEX1560_10211:0.0367283319)100:0.4924225310)56:0.0304833607)62:0.0627636174,(SM000402S15227:0.0619272385,SM000429S16082:0.0763528857)100:0.4969966942)91:0.1116930379)99:0.1630060938)69:0.1010318192,Mp6g09920:0.5524558833)56:0.0666956996,Pp3c22_6390:0.3904539270);

TIME STAMP
----------

Date and time: Sat Feb  3 22:39:19 2024
Total CPU time used: 210991.946 seconds (58h:36m:31s)
Total wall-clock time used: 109869.5083 seconds (30h:31m:9s)

